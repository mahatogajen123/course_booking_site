<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 5; $i++) {
            DB::table("blogs")->insert([
                "blogcategory_id" => $faker->numberBetween($min = 1, $max = 4),
                "title" => $faker->word(),
                "sub_title" => $faker->word(),
                "short_detail" => $faker->sentence(50),
                "detail" => $faker->paragraph(),
                'image' => $faker->image('public/uploads/blog/images/', 300, 440, null, false),
                'created_at' => $faker->date('Y_m_d'),
            ]);
        }
    }
}
