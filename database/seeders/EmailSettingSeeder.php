<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmailSetting;


class EmailSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmailSetting::create([
            'mailer' => 'smtp',
            'host' => 'smtp.gmail.com',
            'port' => '465',
            'user_name' => 'sajhabazzar21@gmail.com',
            'password' => 'sh@jhab@zzar123 ',
            'encryption' => 'ssl',
            'from_address' => 'sajhabazzar21@gmail.com',
            'from_name' => config('app.name')
        ]);
    }
}
