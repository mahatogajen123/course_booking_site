<?php

namespace Database\Seeders;

use App\Models\BlogCategory;
use Illuminate\Database\Seeder;

class BlogCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Food = BlogCategory::create([
            'name' => 'Food',
        ]);
        $Clothes = BlogCategory::create([
            'name' => 'Clothes',
        ]);
        $Tradition = BlogCategory::create([
            'name' => 'Tradition',
        ]);
        $Culture = BlogCategory::create([
            'name' => 'Culture',
        ]);
    }
}
