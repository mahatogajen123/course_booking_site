<?php

namespace Database\Seeders;

use App\Models\BlogCategory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolePermissionSeeder::class,
            UserTableSeeder::class,
            EmailSettingSeeder::class,
            CourseCategorySeeder::class,
            BlogCategorySeeder::class,
            ModuleSeeder::class,
            BlogSeeder::class,
            // MenuSeeder::class,
        ]);
    }
}
