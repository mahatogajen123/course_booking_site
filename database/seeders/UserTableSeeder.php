<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserProfile;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //        $user = User::create([
        //            'name' => 'Administrator',
        //            'email' => 'admin@gmail.com',
        //            'password' => Hash::make('password')
        //        ]);

        $admin = User::create([
            'full_name' => 'admin Yadav',
            'contact_number' => '9800000001',
            'address' => 'Swayambhu',
            'email' => 'admin@test.com',
            'user_type' => 'admin',
            'password' => Hash::make('password')
        ]);

        $user = User::create([
            'full_name' => 'user shrestha',
            'contact_number' => '9800000002',
            'address' => 'Swayambhu',
            'email' => 'user@test.com',
            'user_type' => 'user',
            'password' => Hash::make('password')
        ]);

        $admin->assignRole('admin');
        $user->assignRole('user');
        $user->createToken('appusertoken')->plainTextToken;
    }
}
