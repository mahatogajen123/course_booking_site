<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\CourseCategory;
use Illuminate\Database\Seeder;

class CourseCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $CSIT = CourseCategory::create([
            'name' => 'CSIT',
        ]);
        $BCA = CourseCategory::create([
            'name' => 'BCA',
        ]);
        $BBA = CourseCategory::create([
            'name' => 'BBA',
        ]);
        $BSW = CourseCategory::create([
            'name' => 'BSW',
        ]);
    }
}
