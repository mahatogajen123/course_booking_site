<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Module;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('module_permission')->truncate();
        Schema::enableForeignKeyConstraints();

        Schema::disableForeignKeyConstraints();
        DB::table('modules')->truncate();
        Schema::enableForeignKeyConstraints();

        $modules = [
            'Role'
         ];
      
         foreach ($modules as $module) {
              Module::create(['name' => $module]);
         }

         $module1=Module::find(1);
         $module1->permissions()->sync([1,2,3,4]);
    }
}
