<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('careers', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('sub_title');
            $table->string('job_type');
            $table->string('job_level');
            $table->string('company_name');
            $table->string('location');
            $table->string('offer_salary');
            $table->date('job_create_date');
            $table->date('deadline');
            $table->text('short_detail');
            $table->string('image');
            $table->longText('detail');
            $table->longText('responsibility');
            $table->longText('requirement');
            $table->string('slug');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('careers');
    }
}
