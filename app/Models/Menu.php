<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $guarded = [];

    public function menu_parent()
    {
        return $this->belongsTo(self::class, 'parent_id',);
    }
    public function child_menu()
    {
        return $this->hasMany(self::class, 'parent_id', 'id', 'title');
    }
    public function parent_name()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id', 'title');
    }
    public function active_child_menu()
    {
        return $this->child_menu()->whereStatus(1);
    }
}
