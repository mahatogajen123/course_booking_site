<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'sub_title',
        'short_detail',
        'image',
        'url',
        'course_id'

    ];

    public function Course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
