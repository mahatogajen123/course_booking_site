<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CareerApply extends Model
{
    use HasFactory;
    protected $guarded = [];
    // protected $fillable = ['first_name', 'last_name', 'email', 'address', 'contact', 'file', 'career_id'];
}
