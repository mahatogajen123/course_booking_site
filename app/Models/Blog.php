<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'sub_title',
        'short_detail',
        'image',
        'detail',
        'blogcategory_id',
        'status'
    ];

    protected function BlogCategory()
    {
        return $this->belongsTo(BlogCategory::class,'blogcategory_id','id');
    }
}
