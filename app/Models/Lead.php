<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    use HasFactory;
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'permanent_address',
        'home_contact',
        'personal_contact',
        'current_address',
        'gender',
        'dob',
        'course_id'
    ];

    protected function Course()
    {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }
}
