<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyDetail extends Model
{
    use HasFactory;
    protected $guarded = [];
    // protected $fillable = ['title', 'slogan', 'image', 'copyright'];
    public function images()
    {
        return $this->hasMany(AboutImage::class)->latest()->take(3);
    }
}
