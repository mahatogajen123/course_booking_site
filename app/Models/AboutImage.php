<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutImage extends Model
{
    use HasFactory;
    protected $guarded = [];
    
    public function companydetail()
    {
        return $this->belongsTo(CompanyDetail::class);
    }
}
