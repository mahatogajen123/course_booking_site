<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'=>'required'
,            'title' => 'required',
            'meta_title' => 'required',
            'og_meta_title' => 'required',
            'short_detail' => 'required',
            'description' => 'required',
            'duration' => 'required',
            'start_date' => 'required',
            'end_date' => 'required|after:start_date',
            'price' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
    }
}
