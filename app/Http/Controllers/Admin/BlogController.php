<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // return resource_path('dist\img\avatar.png');
        $page_title = "Blog List";
        $page_description = "List blog";
        $search = $request->search;
        $data = Blog::when($search, function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'desc')->paginate(5);
        return view('backend.system.blog.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Edit Blog";
        $page_description = " Blog Edit page";
        $blogcategory = BlogCategory::select('id', 'name')->get();
        return view('backend.system.blog.create', compact('page_title', 'page_description', 'blogcategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'sub_title' => 'required',
            'short_detail' => 'required',
            'detail' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);

        $attribute = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/blog/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $attribute['image'] = "$Image";
        }
        Blog::create($attribute);
        return redirect()->route('admin.blog.index')->with('success', 'Blog Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        $blogcategory = BlogCategory::select('id', 'name')->get();
        $page_title = "Edit Blog";
        $page_description = "Blog Edit";
        return view('backend.system.blog.edit', compact('page_title', 'page_description', 'blog', 'blogcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'sub_title' => 'required',
            'short_detail' => 'required',
            'detail' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'

        ]);
        $blog = Blog::find($id);
        $attribute = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/blog/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $attribute['image'] = $Image ?? $blog->image;
        }
        $blog->update($attribute);
        return redirect()->route('admin.blog.index')->with('success', 'Blog updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blog->delete();
        return redirect()->back()->with("success", "Blog Deleted Successfully");
    }

    public function changeStatus(Request $request)
    {
        $blog = Blog::find($request->blog_id);
        $blog->status = $request->status;
        $blog->save();
    }
}
