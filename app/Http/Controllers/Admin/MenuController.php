<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use Illuminate\Http\Request;
use Throwable;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Menu List";
        $page_description = "List menu";
        $search = $request->search;
        $data = Menu::with('parent_name')->when($search, function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%')->get();
        })->orderBy('id', 'desc')->paginate(9);
        return view('backend.system.menu.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent  = Menu::select('id', 'title')->get();
        $page_title = " Create Menu";
        $page_description = " Create Menu";
        return View('backend.system.menu.create', compact('page_title', 'page_description', 'parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $credentials = $request->validate([
            'title' => 'required',
            'url' => 'required|url',
            'icon' => 'required|image:mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        if ($credentials) {
            try {
                $attribute = $request->all();
                if ($icon = $request->file('icon')) {
                    $destinationPath = 'uploads/menu/icon';
                    $Icon = date('YmdHis') . "." . $icon->getClientOriginalExtension();
                    $icon->move($destinationPath, $Icon);
                    $attribute['icon'] = $Icon;
                }
                $attribute['slug'] = $request->title;
                $attribute['parent_id'] = $request->parent_id;
                Menu::create($attribute);
                return redirect()->route('admin.menu.index')->with('success', 'Menu created Successfully');
            } catch (Throwable  $e) {
                return redirect()->back()->with(report($e));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit Menu";
        $page_description = "Menu Edit";
        $parent  = Menu::select('id', 'parent_id', 'title')->get();
        $menu = Menu::find($id);
        $parent_info = Menu::find($menu->parent_id);
        return view('backend.system.menu.edit', compact('page_title', 'page_description', 'menu', 'parent', 'parent_info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required',
            'icon' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $menu = Menu::find($id);
        $attribute = $request->all();
        if ($icon = $request->file('icon')) {
            $destinationPath = 'uploads/menu/icon';
            $Icon = date('YmdHis') . "." . $icon->getClientOriginalExtension();
            $icon->move($destinationPath, $Icon);
            $attribute['icon'] = $Icon ?? $menu->icon;
        }
        $attribute['slug'] = $request->title;
        $attribute['parent_id'] = $request->parent_id;
        $menu->update($attribute);
        return redirect()->route('admin.menu.index')->with('success', 'Menu updated Successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Menu::find($id)->delete();
        return redirect()->back()->with('info', 'Menu deleted successfully');
    }

    public function changeStatus(Request $request)
    {
        $menu = Menu::find($request->menu_id);
        $menu->status = $request->status;
        $menu->save();
        // return response()->json(['success' => 'Status change successfully.']);
    }
}
