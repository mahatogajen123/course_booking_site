<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ServiceCategory;
use Illuminate\Http\Request;

class ServiceCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Service Category List";
        $page_description = "List Service Category";
        $search = $request->search;
        $options = ServiceCategory::getPossibleStatuses();
        // dd($options);
        $data = ServiceCategory::when($search, function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'desc')->paginate(5);
        return view('backend.system.service-category.index', compact('page_title', 'page_description', 'data', 'options'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Service Category Create";
        $page_description = "Service Category Ceate";
        return view('backend.system.service-category.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required'
        ]);
        ServiceCategory::create($request->all());
        return redirect()->back()->with('success', 'Service Category created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service_category = ServiceCategory::find($id);
        $request->validate([
            'name' => 'required',
            'status' => 'required'
        ]);
        $service_category->update($request->all());
        return redirect()->back()->with('success', 'Service Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service_category = ServiceCategory::find($id);
        $service_category->delete();
        return redirect()->back()->with('success', 'Service Category deletedf successfully');
    }
    public function changeStatus(Request $request)
    {
        $servicecategory = ServiceCategory::find($request->partner_id);
        $servicecategory->status = $request->status;
        $servicecategory->save();
    }
}
