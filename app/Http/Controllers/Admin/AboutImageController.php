<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AboutImage;
use App\Models\CompanyDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AboutImageController extends Controller
{
    public function createAboutImage()
    {
        $page_title = "Create about image";
        $page_description = "Create image";
        $images = AboutImage::get();
        return view('backend.system.about_image.create', compact('page_title', 'page_description', 'images'));
    }

    public function storeAboutImage(Request $request)
    {
        $company = CompanyDetail::find(1);
        $image = $request->file('file');
        $imageName = time() . '.' . $image->extension();
        $image->move(public_path('uploads/about_image/images'), $imageName);
        $imagePath = 'uploads/about_image/images/' . $imageName;
        $company->images()->create(['image' => $imagePath]);
        return response()->json(['success' => $imageName]);
    }
    public function destroy($id)
    {

        $image = AboutImage::find($id);
        $image->delete();
        return back()->with('success', 'Image deleted successfully');
        // return response()->json([
        //     'message' => 'Image deleted successfully!'
        // ]);
    }
}
