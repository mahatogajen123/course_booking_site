<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Lead;
use Illuminate\Http\Request;
use App\Mail\SendMail;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Lead lists";
        $page_description = "List Leads";
        $search = $request->search;
        $data = Lead::when($search, function ($q) use ($search) {
            $q->where('name', 'LIKE', '%' . $search . '%')->orwhere('email', 'LIKE', '%' . $search . '%')->orwhere('gender', 'LIKE', '%' . $search . '%')->orWhereHas('Course', function ($q) use ($search) {
                $q->where('title', 'LIKE', '%' . $search . '%');
            });
        })->orderBy('id', 'desc')->paginate(5);
        return view('backend.system.lead.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $course = Course::select('id', 'title')->get();
        $page_title = " Create Leads";
        $page_description = " Create Leads";
        return View('backend.system.lead.create', compact('page_title', 'page_description', 'course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'f_name' => 'required',
            'l_name' => 'required',
            'email' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'home_contact' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'personal_contact' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'permanent_address' => 'required',
            'current_address' => 'required',
            'course_id' => 'required',
        ]);
        $details = [
            'title' => ' Hello ' . "  " . $request->f_name . " " . " Thank you for choosing the course",
            'body' => '  I knew that looking back on the tears would make me laugh but I never knew that looking back on the laughs would bring tears.'
        ];
        $attribute = $request->all();
        $attribute['first_name'] = $request->f_name;
        $attribute['last_name'] = $request->l_name;
        Lead::create($attribute);
        \Mail::to($request->email)->send(new \App\Mail\SendMail($details));
        return redirect()->route('admin.lead.index')->with('success', 'Lead created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit Course";
        $page_description = "Course Edit";
        $course = Course::select('id', 'title')->get();
        $lead = Lead::find($id);
        return view('backend.system.lead.edit', compact('page_title', 'page_description', 'course', 'lead'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'f_name' => 'required',
            'l_name' => 'required',
            'email' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'home_contact' => 'required',
            'personal_contact' => 'required',
            'permanent_address' => 'required',
            'current_address' => 'required',
            'course_id' => 'required',
        ]);
        $lead = Lead::find($id);
        $attribute = $request->all();
        $attribute['first_name'] = $request->f_name;
        $attribute['last_name'] = $request->l_name;
        $lead->update($attribute);
        return redirect()->route('admin.lead.index')->with('success', 'lead updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lead::find($id)->delete();
        return redirect()->back()->with('success', 'Lead deleted successfully');
    }
}
