<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\CourseDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CourseDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Course Detail List";
        $page_description = "List of Course Detail";
        $search = $request->search;
        $data = CourseDetail::when($search, function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%')->orwhere('meta_title', 'LIKE', '%' . $search . '%')->orwhere('category_id', 'LIKE', '%' . $search . '%')->orWhereHas('CourseCategory', function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search . '%');
            });
        })->orderBy('id', 'desc')->paginate(5);
        return view('backend.system.course_detail.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $course = Course::select('id', 'title')->get();
        $page_title = " Add Course Detail";
        $page_description = " Add Course Detail";
        return View('backend.system.course_detail.create', compact('page_title', 'page_description', 'course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'course_id' => 'required',
            'title' => 'required|max:50',
            'description' => 'required|min:20',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->toArray());
        } else {
            $value = [
                'title' => $request->title,
                'description' => $request->description,
                'course_id' => $request->course_id,
            ];
            CourseDetail::create($value);
            return redirect()->route('admin.course.index')->with('success', 'Detail Added Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit Course";
        $page_description = "Course Edit";
        $course_detail = CourseDetail::find($id);
        $course = Course::select('id', 'title')->get();
        return view('backend.system.course_detail.edit', compact('page_title', 'page_description', 'course_detail', 'course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course_detail = CourseDetail::find($id);
        $attributes = $request->validate([
            'title' => 'required',
            'description' => 'required|min:20',
            'course_id' => 'required',
        ]);
        if ($attributes) {
            $course_detail->update($attributes);
            return redirect()->route('admin.course.index')->with('success', 'Course detail updated successfully');
        } else
            return back()->with('error', 'validation failed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course_detail = CourseDetail::find($id);
        $course_detail->delete();
        return back()->with('warning', 'Course detail deleted successfully');
    }
}
