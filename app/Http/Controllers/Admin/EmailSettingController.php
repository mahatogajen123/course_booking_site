<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmailSetting;
use Illuminate\Http\Request;
use App\Traits\SmtpSetting;
use Illuminate\Support\Facades\Mail;

class EmailSettingController extends Controller
{

    use SmtpSetting;

    public function __construct()
    {
        // $this->middleware('permission:email-setting-view', ['only' => ['index', 'update']]);

    }
    public function index()
    {
        $emailSetting = EmailSetting::find(1);
        return view('backend.system.email-setting.index', compact('emailSetting'));
    }
    public function update(Request $request)
    {
        // \Artisan::call('config:cache');
        $request->validate(
            [
                'mailer' => 'required|string',
                'host' => 'required|string',
                'port' => 'required|integer',
                'user_name' => 'required|string',
                'password' => 'required|string',
                'encryption' => 'required|string',
                'from_address' => 'required|string',
                'from_name' => 'required|string',
            ]
            // ,[
            //     'user_name.required'=>__('validation.User name is required'),
            //     'user_name.string'=>__('validation.User name should be string'),
            //     'from_address.required'=>__('validation.From Address is required'),
            //     'from_address.string'=>__('validation.From Address should be string'),
            //     'from_name.required'=>__('validation.From name is required'),
            //     'from_name.string'=>__('validation.From name should be string'),
            // ]
        );
        $emailSetting = tap(EmailSetting::find(1))->update($request->all());
        return redirect()->back()->with('emailSetting', $emailSetting)->with('success', __('message.successfully_updated'));
    }

    public function sendMail(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'to_email' => 'required|email',
            'message' => 'required'
        ]);
        $email_settings = EmailSetting::first();
        $this->mailSetting($email_settings);
        $to_email = $request->to_email;
        $data = array('email' => $request->to_email, 'body' => $request->message);
        try {
            Mail::send('mail.test_mail', $data, function ($message) use ($request, $email_settings) {
                $message->to($request->to_email)
                    ->subject('Ecom Email Test');
                $message->from($email_settings->from_address, 'Test Mail');
            });
        } catch (\Exception $e) {
            return $e->getMessage();
            return redirect()->back()->with('error', __('message.something_went_wrong'));
        }
        return redirect()->back()->with('success', __('message.test_successfull_please_check_your_mailbox'));
    }
}
