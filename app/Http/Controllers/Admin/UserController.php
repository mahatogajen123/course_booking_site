<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\user;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $role = $request->role;
        $users = User::when($search, function ($q) use ($search) {
            $q->where('full_name', 'LIKE', '%' . $search . '%')
                ->orWhere('address', 'LIKE', '%' . $search . '%')->orWhere('email', 'LIKE', '%' . $search . '%');
        })->paginate(10);
        return view('backend.system.users.index')->with('users', $users);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();
        $genders = ['male' => "Male", 'female' => 'Female', 'Others' => 'others'];
        return view('backend.system.users.create', compact('roles', 'genders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //        $this->validate($request, [
        //            'first_name' => 'required',
        //            'last_name' => 'required',
        //            'gender' => 'required',
        //            'designation',
        //            'address',
        //            'contact_number',
        //            'alternate_contact_number',
        //            'email' => 'required|unique:users,email',
        //            'roles' => 'required',
        //
        //        ]);

        $attributes = $request->all();
        $attributes['full_name'] = $request->first_name . " " . $request->last_name;
        $attributes['user_type'] = $request->roles;
        $attributes['profile_image'] = $this->uploadImage();
        if ($attributes['password']) {
            $attributes['password'] = bcrypt($request->password);
        }
        // return $request;
        $user = User::create($attributes);
        $user->assignRole($request->input('roles'));
        return redirect()->route('admin.users.index')
            ->with('success', 'Users created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VendorType  $vendorType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('backend.system.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $genders = ['male' => "Male", 'female' => 'Female', 'others' => 'others'];
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();
        return view('backend.system.users.edit', compact('user', 'roles', 'userRole', 'genders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //        $this->validate($request, [
        //            'first_name' => 'required',
        //            'last_name' => 'required',
        //            'email' => 'required',
        //            'designation' => 'required',
        //            'address' => 'required',
        //            'roles' => 'required',
        //            'password' => 'required',
        //
        //            // 'permission' => 'required',
        //        ]);
        $user = User::find($id);
        $attributes = $request->all();
        if ($user) {
            if ($request->file('image')) {
                $old_file = public_path() . $user->image;
                File::delete($old_file);
            }
            $attributes["profile_image"] = $this->uploadImage();

            if (!$attributes['password']) {
                $attributes['password'] = bcrypt('password');
            }
        }
        $attributes['user_type'] = $request->roles;

        $user->update($attributes);


        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($request->input('roles'));
        return redirect()->route('admin.users.index')
            ->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("users")->where('id', $id)->delete();
        return redirect()->route('admin.users.index')
            ->with('success', 'User deleted successfully');
    }
    public function uploadImage()
    {
        if (\Request::file('profile_image')) {
            $file = \Request::file('profile_image');
            $allowed_extensions = ["png", "jpg", "gif"];
            if ($file->getClientOriginalExtension() && !in_array($file->getClientOriginalExtension(), $allowed_extensions)) {
                return ['error' => 'You may only upload png, jpg or gif.'];
            }

            $fileName = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName      = '/uploads/user/' . date('Y', time()) . '/';
            $destinationPath = public_path() . $folderName;
            $safeName        = uniqid() . '.' . $extension;
            $file->move($destinationPath, $safeName);

            $filePath = $folderName . $safeName;
            $filename = $filePath;

            return $filename;
        } else {
            return '';
        }
    }
}
