<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CompanyDetail;
use App\Models\SocialMedia;
use Illuminate\Http\Request;

class CompanyDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Company Details";
        $page_description = "Compny Detail";
        $search = $request->search;
        $data = CompanyDetail::when($search, function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'desc')->paginate(5);
        return view('backend.system.company-detail.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Create Company Detail ";
        $page_description = "Create company detail";
        return view('backend.system.company-detail.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'slogan' => 'required',
            'copyright' => 'required',
            'about' => 'required',
            'moto' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $attribute = $request->all();
        if ($logo = $request->file('logo')) {
            $destinationPath = 'uploads/company/logo';
            $Logo = date('YmdHis') . "." . $logo->getClientOriginalExtension();
            $logo->move($destinationPath, $Logo);
            $attribute['logo'] = $Logo;
        }
        CompanyDetail::create($attribute);
        return redirect()->route('admin.company.detail.index')->with('success', 'Company detail  created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit Company Detil";
        $page_description = "Edit company Detail";
        $company = CompanyDetail::find($id);
        return view('backend.system.company-detail.edit', compact('page_title', 'page_description', 'company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = CompanyDetail::find($id);
        $request->validate([
            'title' => 'required',
            'slogan' => 'required',
            'copyright' => 'required',
            'about' => 'required',
            'moto' => 'required',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $attribute = $request->all();
        if ($logo = $request->file('logo')) {
            $destinationPath = 'uploads/company/logo';
            $Logo = date('YmdHis') . "." . $logo->getClientOriginalExtension();
            $logo->move($destinationPath, $Logo);
        }
        $attribute['logo'] = $Logo ?? $company->logo;

        $company->update($attribute);
        return redirect()->route('admin.company.detail.index')->with('success', 'Company Detail updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CompanyDetail::find($id)->delete();
        return redirect()->back()->with('info', 'Compny detail deleted');
    }
}
