<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Galary;
use Illuminate\Http\Request;


class GallaryController extends Controller
{
	public function create()
	{
		return view('backend.system.gallary.create');
	}
	public function store(Request $request)
	{
		$request->validate([
			'image' => 'required',
			'image.*' => 'mimes:jpeg,jpg,png,gif,csv,txt,pdf|max:2048'
		]);
		$image = array();
		if ($files = $request->file('image')) {
			foreach ($files as  $file) {
				$image_name = rand(200, 2000) . '.' . $file->getClientOriginalExtension();
				$image_path = 'uploads/gallary/images/';
				$image_url = $image_path . $image_name;
				$file->move($image_path, $image_name);
				$image[] = $image_url;
			}
		}

		Galary::insert([
			'image' => implode('|', $image),
		]);
		return back()->with('success', 'Images uploaded successfully');
	}

	public function show()
	{

		$data = Galary::all();
		// $images = explode('|', $data->image);
		return view('backend.system.gallary.show', compact('data'));
	}

	public function destroy($id)
	{
		$photos = Galary::find($id)->delete();
		return back()->with('warning', 'photos deleted successfully');
	}
}
