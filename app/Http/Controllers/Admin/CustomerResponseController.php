<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Enquiry;
use App\Models\GetInTouch;
use Illuminate\Http\Request;

class CustomerResponseController extends Controller
{
    public function enquiry(Request $request)
    {
        $page_title = "Customer Enquiry List";
        $page_description = " Enquiry List";
        $search = $request->search;
        $data = Enquiry::when($search, function ($q) use ($search) {
            $q->where('name', "LIKe", '%' . $search . '%')->orWhere('email', 'LIKE', '%' . $search . '%');
        })->paginate(9);
        return view('backend.system.customer-response.enquiry_list', compact('data', 'page_title', 'page_description'));
    }
    public function enquiryDelete($id)
    {
        Enquiry::find($id)->delete();
        return back()->with('success', 'Enquiry data deleted successfully');
    }

    public function getInTouch(Request $request)
    {
        $page_title = "Customer GetInTouch List";
        $page_description = "GetInTouch List";
        $search = $request->search;
        $data = GetInTouch::when($search, function ($q) use ($search) {
            $q->where('name', "LIKe", '%' . $search . '%')->orWhere('email', 'LIKE', '%' . $search . '%');
        })->paginate(9);
        return view('backend.system.customer-response.getintouch_list', compact('data', 'page_title', 'page_description'));
    }

    public function getInTouchDelete($id)
    {
        GetInTouch::find($id)->delete();
        return back()->with("success", 'Getintouch data deleted successfully');
    }
}
