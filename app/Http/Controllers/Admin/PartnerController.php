<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Partner;
use App\Models\Slider;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Partner";
        $page_description = "Partner";
        $search = $request->search;
        $data = Partner::when($search, function ($q) use ($search) {
            $q->where('name', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'desc')->paginate(5);
        return view('backend.system.partner.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = " Create Partner";
        $page_description = " Create Partner";
        return View('backend.system.partner.create', compact('page_title', 'page_description',));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'url' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $attribute = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/partner/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $attribute['image'] = "$Image";
        }
        if ($logo = $request->file('logo')) {
            $Path = 'uploads/partner/logo';
            $Logo = date('YmdHis') . "." . $logo->getClientOriginalExtension();
            $logo->move($Path, $Logo);
            $attribute['logo'] = "$Logo";
        }
        $attribute['slug'] = $request->name;
        // dd($attribute);

        Partner::create($attribute);
        return redirect()->route('admin.partner.index')->with('success', 'Partner created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit Partner";
        $page_description = "Partner Edit";
        $partner = Partner::find($id);
        return view('backend.system.partner.edit', compact('page_title', 'page_description', 'partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'url' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $partner = Partner::find($id);
        $attribute = $request->all();
        $imagePath = 'uploads/partner/images/' . $partner->image;
        $logoPath = 'uploads/partner/logo/' . $partner->logo;
        if ($request->image && !$request->logo) {
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
        } elseif ($request->logo && !$request->image) {
            if (file_exists($logoPath)) {
                unlink($logoPath);
            }
        } elseif ($request->image && $request->logo) {
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
            if (file_exists($logoPath)) {
                unlink($logoPath);
            }
        }
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/partner/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $attribute['image'] = $Image ?? $partner->image;
        }
        if ($logo = $request->file('logo')) {
            $Path = 'uploads/partner/logo';
            $Logo = date('YmdHis') . "." . $logo->getClientOriginalExtension();
            $logo->move($Path, $Logo);
            $attribute['logo'] = $Logo ?? $partner->logo;
        }
        $attribute['slug'] = $request->name;
        // dd($attribute);

        $partner->update($attribute);
        return redirect()->route('admin.partner.index')->with('success', 'Partner Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = Partner::find($id);
        $logoPath = 'uploads/partner/logo/' . $partner->logo;
        $imagePath = 'uploads/partner/images/' . $partner->image;
        if (file_exists($logoPath)) {
            unlink($logoPath);
        }
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }
        $partner->delete();
        return redirect()->back()->with('success', 'Partner deleted successfully');
    }
    public function changeStatus(Request $request)
    {
        $partner = Partner::find($request->partner_id);
        $partner->status = $request->status;
        $partner->save();
    }
}
