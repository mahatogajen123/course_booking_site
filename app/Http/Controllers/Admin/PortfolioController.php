<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Portfolio List";
        $page_description = "Portfolio List";
        $search = $request->search;
        $data = Portfolio::when($search, function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . "%")->orwhere('sub_title', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'asc')->paginate(8);
        return view('backend.system.portfolio.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = " Create Portfolio";
        $page_description = "Create Portfolio";
        return view('backend.system.portfolio.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'sub_title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpg,jpeg,png,gif,svg,jfif|max:2048'
        ]);

        $attributes = $request->all();
        if ($request->file('image')) {
            $image = $request->image;
            $imagePath = "uploads/portfolio/images";
            $imageName = date('YmdHis') . ' . ' . $image->getClientOriginalExtension();
            $image->move($imagePath, $imageName);
            $attributes['image'] = $imageName;
        }
        Portfolio::create($attributes);
        return redirect()->route('admin.portfolio.index')->with('success', 'Portfolio created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolio = Portfolio::find($id);
        $page_title = 'Edit Portfolio';
        $page_description = 'Edit Portfolio';
        return view('backend.system.portfolio.edit', compact('page_title', 'page_description', 'portfolio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $portfolio = Portfolio::find($id);
        $request->validate([
            'title' => 'required',
            'sub_title' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png,gif,svg,jfif|max:2048'
        ]);

        $attributes = $request->all();
        if ($request->file('image')) {
            $image = $request->image;
            $imagePath = "uploads/portfolio/images";
            $imageName = date('YmdHis') . ' . ' . $image->getClientOriginalExtension();
            $image->move($imagePath, $imageName);
            $attributes['image'] = $imageName ?? $portfolio->image;
        }
        $portfolio->update($attributes);
        return redirect()->route('admin.portfolio.index')->with('success', 'Portfolio updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = Portfolio::find($id);
        $imagePath = 'uploads/portfolio/images/' . $portfolio->image;

        if (file_exists($imagePath)) {
            unlink($imagePath);
        }
        $portfolio->delete();
        return redirect()->back()->with('warning', 'Portfolio deleted successfully');
    }
}
