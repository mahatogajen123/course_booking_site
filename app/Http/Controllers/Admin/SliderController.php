<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Slider List";
        $page_description = "List Slider";
        $search = $request->search;
        $data = Slider::when($search, function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'desc')->paginate(5);
        return view('backend.system.slider.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Slider Create";
        $page_description = "Slider Create";
        $course = Course::select('id', 'title')->get();
        return  view('backend.system.slider.create', compact('page_title', 'page_description', 'course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'sub_title' => 'required',
            'short_detail' => 'required',
            'url' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'course_id' => 'required'
        ]);

        $attribute = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/slider/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $attribute['image'] = "$Image";
        }
        Slider::create($attribute);
        return redirect()->route('admin.slider.index')->with('success', 'Slider created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Slider Edit";
        $page_description = "Slider Edit";
        $course = Course::select('id', 'title')->get();
        $slider = Slider::find($id);
        return view('backend.system.slider.edit', compact('page_title', 'page_description', 'slider', 'course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Slider::find($id);
        $request->validate([
            'title' => 'required',
            'sub_title' => 'required',
            'short_detail' => 'required',
            'url' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'course_id' => 'required'
        ]);
        $attribute = $request->all();
        $imagePath = 'uploads/slider/images' . $slider->image;
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/slider/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $attribute['image'] = $Image ?? $slider->image;
        }
        $slider->update($attribute);
        return redirect()->route('admin.slider.index')->with('success', 'Slider updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::find($id);
        $imagePath = 'uploads/slider/images' . $slider->image;
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }
        $slider->delete();
        return redirect()->back()->with('success', 'Slider deleted successfully');
    }
    public function changeStatus(Request $request)
    {
        $slider = Slider::find($request->slider_id);
        $slider->status = $request->status;
        $slider->save();
        // return response()->json(['success' => 'Status change successfully.']);
    }
}
