<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\Request;

class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "FAQs List";
        $page_description = "List FAQs";
        $search = $request->search;
        $data = Faq::when($search, function ($q) use ($search) {
            $q->where('question', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'desc')->paginate(5);
        return View('backend.system.faqs.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Create FAQs";
        $page_description = "FAQs Create";
        return view('backend.system.faqs.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'question' => 'required',
            'answer' => 'required'
        ]);
        Faq::create($request->all());
        return redirect()->route('admin.faqs.index')->with('success', 'FAQs created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit FAQs";
        $page_description = "FAQs Edit";
        $faq = Faq::find($id);
        return view('backend.system.faqs.edit', compact('page_title', 'page_description', 'faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'question' => 'required',
            'answer' => 'required'
        ]);
        $faq = Faq::find($id);
        $faq->update($request->all());
        return redirect()->route('admin.faqs.index')->with('success', 'FAQs updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Faq::find($id)->delete();
        return redirect()->back()->with('success', 'FAQs deleted successfully');
    }

    public function changeStatus(Request $request)
    {
        $faq = Faq::find($request->faq_id);
        $faq->status = $request->status;
        $faq->save();
    }
}
