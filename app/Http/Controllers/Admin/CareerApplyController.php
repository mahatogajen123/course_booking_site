<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CareerApply;
use Illuminate\Http\Request;

class CareerApplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Career Applied List";
        $page_description = "List Career Applied";
        $search = $request->search;
        $data = CareerApply::when($search, function ($q) use ($search) {
            $q->where('first_name', 'LIKE', '%' . $search . '%')->orwhere('email', 'LIKE', '%' . $search . '%')->orwhere('career_id', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'desc')->paginate(8);
        return view('backend.system.career-apply.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appliedCareer = CareerApply::find($id)->delete();
        return redirect()->back()->with('success', "Career has been deleted");
    }
    public function changeStatus(Request $request)
    {
        $careerApplied = CareerApply::find($request->career_applied_id);
        $careerApplied->status = $request->status;
        $careerApplied->save();
    }
}
