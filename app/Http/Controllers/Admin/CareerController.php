<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Career;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Career List";
        $page_description = "List Career  ";
        $search = $request->search;
        $data = Career::when($search, function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%')->orwhere('sub_title', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'desc')->paginate(5);
        return view('backend.system.career.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Career Create";
        $page_description = "Create Career";
        return view('backend.system.career.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'sub_title' => 'required',
            'job_type' => 'required',
            'job_level' => 'required',
            'company_name' => 'required',
            'location' => 'required',
            'offer_salary' => 'required',
            'job_create_date' => 'required',
            'deadline' => 'required',
            'short_detail' => 'required',
            'detail' => 'required',
            'responsibility' => 'required',
            'requirement' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $attribute = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/career/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $attribute['image'] = "$Image";
        }
        $attribute['slug'] = $request->title;
        Career::create($attribute);
        return redirect()->route('admin.career.index')->with('success', 'Career created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Career Edit";
        $page_description = "Edit Career";
        $career = Career::find($id);
        return view('backend.system.career.edit', compact('page_title', 'page_description', 'career'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'sub_title' => 'required',
            'job_type' => 'required',
            'job_level' => 'required',
            'company_name' => 'required',
            'location' => 'required',
            'offer_salary' => 'required',
            'job_create_date' => 'required',
            'deadline' => 'required',
            'short_detail' => 'required',
            'detail' => 'required',
            'responsibility' => 'required',
            'requirement' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $career = Career::find($id);
        $attribute = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/career/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            // $attribute['image'] = "$Image";
        }
        $attribute['slug'] = $request->title;
        $attribute['image'] = $Image ?? $career->image;
        $career->update($attribute);
        return redirect()->route('admin.career.index')->with('success', 'Career updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Career::find($id)->delete();
        return redirect()->back()->with('success', 'Career deleted successfully');
    }
    public function changeStatus(Request $request)
    {
        $career = Career::find($request->career_id);
        $career->status = $request->status;
        $career->save();
    }
}
