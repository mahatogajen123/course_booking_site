<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\ServiceCategory;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Service List";
        $page_description = "List Service";
        $search = $request->search;
        $data = Service::when($search, function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%')->orWhereHas('ServiceCategory', function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search . '%');
            });
        })->orderBy('id', 'desc')->paginate(5);
        return view('backend.system.service.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = ServiceCategory::select('id', 'name')->get();
        $page_title = " Create Service";
        $page_description = " Create Service";
        return View('backend.system.service.create', compact('page_title', 'page_description', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'sub_title' => 'required',
            'short_detail' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'icon' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $attribute = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/service/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $attribute['image'] = "$Image";
        }
        if ($icon = $request->file('icon')) {
            $Path = 'uploads/service/icon';
            $Icon = date('YmdHis') . "." . $icon->getClientOriginalExtension();
            $icon->move($Path, $Icon);
            $attribute['icon'] = "$Icon";
        }
        Service::create($attribute);
        return redirect()->route('admin.service.index')->with('success', 'service created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit Service";
        $page_description = "Service Edit";
        $servicecategory = ServiceCategory::select('id', 'name')->get();
        $service = Service::find($id);
        return view('backend.system.service.edit', compact('page_title', 'page_description', 'service', 'servicecategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'sub_title' => 'required',
            'short_detail' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'icon' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $service = Service::find($id);
        $imagePath = 'uploads/service/images/' . $service->image;
        $iconPath = 'uploads/service/icon/' . $service->icon;
        if ($request->image && !$request->icon) {
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
        } elseif ($request->icon && !$request->image) {
            if (file_exists($iconPath)) {
                unlink($iconPath);
            }
        } elseif ($request->image && $request->icon) {
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
            if (file_exists($iconPath)) {
                unlink($iconPath);
            }
        }
        $attribute = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/service/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $attribute['image'] = $Image ?? $service->image;
        }

        if ($icon = $request->file('icon')) {
            $Path = 'uploads/service/icon';
            $Icon = date('YmdHis') . "." . $icon->getClientOriginalExtension();
            $icon->move($Path, $Icon);
            $attribute['icon'] = $Icon ?? $service->icon;
        }

        $service->update($attribute);
        return redirect()->route('admin.service.index')->with('success', 'Service Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);
        $iconPath = 'uploads/service/icon/' . $service->icon;
        $imagePath = 'uploads/service/images/' . $service->image;
        if (file_exists($iconPath)) {
            unlink($iconPath);
        }
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }
        $service->delete();
        return redirect()->back()->with('success', 'Service deleted successfully');
    }
}
