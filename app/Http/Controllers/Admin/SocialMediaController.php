<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SocialMedia;
use Illuminate\Http\Request;

class SocialMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Social media Lists";
        $page_description =  "Social media list";
        $search = $request->search;
        $data = SocialMedia::when($search, function ($q) use ($search) {
            $q->where('name', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'desc')->paginate(5);
        return view('backend.system.social-media.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Create Social Media";
        $page_description = "Create Social Media";
        return view('backend.system.social-media.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'url' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $attribute = $request->all();
        if ($logo = $request->file('logo')) {
            $destinationPath = 'uploads/socialmedia/logo';
            $Logo = date('YmdHis') . "." . $logo->getClientOriginalExtension();
            $logo->move($destinationPath, $Logo);
            $attribute['logo'] = "$Logo";
        }
        SocialMedia::create($attribute);
        return redirect()->route('admin.social.media.index')->with('success', 'Social media  created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit Social Media";
        $page_description = "Edit Social Media";
        $socialmedia = SocialMedia::find($id);
        return view('backend.system.social-media.edit', compact('page_title', 'page_description', 'socialmedia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $socialmedia = SocialMedia::find($id);

        $request->validate([
            'name' => 'required',
            'url' => 'required',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $attribute = $request->all();
        // $destinationPath = 'uploads/socialmedia/logo/' . $socialmedia->logo;

        if ($logo = $request->file('logo')) {
            $destinationPath = 'uploads/socialmedia/logo';
            $Logo = date('YmdHis') . "." . $logo->getClientOriginalExtension();
            $logo->move($destinationPath, $Logo);
            $attribute['logo'] = $Logo ?? $socialmedia->logo;
        }
        $socialmedia->update($attribute);
        return redirect()->route('admin.social.media.index')->with('success', ' Social Media  updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $socialmedia = SocialMedia::find($id);
        $destinationPath = 'uploads/socialmedia/logo/' . $socialmedia->logo;
        if (file_exists($destinationPath)) {
            unlink($destinationPath);
        }
        $socialmedia->delete();
        return redirect()->back()->with('success', 'Social media deleted successfully');
    }
}
