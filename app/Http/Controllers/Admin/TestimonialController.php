<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_title = "Testimonial List";
        $page_description = "List Testimonial ";
        $search = $request->search;
        $data = Testimonial::when($search, function ($q) use ($search) {
            $q->where('person_name', 'LIKE', '%' . $search . '%')->orWhere('company_name', 'LIKE', '%' . $search . '%')->orWhere('title', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'desc')->paginate(5);
        return view('backend.system.testimonial.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = " Create Testimonial";
        $page_description = " Create Testimonail";
        return View('backend.system.testimonial.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'company_name' => 'required',
            'person_name' => 'required',
            'position' => 'required',
            'title' => 'required',
            'detail' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $attribute = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/testimonial/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $attribute['image'] = "$Image";
        }
        Testimonial::create($attribute);
        return redirect()->route('admin.testimonial.index')->with('success', 'Course created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit Course";
        $page_description = "Course Edit";
        $testimonial = Testimonial::find($id);
        return view('backend.system.testimonial.edit', compact('page_title', 'page_description', 'testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $testimonail = Testimonial::find($id);
        $request->validate([
            'company_name' => 'required',
            'person_name' => 'required',
            'position' => 'required',
            'title' => 'required',
            'detail' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $attribute = $request->all();
        if ($image = $request->file('image')) {
            $destinationPath = 'uploads/testimonial/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $attribute['image'] = $Image ?? $testimonail->image;
        }
        $testimonail->update($attribute);
        return redirect()->route('admin.testimonial.index')->with('success', 'Testimonial Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::find($id);
        $imagePath = 'uploads/testimonail/images/' . $testimonial->image;
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }
        $testimonial->delete();
        return back()->with('error', 'Testimonail deleted successfully');
    }
    public function changeStatus(Request $request)
    {
        $testimonail = Testimonial::find($request->testimonial_id);
        $testimonail->status = $request->status;
        $testimonail->save();
    }
}
