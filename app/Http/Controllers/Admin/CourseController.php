<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CourseFormRequest;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\CourseDetail;
use App\Repositories\CourseRepository;
use Illuminate\Http\Request;

class CourseController extends Controller
{

    private $courseRepository;
    function __construct(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $search = $request->search;
        $page_title = "Course List";
        $page_description = "List Course";
        $data = $this->courseRepository->allCourse($search);

        return view('backend.system.course.index', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = CourseCategory::select('id', 'name')->get();
        $page_title = " Create Course";
        $page_description = " Create Course";
        return View('backend.system.course.create', compact('page_title', 'page_description', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseFormRequest $request)
    {
        $validatedData = $request->validated();
        if ($validatedData) {
            $this->courseRepository->StoreCourses($validatedData);
        }
        return redirect()->route('admin.course.index')->with('success', 'Course created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = "Edit Course";
        $page_description = "Course Edit";
        $course = Course::find($id);
        $coursecategory = CourseCategory::select('id', 'name')->get();
        return view('backend.system.course.edit', compact('page_title', 'page_description', 'course', 'coursecategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::find($id);
        $request->validate([
            'category_id' => 'required',            
            'title' => 'required',
            'meta_title' => 'required',
            'og_meta_title' => 'required',
            'short_detail' => 'required',
            'description' => 'required',
            'duration' => 'required',
            'start_date' => 'required',
            'end_date' => 'required|after:start_date',
            'price' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
            $attribute = $request->all();
            if ($image = $request->file('image')) {
                $destinationPath = 'uploads/course/images';
                $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $Image);
                $attribute['image'] = $Image ?? $course->image;
            }
            $attribute['slug'] = $request->title;
            $course->update($attribute);
        return redirect()->route('admin.course.index')->with('success', 'Course updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        $imagePath = 'uploads/course/images/' . $course->image;
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }
        $course->delete();
        return redirect()->back()->with('success', 'Course Deleted Successsfully');
    }

    public function changeStatus(Request $request)
    {
        $course = Course::find($request->course_id);
        $course->status = $request->status;
        $course->save();
        // return response()->json(['success' => 'Status change successfully.']);
    }
}
