<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Models\Career;
use App\Models\CompanyDetail;
use App\Models\Course;
use App\Models\Menu;
use App\Models\Service;
use App\Models\SocialMedia;
use App\Models\Team;
use App\Models\Portfolio;
use App\Models\Slider;
use CreatePortfoliosTable;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware(['auth', 'verified']);
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $services = Service::select('title', 'description', 'icon',)->limit(4)->get();
        $teams = Team::select('id', 'name', 'designation', 'education', 'image', 'description')->take(4)->get();
        $portfolios = Portfolio::select('title', 'image', 'description', 'sub_title', 'id')->get();
        $first_portfolio = Portfolio::first();
        $courses = Course::select('id', 'title', 'image', 'duration')->take(8)->get();
        $sliders = Slider::select('id', 'image')->get();


        // $checkUser = [];
        // if (auth()->check())
        //     if (auth()->user()->user_type == 'user') {
        //         $checkUser['user'] = auth()->user();
        //         $checkUser['isLogin'] = true;
        //     } else {
        //         $checkUser['isLogin'] = false;
        //     }
        // else
        //     $checkUser['isLogin'] = false;
        return view(
            'frontend.themes.course_booking_site.homepage',
            compact('services', 'teams', 'portfolios', 'first_portfolio', 'courses', 'sliders')
        );
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('home');
    }
    public function careerpage()
    {
        $careers = Career::get();
        return view('frontend.themes.career.career', compact('careers'));
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $data = Course::when($search, function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%');
        })->orderBy('id', 'desc')->paginate(5);
        if ($data->isEmpty()) {
            return redirect()->route('course.search')->with('message', 'We could not find the course named ' . $search . ' you are searching for.');
        } else {
            return view('frontend.themes.course_booking_site.components.search', compact('data'));
        }
    }
}
