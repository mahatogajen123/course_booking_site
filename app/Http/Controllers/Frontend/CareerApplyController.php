<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Mail\CareerMail;
use App\Models\Career;
use App\Models\CareerApply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CareerApplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $careers = Career::get();
        return view('frontend.themes.career.career', compact('careers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function apply(Request $request)
    {
        $career = Career::where('slug', $request->careerform_slug)->first();
        return view('frontend.themes.course_booking_site.components.careerform', compact('career'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'contact' => 'required|numeric',
            'portfolio_url' => 'required|url',
            'resume' => 'required|mimes:pdf,doc,docx|max:5000',
            'about' => 'required|min:20|max:200',

        ]);
        $attribute = $request->all();
        $attribute['career_id'] = $id;
        if ($file = $request->file('resume')) {
            $Path = 'uploads/appliedCareer/resume';
            $File = $request->name . "." . $file->getClientOriginalExtension();
            $file->move($Path, $File);
            $attribute['resume'] = $File ?? null;
        }

        $details = [
            'title' => ' Hello ' . "  " . $request->name . " " . " Thank you for choosing the Career",
            'body' => 'You  will be informed after you havebeen short listed for the post  ',
            'url' => 'https://www.eeeinnovation.com/',
        ];
        CareerApply::create($attribute);
        Mail::to($request->email)->send(new CareerMail($details));
        return redirect()->route('course.booking.site.career')->with('success', 'Applied for the choosen career');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function view(Request $request)
    {
        $career = Career::where('slug', $request->career)->first();
        return view('frontend.themes.career.careerview', compact('career'));
    }
}
