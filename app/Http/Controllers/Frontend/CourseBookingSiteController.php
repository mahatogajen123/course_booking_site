<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Mail\EnquiryMail;
use App\Models\Blog;
use App\Models\Career;
use App\Models\Course;
use App\Models\Enquiry;
use App\Models\GetInTouch;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\CompanyDetail;
use Illuminate\Support\Facades\Validator;

class CourseBookingSiteController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function individual_career(Request $request)
    {
        $career = Career::where('slug', $request->career)->first();


        return view('frontend.themes.course_booking_site.components.individual_career', compact('career',));
    }


    public function career(Request $request)
    {
        $careers = Career::paginate(4);
        if ($request->ajax()) {
            $view = view('frontend.themes.course_booking_site.components.careers_data', compact('careers'))->render();
            return response()->json(['html' => $view]);
        }
        return view('frontend.themes.course_booking_site.components.career', compact('careers'));
    }
    public function courses(Request $request)
    {
        $courses = Course::select('id', 'title', 'image', 'duration', 'slug')->paginate(4);
        if ($request->ajax()) {
            $view = view('frontend.themes.course_booking_site.components.course_data', compact('courses'))->render();
            return response()->json(['html' => $view]);
        }
        return view('frontend.themes.course_booking_site.components.courses', compact('courses'));
    }

    public function individual_course(Request $request)
    {
        $course = Course::where('slug', $request->course_slug)->first();
        $courses = Course::select('id', 'title')->get();
        return view('frontend.themes.course_booking_site.components.individual_course', compact('course', 'courses'))->with('success', 'Enquiry submited');
    }
    public function about()
    {
        $company_details = CompanyDetail::first();
        return view('frontend.themes.course_booking_site.components.about', compact('company_details'));
    }

    public function team()
    {
        $teams = Team::get();
        return view('frontend.themes.course_booking_site.components.team', compact('teams'));
    }
    public function contact()
    {
        return view('frontend.themes.course_booking_site.components.contact');
    }
    public function enquiry()
    {
        $courses = Course::select('title')->get();
        return view('frontend.themes.course_booking_site.components.enquiry', compact('courses'));
    }
    public function enquiryStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'contact' => 'required',
            'course' => 'required',
            'message' => 'required'
        ]);
        if ($validator->fails()) {
            if ($request->expectsJson())
                return response()->json(['status' => 0, 'error' => $validator->errors()->toArray()]);
            else
                return back()->withErrors($validator->errors());
        } else {
            $values = [
                'name' => $request->name,
                'email' => $request->email,
                'contact' => $request->contact,
                'course' => $request->course,
                'optional_course' => $request->optional_course,
                'message' => $request->message,
            ];

            $enquiry = Enquiry::create($values);
            $details = [
                'title' => 'Hello' . " " .  $request->name . " " . 'your enquiry has been received. you will be shortly informed about your queries',
                'body' => "if you are interested  please visit our office",
                'url' => 'https://www.eeeinnovation.com/',
            ];
            if ($enquiry) {
                Mail::to($request->email)->send(new EnquiryMail($details));
                if ($request->expectsJson())
                    return response()->json(['status' => 1, 'msg' => 'Enquiry has been successfully sent']);
                else
                    return back()->with('success', 'Your enquiry has been sent');
            }
        }
    }
    public function blog()
    {
        $blog = Blog::select('image', 'title', 'detail', 'created_at')->get();
        $latest_blog = Blog::latest()->orderBy('id', 'desc')->take(3)->get();
        return view('frontend.themes.course_booking_site.components.blog', compact('blog', 'latest_blog'));
    }
    public function individual_blog(Request $request)
    {
        $blog = Blog::where('title', $request->slug)->first();
        $latest_blogs = Blog::latest()->orderBy('id', 'desc')->take(3)->get();
        $search = $request->search;
        $data = Blog::when($search, function ($query) use ($search) {
            $query->where('title', 'LIKE', '%' . $search . '%');
        });
        return view('frontend.themes.course_booking_site.components.individual_blog', compact('blog', 'latest_blogs', 'data'));
    }

    public function getInTouch(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'contact' => 'required|min:10|numeric',
            'message' => 'required|min:20|max:200',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0, 'error' => $validator->errors()->toArray()]);
        } else {
            $values = [
                'name' => $request->name,
                'email' => $request->email,
                'contact' => $request->contact,
                'message' => $request->message,
            ];
            $getintouch = GetInTouch::create($values);
            if ($getintouch) {
                return response()->json(['status' => 1, 'msg' => 'Thanks for contacting us']);
            }
        }
    }
}
