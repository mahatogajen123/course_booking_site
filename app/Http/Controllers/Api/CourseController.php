<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use  illuminate\Support\Facades\DB;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course = Course::all();
        if ($course) {
            return response()->json([
                'syccess' => true,
                'message' => "Course list are listed below",
                'data' => $course
            ], 201);
        }
        return response('No data found');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'title' => 'required',
            'meta_title' => 'required',
            'og_meta_title' => 'required',
            'short_detail' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error', $validator->errors());
        } else {
            if ($image = $request->file('image')) {
                $destinationPath = 'uploads/course/images';
                $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $Image);
                $input['image'] = $Image ?? $request->image;
            }
            $input['slug'] = $request->title;
            // $input['category_id'] = $request->category_id;
        }
        $course = Course::create($input);
        return response()->json([
            'success' => true,
            'message' => "course created successfully",
            'data' => $course
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::find($id);
        $input = $request->all();
        // return response([$input, $course]);
        $validator = Validator::make($input, [
            'title' => 'required',
            // 'meta_title' => 'required',
            // 'og_meta_title' => 'required',
            // 'short_detail' => 'required',
            // 'description' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        } else {
            if ($image = $request->file('image')) {
                $destinationPath = 'uploads/course/images';
                $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $Image);
                $attribute['image'] = $Image ?? $course->image;
            }
            $course['slug'] = $input['title'];
            // $input['category_id'] = $request->category_id;
        }
        $course->update($input);
        return response()->json([
            'success' => true,
            'message' => "course updated successfully",
            'data' => $course
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $input = Course::find($id)->delete();
        return response()->json(['message' => 'Course ' . ' ' . $id . ' ' . "deleted successsfully"]);
    }
}
