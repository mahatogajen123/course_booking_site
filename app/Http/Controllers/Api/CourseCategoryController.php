<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CourseCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CourseCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $CourseCategory = CourseCategory::all();
        return response()->json(['CourseCategory' => $CourseCategory], 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make(
            $input,
            [
                'name' => 'required'
            ]
        );
        if ($validator->fails()) {
            return $this->sendError('Validation Error', $validator->errors());
        }
        $CourseCategory = CourseCategory::create($input);
        return response()->json([
            'success' => true,
            'message' => 'Course Category Created successfully',
            'data' => $CourseCategory
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $CourseCategory = CourseCategory::find($id);
        if (is_null($CourseCategory)) {
            return $this->sendError('Category not found');
        }
        return response()->json([
            'success' => true,
            'message' => 'Category Shown successfully',
            'data' => $CourseCategory
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Category = CourseCategory::find($id);
        $input = $request->all();
        $validator = Validator::make(
            $input,
            [
                'name' => 'required'
            ]
        );
        if ($validator->fails()) {
            return $this->sendError('Validation Error', $validator->errors());
        }
        $Category->update($input);
        return response()->json([
            'success' => true,
            'message' => 'Course Category updated successfully',
            'data' => $Category
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $CourseCategory = CourseCategory::find($id)->delete();
        return response()->json(['success'=>true,
        'message'=>'Course category deleted successfully',
    'id'=>CourseCategory::find($id)]);
    }
}
