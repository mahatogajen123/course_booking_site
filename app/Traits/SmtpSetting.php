<?php

namespace App\Traits;

trait SmtpSetting{
    public function mailSetting($email_settings)
    {
        $config = array(
            'driver'     =>     $email_settings['mailer'],
            'host'       =>     $email_settings['host'],
            'port'       =>     $email_settings['port'],
            'username'   =>     $email_settings['user_name'],
            'password'   =>     $email_settings['password'],
            'encryption' =>     $email_settings['encryption'],
            'from'       =>     $email_settings['from_address'],
        );
        \Config::set('mail', $config);

    }
}
