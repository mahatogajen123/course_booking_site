<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            return false;
        });
    }
    public function render($request, Throwable $e)
    {

        if ($this->isHttpException($e)) {
            $code=$e->getStatusCode();
            $message='';
            if ($code == 404) {
                $message='Address Not Found';
                return response()->view('interface.errors.' . 'error',compact('code','message'));
            }
            if ($code == 400) {
                $message='Bad Request';
                return response()->view('interface.errors.' . 'error',compact('code','message'));
            }
            if ($code == 401) {
                $message='Unauthorized';
                return response()->view('interface.errors.' . 'error',compact('code','message'));
            }
            if ($code == 405) {
                $message='Method Not Allowed';
                return response()->view('interface.errors.' . 'error',compact('code','message'));
            }
            if ($code == 500) {
                $message='Internal Server Error';
                return response()->view('interface.errors.' . 'error',compact('code','message'));
            }
            if ($code == 502) {
                $message='Bad Gateway';
                return response()->view('interface.errors.' . 'error',compact('code','message'));
            }
        }

        return parent::render($request, $e);
    }

}
