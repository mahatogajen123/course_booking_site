<?php

namespace App\Repositories;

interface CourseRepositoryInterface
{
    public function allCourse($request);
    public function StoreCourses(array $data);
}
