<?php

namespace App\Repositories;

use App\Http\Requests\CourseFormRequest;
use App\Models\Course;

class CourseRepository implements CourseRepositoryInterface
{
    public function allCourse($search)
    {
        return Course::when($search, function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%')->orwhere('meta_title', 'LIKE', '%' . $search . '%')->orwhere('category_id', 'LIKE', '%' . $search . '%')->orWhereHas('CourseCategory', function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search . '%');
            });
        })->orderBy('id', 'desc')->paginate(5);
    }
    public function StoreCourses(array $data)
    {
        if ($image = $data['image']) {
            $destinationPath = 'uploads/course/images';
            $Image = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $Image);
            $data['image'] = $Image;
        }
        $data['slug'] = $data['title'];

        return Course::create($data);
    }
}
