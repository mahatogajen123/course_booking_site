<?php

use App\Http\Controllers\Admin\BlogCategoryController;
use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\PartnerController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\CareerController;
use App\Http\Controllers\Admin\FaqsController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\ServiceCategoryController;
use App\Http\Controllers\Admin\TestimonialController;
use App\Http\Controllers\Frontend\CareerApplyController;
use App\Http\Controllers\Frontend\CourseBookingSiteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('backend.system.welcome');
//});
Route::get('course-status', [CourseController::class, 'changeStatus']);
Route::get('partner-status', [PartnerController::class, 'changeStatus']);
Route::get('slider-status', [SliderController::class, 'changeStatus']);
Route::get('blog-status', [BlogController::class, 'changeStatus']);
Route::get('career-status', [CareerController::class, 'changeStatus']);
Route::get('career-apply-status', [CareerApplyController::class, 'changeStatus']);
Route::get('faqs-status', [FaqsController::class, 'changeStatus']);
Route::get('menu-status', [MenuController::class, 'changeStatus']);
Route::get('blogcategory-status', [BlogCategoryController::class, 'changeStatus']);
Route::get('servicecategory-status', [ServiceCategoryController::class, 'changeStatus']);
Route::get('testimonial-status', [TestimonialController::class, 'changeStatus']);


// Route::get('profile', ['as' => 'profile', 'uses' => 'HomeController@profile']);
// Route::get('email', [HomeController::class, 'sendMail']);

Route::get('language/{lang}', function ($lang) {
    session()->put('locale', $lang);
    return redirect()->back();
})->name('change.locale');


// Route::resources('career-apply', CareerApplyController::class);


Route::prefix('admin')->group(function () {
    Route::get('login', [AuthController::class, 'adminLoginIndex'])->name('admin.login.index');
    Route::post('login', [AuthController::class, 'login'])->name('admin.login');
    Route::post('logout', [AuthController::class, 'logout'])->name('admin.logout');
});
//Route::get('home', ['as' => 'home','uses' => 'HomeController@index']);
Route::group(['middleware' => ['auth']], function () {
    Route::prefix('admin')->as('admin.')->group(function () {

        Route::resource('roles', Rolecontroller::class);
        Route::get('roles/delete/{id}', [RoleController::class, 'destroy'])->name('roles.delete');
        Route::resource('users', Usercontroller::class);
        Route::get('users/delete/{id}', [UserController::class, 'destroy'])->name('users.delete');
        Route::group(['namespace' => 'App\Http\Controllers\Admin'], function () {
            Route::get('board', ['as' => 'home', 'uses' => 'HomeController@index']);

            //email setting route
            Route::get('email-setting', ['as' => 'email-setting.index', 'uses' => "EmailSettingController@index"]);
            Route::post('email-setting', ['as' => 'email-setting.update', 'uses' => 'EmailSettingController@update']);
            Route::post('send-test-mail', ['as' => 'email-setting.sendMail', 'uses' => 'EmailSettingController@sendMail']);

            //course controller
            Route::get('course', ['as' => 'course.index', 'uses' => 'CourseController@index']);
            Route::get('course-create', ['as' => 'course.create', 'uses' => 'CourseController@create']);
            Route::post('course-store', ['as' => 'course.store', 'uses' => 'CourseController@store']);
            Route::get('course-edit/{id}', ['as' => 'course.edit', 'uses' => 'CourseController@edit']);
            Route::post('course-update/{id}', ['as' => 'course.update', 'uses' => 'CourseController@update']);
            Route::get('course-delete/{id}', ['as' => 'course.delete', 'uses' => 'CourseController@destroy']);

            //course detail controller router
            Route::group(['prefix' => 'course-detail'], function () {
                Route::get('', ['as' => 'course.detail.index', 'uses' => 'CourseDetailController@index']);
                Route::get('create', ['as' => 'course.detail.create', 'uses' => 'CourseDetailController@create']);
                Route::post('store', ['as' => 'course.detail.store', 'uses' => 'CourseDetailController@store']);
                Route::get('edit/{id}', ['as' => 'course.detail.edit', 'uses' => 'CourseDetailController@edit']);
                Route::post('update/{id}', ['as' => 'course.detail.update', 'uses' => 'CourseDetailController@update']);
                Route::get('delete/{id}', ['as' => 'course.detail.delete', 'uses' => 'CourseDetailController@destroy']);
            });


            //partner controller 
            Route::get('partner', ['as' => 'partner.index', 'uses' => 'PartnerController@index']);
            Route::get('partner-create', ['as' => 'partner.create', 'uses' => 'PartnerController@create']);
            Route::post('partner-store', ['as' => 'partner.store', 'uses' => 'PartnerController@store']);
            Route::get('partner-edit/{id}', ['as' => 'partner.edit', 'uses' => 'PartnerController@edit']);
            Route::post('partner-update/{id}', ['as' => 'partner.update', 'uses' => 'PartnerController@update']);
            Route::get('partner-delete/{id}', ['as' => 'partner.delete', 'uses' => 'PartnerController@destroy']);

            //course categogry
            Route::get('course-category', ['as' => 'course.category.index', 'uses' => 'CourseCategoryController@index']);
            Route::get('course-category-create', ['as' => 'course.category.create', 'uses' => 'CourseCategoryController@create']);
            Route::post('course-category-store', ['as' => 'course.category.store', 'uses' => 'CourseCategoryController@store']);
            Route::post('course-category-update/{id}', ['as' => 'course.category.update', 'uses' => 'CourseCategoryController@update']);
            Route::get('course-category-delete/{id}', ['as' => 'course.category.delete', 'uses' => 'CourseCategoryController@destroy']);

            //slider
            Route::get('slider', ['as' => 'slider.index', 'uses' => 'SliderController@index']);
            Route::get('slider-create', ['as' => 'slider.create', 'uses' => 'SliderController@create']);
            Route::post('slider-store', ['as' => 'slider.store', 'uses' => 'SliderController@store']);
            Route::get('slider-edit/{id}', ['as' => 'slider.edit', 'uses' => 'SliderController@edit']);
            Route::post('slider-update/{id}', ['as' => 'slider.update', 'uses' => 'SliderController@update']);
            Route::get('slider-delete/{id}', ['as' => 'slider.delete', 'uses' => 'SliderController@destroy']);


            //Blog category route
            Route::get('blog-category', ['as' => 'blog.category.index', 'uses' => 'BlogCategoryController@index']);
            Route::get('blog-category-create', ['as' => 'blog.category.create', 'uses' => 'BlogCategoryController@create']);
            Route::post('blog-category-store', ['as' => 'blog.category.store', 'uses' => 'BlogCategoryController@store']);
            Route::post('blog-category-update/{id}', ['as' => 'blog.category.update', 'uses' => 'BlogCategoryController@update']);
            Route::get('blog-category-delete/{id}', ['as' => 'blog.category.delete', 'uses' => 'BlogCategoryController@destroy']);

            //blog route
            Route::get('blog', ['as' => 'blog.index', 'uses' => 'BlogController@index']);
            Route::get('blog-create', ['as' => 'blog.create', 'uses' => 'BlogController@create']);
            Route::post('blog-store', ['as' => 'blog.store', 'uses' => 'BlogController@store']);
            Route::get('blog-edit/{id}', ['as' => 'blog.edit', 'uses' => 'BlogController@edit']);
            Route::post('blog-update/{id}', ['as' => 'blog.update', 'uses' => 'BlogController@update']);
            Route::get('blog-delete/{id}', ['as' => 'blog.delete', 'uses' => 'BlogController@destroy']);

            //Career route
            Route::get('career', ['as' => 'career.index', 'uses' => 'CareerController@index']);
            Route::get('career-create', ['as' => 'career.create', 'uses' => 'CareerController@create']);
            Route::post('career-store', ['as' => 'career.store', 'uses' => 'CareerController@store']);
            Route::get('career-edit/{id}', ['as' => 'career.edit', 'uses' => 'CareerController@edit']);
            Route::post('career-update/{id}', ['as' => 'career.update', 'uses' => 'CareerController@update']);
            Route::get('career-delete/{id}', ['as' => 'career.delete', 'uses' => 'CareerController@destroy']);

            // faqs route 
            Route::get('faqs', ['as' => 'faqs.index', 'uses' => 'FaqsController@index']);
            Route::get('faqs-create', ['as' => 'faqs.create', 'uses' => 'FaqsController@create']);
            Route::post('faqs-store', ['as' => 'faqs.store', 'uses' => 'FaqsController@store']);
            Route::get('faqs-edit/{id}', ['as' => 'faqs.edit', 'uses' => 'FaqsController@edit']);
            Route::post('faqs-update/{id}', ['as' => 'faqs.update', 'uses' => 'FaqsController@update']);
            Route::get('faqs-delete/{id}', ['as' => 'faqs.delete', 'uses' => 'FaqsController@destroy']);


            //menu web route

            Route::get('menu', ['as' => 'menu.index', 'uses' => 'MenuController@index']);
            Route::get('menu/create', ['as' => 'menu.create', 'uses' => 'MenuController@create']);
            Route::post('menu/store', ['as' => 'menu.store', 'uses' => 'MenuController@store']);
            Route::get('menu/edit/{id}', ['as' => 'menu.edit', 'uses' => 'MenuController@edit']);
            Route::post('menu/update/{id}', ['as' => 'menu.update', 'uses' => 'MenuController@update']);
            Route::get('menu-delete/{id}', ['as' => 'menu.delete', 'uses' => 'MenuController@destroy']);

            //leads route for student enrolled in course 
            Route::get('lead', ['as' => 'lead.index', 'uses' => 'LeadController@index']);
            Route::get('lead/create', ['as' => 'lead.create', 'uses' => 'LeadController@create']);
            Route::post('lead/store', ['as' => 'lead.store', 'uses' => 'LeadController@store']);
            Route::get('lead/edit/{id}', ['as' => 'lead.edit', 'uses' => 'LeadController@edit']);
            Route::post('lead/update/{id}', ['as' => 'lead.update', 'uses' => 'LeadController@update']);
            Route::get('lead-delete/{id}', ['as' => 'lead.delete', 'uses' => 'LeadController@destroy']);
            // route for company details
            Route::get('company-detail', ['as' => 'company.detail.index', 'uses' => 'CompanyDetailController@index']);
            Route::get('company-detail/create', ['as' => 'company.detail.create', 'uses' => 'CompanyDetailController@create']);
            Route::post('company-detail/store', ['as' => 'company.detail.store', 'uses' => 'CompanyDetailController@store']);
            Route::get('company-detail/edit/{id}', ['as' => 'company.detail.edit', 'uses' => 'CompanyDetailController@edit']);
            Route::post('company-detail/update/{id}', ['as' => 'company.detail.update', 'uses' => 'CompanyDetailController@update']);
            Route::get('company-detail/delete/{id}', ['as' => 'company.detail.delete', 'uses' => 'CompanyDetailController@destroy']);
            // route for social media
            Route::get('social-media', ['as' => 'social.media.index', 'uses' => 'SocialMediaController@index']);
            Route::get('social-media/create', ['as' => 'social.media.create', 'uses' => 'SocialMediaController@create']);
            Route::post('social-media/store', ['as' => 'social.media.store', 'uses' => 'SocialMediaController@store']);
            Route::get('social-media/edit/{id}', ['as' => 'social.media.edit', 'uses' => 'SocialMediaController@edit']);
            Route::post('social-media/update/{id}', ['as' => 'social.media.update', 'uses' => 'SocialMediaController@update']);
            Route::get('social-media/delete/{id}', ['as' => 'social.media.delete', 'uses' => 'SocialMediaController@destroy']);
            // route for Service Category
            Route::get('service-category', ['as' => 'service.category.index', 'uses' => 'ServiceCategoryController@index']);
            Route::get('service-category/create', ['as' => 'service.category.create', 'uses' => 'ServiceCategoryController@create']);
            Route::post('service-category/store', ['as' => 'service.category.store', 'uses' => 'ServiceCategoryController@store']);
            Route::get('service-category/edit/{id}', ['as' => 'service.category.edit', 'uses' => 'ServiceCategoryController@edit']);
            Route::post('service-category/update/{id}', ['as' => 'service.category.update', 'uses' => 'ServiceCategoryController@update']);
            Route::get('service-category/delete/{id}', ['as' => 'service.category.delete', 'uses' => 'ServiceCategoryController@destroy']);
            // route for service
            Route::get('service', ['as' => 'service.index', 'uses' => 'ServiceController@index']);
            Route::get('service/create', ['as' => 'service.create', 'uses' => 'ServiceController@create']);
            Route::post('service/store', ['as' => 'service.store', 'uses' => 'ServiceController@store']);
            Route::get('service/edit/{id}', ['as' => 'service.edit', 'uses' => 'ServiceController@edit']);
            Route::post('service/update/{id}', ['as' => 'service.update', 'uses' => 'ServiceController@update']);
            Route::get('service/delete/{id}', ['as' => 'service.delete', 'uses' => 'ServiceController@destroy']);
            // route for team
            Route::get('team', ['as' => 'team.index', 'uses' => 'TeamController@index']);
            Route::get('team/create', ['as' => 'team.create', 'uses' => 'TeamController@create']);
            Route::post('team/store', ['as' => 'team.store', 'uses' => 'TeamController@store']);
            Route::get('team/edit/{id}', ['as' => 'team.edit', 'uses' => 'TeamController@edit']);
            Route::post('team/update/{id}', ['as' => 'team.update', 'uses' => 'TeamController@update']);
            Route::get('team/delete/{id}', ['as' => 'team.delete', 'uses' => 'TeamController@destroy']);

            //route for career applied
            Route::get('career-applied', ['as' => 'career.applied.index', 'uses' => 'CareerApplyController@index']);
            Route::get('career-applied/delete/{id}', ['as' => 'career.applied.delete', 'uses' => 'CareerApplyController@destroy']);

            //route for portfoli 
            Route::get('portfolio', ['as' => 'portfolio.index', 'uses' => 'PortfolioController@index']);
            Route::get('portfolio/create', ['as' => 'portfolio.create', 'uses' => 'PortfolioController@create']);
            Route::post('portfolio/store', ['as' => 'portfolio.store', 'uses' => 'PortfolioController@store']);
            Route::get('portfolio/edit/{id}', ['as' => 'portfolio.edit', 'uses' => 'PortfolioController@edit']);
            Route::post('portfolio/update/{id}', ['as' => 'portfolio.update', 'uses' => 'PortfolioController@update']);
            Route::get('portfolio/delete/{id}', ['as' => 'portfolio.delete', 'uses' => 'PortfolioController@destroy']);

            //customer response route
            Route::group(['prefix' => 'customer/'], function () {
                Route::get('enquiry', ['as' => 'customer.enquiry', 'uses' => 'CustomerResponseController@enquiry']);
                Route::get('enquiry/delete{id}', ['as' => 'customer.enquiry.delete', 'uses' => 'CustomerResponseController@enquiryDelete']);
                Route::get('getintouch', ['as' => 'customer.getintouch', 'uses' => 'CustomerResponseController@getInTouch']);
                Route::get('getintouch/delete/{id}', ['as' => 'customer.getintouch.delete', 'uses' => 'CustomerResponseController@getInTouchDelete']);
            });
            Route::group(['prefix' => 'photo-upload'], function () {
                Route::get('', ['as' => 'photo.create', 'uses' => 'GallaryController@create']);
                Route::post('/store', ['as' => 'photo.store', 'uses' => 'GallaryController@store']);
                Route::get('/show', ['as' => 'photo.show', 'uses' => 'GallaryController@show']);
                Route::get('/delete/{id}', ['as' => 'photo.delete', 'uses' => 'GallaryController@destroy']);
            });
            //testimonial route
            Route::group(['prefix' => 'testimonail'], function () {
                Route::get('portfolio', ['as' => 'testimonial.index', 'uses' => 'TestimonialController@index']);
                Route::get('/create', ['as' => 'testimonial.create', 'uses' => 'TestimonialController@create']);
                Route::post('/store', ['as' => 'testimonial.store', 'uses' => 'TestimonialController@store']);
                Route::get('/edit/{id}', ['as' => 'testimonial.edit', 'uses' => 'TestimonialController@edit']);
                Route::post('/update/{id}', ['as' => 'testimonial.update', 'uses' => 'TestimonialController@update']);
                Route::get('/delete/{id}', ['as' => 'testimonial.delete', 'uses' => 'TestimonialController@destroy']);
            });
            Route::get('about-image/create', ['as' => 'about.image.create', 'uses' => 'AboutImageController@createAboutImage']);
            Route::post('about-image/store', ['as' => 'about.image.store', 'uses' => 'AboutImageController@storeAboutImage']);
            Route::get('about-image/delete/{id}', ['as' => 'about.image.delete', 'uses' => 'AboutImageController@destroy']);
        });
    });


    Route::get('admin', function () {
        return redirect()->route('admin.login.index');
    })->where('admin', '.*');
});


//frontend  web routes

Route::get(' ', [HomeController::class, 'index'])->name('homepage');
Route::get('/search', [HomeController::class, 'search'])->name('course.search');
//course booking sites route
Route::get('course-booking/', function () {
    return view('frontend.themes.course-booking-site.homepage');
});
Route::group(['namespace' => 'App\Https\Controllers\Frontend'], function () {
    Route::get('career/career-apply-form', [CareerApplyController::class, 'apply'])->name('course.booking.site.career.apply.form');
    Route::post('career-form-store/{id}', [CareerApplyController::class, 'store'])->name('course.booking.site.career.store');
    Route::get('career', [CourseBookingSiteController::class, 'career'])->name('course.booking.site.career');
    Route::get('career/individual-career', [CourseBookingSiteController::class, 'individual_career'])->name('course.booking.site.individual.career');
    Route::get('courses', [CourseBookingSiteController::class, 'courses'])->name('course.booking.site.courses');
    Route::get('courses/individual-course', [CourseBookingSiteController::class, 'individual_course'])->name('course.booking.site.individual.course');
    Route::get('about', [CourseBookingSiteController::class, 'about'])->name('course.booking.site.about');
    Route::get('team', [CourseBookingSiteController::class, 'team'])->name('course.booking.site.team');
    Route::get('contact', [CourseBookingSiteController::class, 'contact'])->name('course.booking.site.contact');
    Route::get('enquiry', [CourseBookingSiteController::class, 'enquiry'])->name('course.booking.site.enquiry');
    Route::post('enquiry/store', [CourseBookingSiteController::class, 'enquiryStore'])->name('course.booking.site.enquiry.store');
    Route::get('blog', [CourseBookingSiteController::class, 'blog'])->name('course.booking.site.blog');
    Route::get('blog/individual-blog', [CourseBookingSiteController::class, 'individual_blog'])->name('course.booking.site.individual.blog');
    Route::post('get-in-touch', [CourseBookingSiteController::class, 'getInTouch'])->name('course.booking.site.getintouch');
});
