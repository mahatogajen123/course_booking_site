<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BlogCategoryController;
use App\Http\Controllers\Api\BlogController;
use App\Http\Controllers\Api\CareerController;
use App\Http\Controllers\Api\CourseCategoryController;
use App\Http\Controllers\Api\CourseController;
use App\Http\Controllers\Api\PartnerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Public routes
//Route::post('/provider/register', [AuthController::class, 'register']);
//Route::post('/provider/login', [AuthController::class, 'login']);
Route::post('/user/register', [AuthController::class, 'register']);
Route::post('/user/login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    // Route::post('/update/user', [AuthController::class, 'updateUser']);
    // Route::post('/update/user/{id}', [AuthController::class, 'updateUser']);
    Route::post('/user/logout', [AuthController::class, 'logout']);

    Route::prefix('user')->as('user.')->group(function () {
        //course category
        Route::get('course-category', [CourseCategoryController::class, 'index']);
    });
});

//course category api route
Route::get('course-category', [CourseCategoryController::class, 'index']);
// Route::post('course-category/store', [CourseCategoryController::class, 'store']);
// Route::put('course-category/update/{id}', [CourseCategoryController::class, 'update']);
// Route::delete('course-category/delete/{id}', [CourseCategoryController::class, 'destroy']);

//course Api route 
Route::get('course', [CourseController::class, 'index']);
// Route::post('course/store', [CourseController::class, 'store']);
// Route::post('course/update/{id}', [CourseController::class, 'update']);
// Route::delete('course/delete/{id}', [CourseController::class, 'destroy']);

//Blog Category api route

Route::get('blog-category', [BlogCategoryController::class, 'index']);


//blog list

Route::get('blog', [BlogController::class, 'index']);

//Career api Route
Route::get('career', [CareerController::class, 'index']);

//partner route
Route::get('partner', [PartnerController::class, 'index']);
