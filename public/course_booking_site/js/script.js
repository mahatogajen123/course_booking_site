$(window).scroll(function () {
    if ($(window).scrollTop() > 450) {
        $('#navbar2').show(700)
        $('#navbar-example').hide(1500)
    }
    else {
        $('#navbar2').hide(900)
        $('#navbar-example').show(500)
    }
    if ($(window).scrollTop() > 700) {
        $('.arrow-button').show(700)
    }
    else {
        $('.arrow-button').hide(300)
    }
});
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
function show() {
    $(".less").toggle();
    $(".more").toggle();
}
$(function () {
    $('.expandallbutton').on('click', function (e) {
        $('.accordion-collapse').toggleClass('show');
        $('.accordion-button').toggleClass('collapsed');
    })
});
function individualclosed() {
    if ($('.js .show').length < 1) {
        $('.expandallbutton').addClass('collapsed');
    }
    // console.log($('.js .show').length);
    // console.log($('.accordion-collapse').length);

    // if( $('.js .show').length  1 ){
    //     console.log($('.js .show').length);
    //   $('.expandallbutton').addClass('collapsed');        
    // }
}
var url = window.location;
// for sidebar menu entirely but not cover treeview
$('.navbar-light .navbar-nav .nav-link').filter(function () {
    var active = url.href.split('/');
    var href = this.href.split('/');
    return href[3] == active[3];
}).addClass('active');

//enquiry js here
$(document).ready(function () {
    $('.enquiry-form').on('submit', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: $(this).attr('action'),
            type: "POST",
            data: $(this).serialize(),
            beforeSend: function () {
                $(document).find('span.error-text').text('');
            },
            success: function (data) {
                if (data.status == 0) {
                    $.each(data.error, function (prefix, val) {
                        $('span.' + prefix + '_error').text(val[0]);
                    });
                } else {
                    $('.enquiry-form')[0].reset();
                    alert(data.msg);
                }
            }
        });
    });
});


//get in touch jquery and ajax call

$(document).ready(function () {
    $('#get_in_touch').on('submit', function (e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: $(this).attr('action'),
            type: "POST",
            data: $(this).serialize(),
            beforeSend: function () {
                $(document).find('span.error-text').text('');
            },
            success: function (data) {
                if (data.status == 0) {
                    $.each(data.error, function (prefix, val) {
                        $('span.' + prefix + '_error').text(val[0]);
                    });
                } else {
                    $('#get_in_touch')[0].reset();
                    alert(data.msg);
                }
            }
        });
    });
});

