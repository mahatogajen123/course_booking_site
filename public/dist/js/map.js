function initMap(){
    const directionsRenderer= new google.maps.directionsRenderer();
    const directionsService= new google.maps.directionsService();
    const map = new google.maps.Map(document.getElementById('map'),{
        zoom:14,
        center:{lat:27.7172, lng:85.3240},
    });

    directionsRenderer.setMap(map);

    calculateAndDisplayRoute(directionsService,directionsRenderer);
    document.getElementById('mode').addEventListener("change",()=>{
        calculateAndDisplayRoute(directionsService,directionsRenderer);
    });
}

function calculateAndDisplayRoute(directionsService, directionsRenderer){
    const selectMode = document.getElementById('mode').value;

    directionsService.route({
        origin:document.getElementById('origin').value,
        destination:document.getElementById('destination').value,

        travelMode: google.map.travelmode[selectMode]

    })
    .then((response)=>{
        directionsRenderer.setDirections(response);
    })
    .catch((e)=>window.alert("direction alert failed due to"));

}