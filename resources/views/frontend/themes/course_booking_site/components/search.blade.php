@extends('frontend.themes.course_booking_site.layouts.front_master')
@section('container')
    <div class="container pt-191">
        <span class=" text-start">
            @if ($message = Session::get('message'))
                <h4 class="alert-error alert-block text-center">
                    <i class="fas fa-exclamation-triangle"></i>
                    <strong>{{ $message }}</strong>
                </h4>
            @endif

        </span>
        <div class="row mb-4">
            @if (!$data->isEmpty())
                @foreach ($data as $item)
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-around ">
                        <a href="{{ route('course.booking.site.individual.course', ['course_slug' => $item->title]) }}"
                            class="decoration-none">
                            <div class="course">
                                <img src="{{ asset('uploads/course/images/' . $item->image) }}" width="100%" height="240"
                                    alt="ui/ux">
                                <h4 class="fs-5 fw-bolder mt-2">{{ $item->title }}</h4>
                                <small>Duration:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</small><strong>{{ $item->duration }}</strong>
                            </div>
                        </a>
                    </div>
                @endforeach
            @else
                @php $message = Session::get('error') @endphp
                <div class="alert alert-danger alert-block text-center">
                    <strong> {{ $message }}</strong>
                </div>
            @endif
        </div>
        <h2 class="heading">NEED HELP FIGURING OUT<br> WHICH CLASS IS RIGHT FOR YOU?</h2>
        <div class="text-center">
            <a href="{{ route('course.booking.site.enquiry') }}">
                <button type="button" class="btn btn-outline-l rounded-pill px-4 fw-bolder mb-4"> Find a class for you
                </button>
            </a>
        </div>
    </div>
@endsection
