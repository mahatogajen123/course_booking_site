@extends('frontend.themes.course_booking_site.layouts.front_master')
@section('container')

    <div class="pt-191">
        <div class="course-head d-flex align-items-center justify-content-center"
            style="background: linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url({{ asset('uploads/course/images/' . $course->image) }});">
            <h1 class="fw-bolder text-white">{{ $course->title }}</h1>
        </div>


        <div class="d-flex justify-content-center mt-n4">
            <div class="course-block row">
                <div class="col-lg-4 col-4 d-flex align-items-center justify-content-md-evenly">
                    <img src="{{ asset('course_booking_site/img/Calender.png') }}" alt="">
                    <div class="me-4">
                        <h6 class="fw-bolder fs-5 mb-0">Start Date</h6>
                        <small>{{ $course->start_date }}</small>
                    </div>
                </div>
                <div class="col-lg-4 col-4 d-flex align-items-center justify-content-md-evenly">
                    <img src="{{ asset('course_booking_site/img/Calender.png') }}" alt="">
                    <div class="me-4">
                        <h6 class="fw-bolder fs-5 mb-0">End Date</h6>
                        <small>{{ $course->end_date }}</small>
                    </div>
                </div>
                <div class="col-lg-4 col-4 d-flex align-items-center justify-content-md-around">
                    <img src="{{ asset('course_booking_site/img/Clock2.png') }}" alt="">
                    <div>
                        <h6 class="fw-bolder fs-5 mb-0">Course Duration</h6>
                        <small>{{ $course->duration }}</small>
                    </div>
                </div>
            </div>

        </div>
        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-7">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home"
                                type="button" role="tab" aria-controls="home"
                                aria-selected="true"><strong>Overview</strong></button>
                        </li>
                      
                    </ul>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                            <p class="mt-5 mb-4 text-justify fs-9"> {!! $course->description !!}</p>

                            {{-- <div class="collapse mt-2 mb-5" id="collapseExample">
                                <p class="text-justify fs-9">This course helps students learn basic principles to create a
                                    professional design for any application. Therefore, UI/UX training not only offers the
                                    essential skills and tips to become a proficient designer but also improves the
                                    competitiveness of any design professional to become globally competent.</p>
                            </div>
                            <div class="text-center pt-4 less">
                                <button class="btn text-uppercase fw-bolder" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"
                                    onclick="show()">
                                    See Less <i class="fas fa-chevron-up ms-2 fs-5"></i>
                                </button>
                            </div>
                            <div class="text-center pt-4 more">
                                <button class="btn text-uppercase fw-bolder" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"
                                    onclick="show()">
                                    See More <i class="fas fa-chevron-down ms-2 fs-5"></i>
                                </button>
                            </div> --}} 
                        </div>

                        {{-- <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <h2>Aile keii xaina paxi keii vay rakham la ni ta</h2>
                        </div> --}}
                    </div>
                    <div class="accordion accordion-flush mt-5" id="accordionFlushExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header mb-4 expandall" id="flush-heading">
                                <button class="accordion-button expandallbutton text-purple collapsed fw-bolder"
                                    type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse"
                                    aria-expanded="false" aria-controls="flush-collapse">
                                    {{ $course->title }}-Syllabus<span class="expand ms-auto">Expand All</span><span
                                        class="closeall ms-auto">Close All</span>
                                </button>
                            </h2>
                            @foreach ($course->courseDetail as $item)
                                <div class="accordion-item border-top-0 js">
                                    <h2 class="accordion-header" id="flush-heading{{ $item->id }}">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#flush-collapse{{ $item->id }}" aria-expanded="false"
                                            aria-controls="flush-collapse{{ $item->id }}" onclick="individualclosed()">
                                            {{ $item->title }}
                                        </button>
                                    </h2>
                                    <div id="flush-collapse{{ $item->id }}" class="accordion-collapse collapse"
                                        aria-labelledby="flush-heading{{ $item->id }}"
                                        data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body">{!! $item->description !!}</div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                <div class="col-lg-5 mt-5 mt-lg-0 d-lg-flex justify-content-lg-end d-block">
                    <div class="div">
                        <div class="course-detail p-5">
                            <div class="d-flex justify-content-between align-items-center">
                                <h5 class="m-0 p-0">Price (NPR)</h5>
                                <span class="text-purple fs-2 fw-bolder">Rs. {{ $course->price }}</span>
                            </div>
                            <div class="text-center mt-3">
                                <button type="button" class="btn btn-outline-l rounded-pill px-6 py-1" id="profile-tab"
                                    data-bs-toggle="modal" data-bs-target="#enquiry" type="button" role="tab"
                                    aria-controls="profile" aria-selected="false">ENQUIRE</button>
                            </div>
                            <div class="row align-items-center mt-5">
                                <div class="col-2">
                                    <img src="{{ asset('course_booking_site/img/Education.png') }}" alt="">
                                </div>
                                <div class="col-10">
                                    <p class="fs-6 m-0 text-center">EEE Innovation Ghar Pvt. Ltd<br>
                                </div>
                            </div>
                            <div class="row align-items-center mt-5">
                                <div class="col-2">
                                    <img src="{{ asset('course_booking_site/img/CLocation.png') }}" alt="">
                                </div>
                                <div class="col-10">
                                    <p class="fs-6 m-0 text-center">EEE Innovation Ghar Pvt. Ltd<br>
                                        Swoyambhu - 15, Kathmandu</p>
                                </div>
                            </div>
                            <div class="row align-items-center mt-5">
                                <div class="col-2">
                                    <img src="{{ asset('course_booking_site/img/Call.png') }}" alt="">
                                </div>
                                <div class="col-10">
                                    <p class="fs-6 m-0 text-center">EEE Innovation Ghar Pvt. Ltd<br>
                                        Swoyambhu - 15, Kathmandu</p>
                                </div>
                            </div>
                        </div>
                        <div class="class-schedule mt-5 text-center pt-2">
                            <h3>Upcoming Class Schedule</h3>
                            <h5 class="mt-3">07 Jan 2022</h5>
                            <h5 class="mt-3">08:30 AM - 10:00 AM</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('frontend.themes.course_booking_site.modal.enquiry')

@endsection
