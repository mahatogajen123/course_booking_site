@extends('frontend.themes.course_booking_site.layouts.front_master')
@section('container')
    <div class="container pt-191">
        <h2 class="heading">Meet our Superheroes Instructors</h2>
        <div class="row mb-4">
            @foreach ($teams as $team)
                <div class="col-lg-3 col-md-6 col-sm-12 col-12 teammembers d-flex d-md-block justify-content-center">
                    <div class="step pt-4 mb-4 mb-md-0" style="background: url()">
                        <h2 class="fs-5 fw-bold">{{ $team->designation }}</h2>
                        <img src="{{ asset('uploads/team/images/' . $team->image) }}" width="150" height="150" alt=""
                            class="rounded-circle">
                        <h2 class="fs-5 fw-bolder mt-4">{{ $team->name }}</h2>
                    </div>
                </div>
            @endforeach
        </div>
        {{-- <div class="row">
            @foreach ($teams as $team)
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="step" style="background: url()">
                        <h2 class="fs-5 fw-bold">{{ $team->designation }}</h2>
                        <img src="{{ asset('uploads/team/images/' . $team->image) }}" width="150" height="150" alt=""
                            class="rounded-circle">
                        <h2 class="fs-5 fw-bolder mt-4">{{ $team->name }}</h2>
                    </div>
                </div>
            @endforeach
        </div> --}}
    </div>
@endsection
