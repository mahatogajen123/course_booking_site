@extends('frontend.themes.course_booking_site.layouts.front_master')
@section('container')
    <div class="container px-0 pt-191">
        <div class="row g-0">
            <div class="col-lg-8 iblog pe-lg-5">
                <h2 class="heading mb-5 text-start">{{ $blog->title }}</h2>
                <img src="{{ asset('uploads/blog/images/' . $blog->image) }}" width="100%" height="300" alt="...">
                <p class="mt-5 fs-9 text-justify">
                    {{ $blog->short_detail }}<br><br>{{ $blog->detail }}

                </p>
            </div>
            <div class="col-lg-4">
                <h2 class="heading mb-3 fs-5 text-start">About Author</h2>
                <div class="row g-0 recent author mb-4">
                    <div class="col-lg-2 mb-4">
                        <img src="{{ asset('course_booking_site/img/alex.png') }}" class="rounded-circle" height="60"
                            width="60" alt="...">
                    </div>
                    <div class="col-lg-10 mb-4 ps-3">
                        <p class="fw-bold mb-2">Jay Kr. Singh</p>
                        <small class="text-secondary">Published: 22 Aug 2021</small>
                        <ul class="m-0 mt-3">
                            <li class="me-4"><img src="{{ asset('course_booking_site/img/Facebook.png') }}"
                                    width="23" alt=""></li>
                            <li class="me-4"><img src="{{ asset('course_booking_site/img/instagram.png') }}"
                                    width="23" alt=""></li>
                            <li class="me-4"><img src="{{ asset('course_booking_site/img/Twitter.png') }}"
                                    width="23" alt=""></li>
                            <li class="me-4"><img src="{{ asset('course_booking_site/img/linkedin.png') }}"
                                    width="23" alt=""></li>
                        </ul>
                    </div>
                </div>
                <h2 class="heading mb-4 fs-3 text-start">Recent Posts</h2>
                @foreach ($latest_blogs as $latest_blog)
                    <a href="{{ route('course.booking.site.individual.blog', ['slug' => $latest_blog->title]) }}"
                        class="decoration-none text-dark">
                        <div class="row g-0 recent mb-3">
                            <div class="col-lg-4 mb-3">
                                <img src="{{ asset('uploads/blog/images/' . $latest_blog->image) }}"
                                    class="rounded-3" height="100" width="100" alt="...">
                            </div>
                            <div class="col-lg-8 mb-3">
                                <h5 class="fw-bold mb-3">{{ $latest_blog->title }}</h5>
                                <small class="text-secondary">{{ $latest_blog->created_at }}</small>
                            </div>
                        </div>
                    </a>
                @endforeach

                <h2 class="fs-4 fw-bolder mt-5 mb-4 text-start">Search Blogs</h2>
                <form action="">
                    <div class="input-group mb-3 sborder rounded-pill p-0 me-5">
                        <div class="input-group-prepend border-0 p-0">
                            <button id="button-addon4" type="button" class="btn btn-link text-secondary py-0 mt-1"><i
                                    class="fa fa-search mt-2"></i></button>
                        </div>
                        <input type="search" placeholder="Search" aria-describedby="button-addon4"
                            class="py-2 text-center form-control bg-none border-0" name="search">
                    </div>
                </form>
                <h2 class="fs-4 fw-bolder mt-5 mb-4 text-start">Tags</h2>
                <span class="badge rounded-pill bg-lightgrey fw-normal px-5 fs-8 py-2 me-2 mb-3">IT</span>
                <span class="badge rounded-pill bg-lightgrey fw-normal px-5 fs-8 py-2 me-2 mb-3">Technology</span>
                <span class="badge rounded-pill bg-lightgrey fw-normal px-5 fs-8 py-2 me-2 mb-3">Dev</span>

            </div>
        </div>
    </div>
@endsection
