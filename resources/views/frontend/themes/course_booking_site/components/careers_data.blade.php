@foreach ($careers as $career)
    <div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-around">
        <a href="{{ route('course.booking.site.individual.career', ['career' => $career->slug]) }}"
            class="course decoration-none text-dark  mx-5 mx-sm-0">
            <div class="text-center">
                <img src="{{ asset('uploads/career/images/' . $career->image) }}" width="150" height="150"
                    class="rounded-circle" alt="ui/ux">
            </div>
            <div class="d-flex justify-content-between mt-4">
                <small>{{ $career->job_type }}</small>
                <strong>{{ $career->job_create_date }}</strong>
            </div>
            <h4 class="fs-4 fw-bolder mt-2">{{ $career->title }}</h4>
            <h4 class="fs-6 fw-bolder mt-2">{{ $career->company_name }}</h4>
            <small>{{ $career->location }}</small>
        </a>
    </div>
@endforeach
