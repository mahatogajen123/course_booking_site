@extends('frontend.themes.course_booking_site.layouts.front_master')
@section('container')
    <div class="container px-0 pt-191">
        <div class="row blog g-0 mx-sm-0 mx-4">
            <div class="col-lg-8 pe-lg-5">
                <h2 class="heading text-start mb-4">Blog</h2>
                @foreach ($blog as $item)
                    <div class="card mb-5">
                        <a href="{{ route('course.booking.site.individual.blog', ['slug' => $item->title]) }}"
                            class="decoration-none text-dark">
                            <img src="{{ asset('uploads/blog/images/' . $item->image) }}" class="card-img-top"
                                alt="{{ asset('course_booking_site/img/banner.png') }}" height="300" width="400">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <small>10 Min Read</small>
                                    <strong>{{ $item->created_at }}</strong>
                                </div>
                                <h5 class="card-title fw-bolder mt-3 mb-4 fs-4">{{ $item->title }}</h5>
                                <p class="card-text text-justify fs-8 fw-bold mb-3">{{ $item->detail }}</p>
                                <div class="text-end">
                                    <a href="{{ route('course.booking.site.individual.blog', ['slug' => $item->title]) }}"
                                        class="decoration-none text-dark fw-bold fs-6">Read More<i
                                            class="fas fa-arrow-right text-purple ms-3 fs-4"></i></a>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="col-lg-4 col-12">
                <h2 class="heading fs-3 mb-4 text-start">Recent Posts</h2>
                @foreach ($latest_blog as $blog)
                    <a href="{{ route('course.booking.site.individual.blog', ['slug' => $item->title]) }}"
                        class="decoration-none text-dark">
                        <div class="row g-0 recent mb-3">
                            <div class="col-lg-4 col-4 mb-3">
                                <img src="{{ asset('uploads/blog/images/' . $blog->image) }}" class="rounded-3"
                                    height="100" width="100" alt="...">
                            </div>
                            <div class="col-lg-8 col-8 mb-3">
                                <h5 class="fw-bold mb-3">{{ $blog->title }}</h5>
                                <small class="text-secondary">{{ $blog->created_at }}</small>
                            </div>
                        </div>
                    </a>
                @endforeach
                <h2 class="fs-4 fw-bolder mt-5 mb-4 text-start">Search Blogs</h2>
                <form action="">
                    <div class="input-group mb-3 sborder rounded-pill p-0 me-5">
                        <div class="input-group-prepend border-0 p-0">
                            <button id="button-addon4" type="button" class="btn btn-link text-secondary py-0 mt-1"><i
                                    class="fa fa-search mt-2"></i></button>
                        </div>
                        <input type="search" placeholder="Search" aria-describedby="button-addon4"
                            class="py-2 text-center form-control bg-none border-0">
                    </div>
                </form>
                <h2 class="fs-4 fw-bolder mt-5 mb-4 text-start">Tags</h2>
                <span class="badge rounded-pill bg-lightgrey fw-normal px-5 fs-8 py-2 me-2 mb-3">IT</span>
                <span class="badge rounded-pill bg-lightgrey fw-normal px-5 fs-8 py-2 me-2 mb-3">Technology</span>
                <span class="badge rounded-pill bg-lightgrey fw-normal px-5 fs-8 py-2 me-2 mb-3">Dev</span>

            </div>
        </div>
    </div>
@endsection
