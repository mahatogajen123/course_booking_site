@extends('frontend.themes.course_booking_site.layouts.front_master')
@section('container')
    <?php $total = $courses->total(); ?>
    <?php $pagesize = $courses->count(); ?>
    <div class="container pt-191">
        <h2 class="heading text-start">Browse All Courses</h2>
        <div class="row mb-4">
            <div class="row" id="post-data">
                @include('frontend.themes.course_booking_site.components.course_data')
            </div>
            @if ($pagesize >= 4)
                <div class="text-center more">
                    <button type="button" class="btn btn-outline-l load-more rounded-pill px-6 pe-4 fw-bolder my-4"
                        data-bs-toggle="collapse" data-bs-target="#collapseExample" aria-expanded="false"
                        aria-controls="collapseExample" id="load_more">
                        <div class="spinner-border text-white ajax-load text-center mx-3" style="display:none" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div> Load
                        More<i class="fas fa-arrow-down text-end ps-4 text-blue"></i>
                    </button>
                </div>
            @endif
        </div>
        <h2 class="heading">NEED HELP FIGURING OUT<br> WHICH CLASS IS RIGHT FOR YOU?</h2>
        <div class="text-center">
            <a href="{{ route('course.booking.site.enquiry') }}">
                <button type="button" class="btn btn-outline-l rounded-pill px-4 fw-bolder mb-4"> Find a class for you
                </button>
            </a>
        </div>
    </div>

    <script type="text/javascript">
        var page = 1;
        var total = "<?php echo $total; ?>";
        var pagesize = "<?php echo $pagesize; ?>";
        if (page >= Math.ceil(total / pagesize)) {
            $('.load-more').hide();
        }
        $('#load_more').on('click', function() {
            page++;
            loadMoreData(page);
            if (page >= Math.ceil(total / pagesize)) {
                $('.load-more').hide();
            }
        });

        function loadMoreData(page) {
            $.ajax({
                    url: '?page=' + page,
                    type: "get",
                    beforeSend: function() {
                        $('.ajax-load').show();
                        $('.load-more').addClass("px-4");
                    }

                })
                .done(function(data) {

                    if (data.html == "") {
                        // $('.load-more').hide();
                        return;
                    }
                    $('.ajax-load').hide();
                    $('.load-more').removeClass("px-4");
                    $("#post-data").append(data.html);
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    window.alert('Something went wrong...');
                });
        }
    </script>
@endsection
