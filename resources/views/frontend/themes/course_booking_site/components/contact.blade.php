@extends('frontend.themes.course_booking_site.layouts.front_master')
@section('container')
    <div class="pt-191">
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1248.7671955330372!2d85.28319676173679!3d27.715834832975183!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb189003310b6f%3A0x7b5c1e7bb0027174!2sMachhapuchchhre%20Bank!5e0!3m2!1sen!2snp!4v1640841869863!5m2!1sen!2snp"
            width="100%" height="440" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
    <div class="container">
        <h1 class="interested mt-4">Interested?</h1>
        <h2 class="heading text-start">Get In Touch With Us</h2>
        <div class="row mb-4 g-0  d-flex justify-content-between">
            <div class="col-lg-5 col-md-6 col-sm-6 col-12 pe-lg-5">
                <p class="text-justify fs-8">
                    We read each email and reply within 2 business days.<br>
                    Please enter your correct email address, so that we can get back to you.
                </p>
                <form class="mt-5 get_in_touch" id="get_in_touch" action="{{ route('course.booking.site.getintouch') }}">
                    @csrf
                    <div class="mb-3 form-floating">
                        <input type="text" class="form-control pb-1" id="name" placeholder="Full Name" name="name">
                        <label for="name">Full Name <strong class="text-danger">*</strong></label>
                        <span class="text-danger error-text name_error"></span>
                    </div>
                    <div class="mb-3 form-floating">
                        <input type="email" class="form-control" id="email" placeholder="Email Address" name="email">
                        <label for="email">Email <strong class="text-danger">*</strong></label>
                        <span class="text-danger error-text email_error"></span>
                    </div>
                    <div class="mb-3 form-floating">
                        <input type="tel" class="form-control" id="contact" placeholder="Conatct Number" name="contact">
                        <label for="contact">Contact<strong class="text-danger">*</strong></label>
                        <span class="text-danger error-text contact_error"></span>
                    </div>
                    <div class="mb-5 form-floating">
                        <textarea class="form-control" id="message" rows="6" placeholder="Enter Your Message here."
                            name="message"></textarea>
                        <label for="message">Message <strong class="text-danger">*</strong></label>

                        <span class="text-danger error-text message_error"></span>
                    </div>
                    <div class="text-center">
                        <button type="submit"
                            class="btn btn-outline-l fw-bold fs-5 rounded-pill px-6 py-1 mb-5">SUBMIT</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="row g-0 ps-lg-0 ps-3">
                    <div class="col-6 pt-lg-5">
                        <img src="{{ asset('course_booking_site/img/about1.png') }}" width="87%" alt="">
                    </div>
                    <div class="col-6 mt-n2">
                        <img src="{{ asset('course_booking_site/img/about1.png') }}" width="87%" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
