@extends('frontend.themes.course_booking_site.layouts.front_master')
{{-- @dd($courses) --}}
@section('container')

    <div class="container pt-191">
        <h2 class="heading mb-4">Interested to learn with us ? Send an Enquiry</h2>
        <div class="d-flex justify-content-center">
            <form action="{{ route('course.booking.site.enquiry.store') }}" method="POST">
                @csrf
                <span id="flash-message"> @include('flash-message')</span>
                <div class="mb-3">
                    <label for="name" class="form-label">Full Name <strong class="text-danger">*</strong></label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                </div>
                @if ($errors->has('name'))
                    <p style="color: red">
                        {{ $errors->first('name') }}
                    </p>
                @endif

                <div class="mb-3">
                    <label for="email" class="form-label">Email Address <strong class="text-danger">*</strong></label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                </div>
                @if ($errors->has('email'))
                    <p style="color: red">
                        {{ $errors->first('email') }}
                    </p>
                @endif
                <div class="mb-3">
                    <label class="form-label" for="inlineFormInputGroupUsername">Mobile Number <strong
                            class="text-danger">*</strong></label>
                    <div class="input-group">
                        <div class="input-group-text num">+977<i class="fas fa-sort-down ms-2 fs-3 mt-n1"></i></div>
                        <input type="tel" class="form-control rounded-start" id="inlineFormInputGroupUsername"
                            name="contact" value="{{ old('contact') }}">
                    </div>
                    @if ($errors->has('contact'))
                        <p style="color: red">
                            {{ $errors->first('contact') }}
                        </p>
                    @endif
                </div>
                <div class="mb-3">
                    <label class="form-label" for="inlineFormInputGroupUsername">Interested Course <strong
                            class="text-danger">*</strong></label>
                    <select class="form-select form-control" aria-label="Default select example" name="course">
                        <option selected disabled>Please Select Any Course</option>
                        @foreach ($courses as $course)
                            <option value="{{ $course->title }}">{{ $course->title }}</option>

                        @endforeach
                        {{-- <option selected>Select Course</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option> --}}
                    </select>
                </div>
                @if ($errors->has('course'))
                    <p style="color: red">
                        {{ $errors->first('course') }}
                    </p>
                @endif
                <div class="mb-3">
                    <label for="optional_course" class="form-label">If Any Other Course (Optional)</label>
                    <input type="text" class="form-control" id="optional_course">
                </div>
                @if ($errors->has('optional_course'))
                    <p style="color: red">
                        {{ $errors->first('optional_course') }}
                    </p>
                @endif
                <div class="mb-5">
                    <label for="exampleFormControlTextarea1" class="form-label">Any Specific Message <strong
                            class="text-danger">*</strong></label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" cols="50" name="message"
                        value="{{ old('message') }}"></textarea>
                    @if ($errors->has('message'))
                        <p style="color: red">
                            {{ $errors->first('message') }}
                        </p>
                    @endif
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-outline-l fw-bold fs-5 rounded-pill px-6 py-1 mb-2">SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
@endsection
