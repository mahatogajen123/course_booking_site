@foreach ($courses as $course)
    <div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-around ">
        <a href="{{ route('course.booking.site.individual.course', ['course_slug' => $course->title]) }}"
            class="decoration-none">
            <div class="course">
                <img src="{{ asset('uploads/course/images/' . $course->image) }}" width="100%" height="240"
                    alt="ui/ux">
                <h4 class="fs-5 fw-bolder mt-2">{{ $course->title }}</h4>
                <small>Duration:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</small><strong>{{ $course->duration }}</strong>
            </div>
        </a>
    </div>
@endforeach
