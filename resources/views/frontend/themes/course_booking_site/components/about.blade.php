@extends('frontend.themes.course_booking_site.layouts.front_master')
@section('container')
    @if ($company_details)
        <div class="container px-0 pt-191">
            <div class="row mb-4 g-0 mx-sm-0 mx-4 d-flex justify-content-between">
                <div class="col-lg-5 col-md-6 col-sm-12 col-12">
                    <h2 class="heading text-start">About Us</h2>
                    <p class="text-justify lh-1" style="font-size: 1.2rem;">
                        {!! $company_details->about !!}
                    </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 mt-4">
                    <div class="row g-0 image">
                        @foreach ($company_details->images as $image)
                            <div class="col-4">
                                <img src="{{ $image->image }}" alt="" width="91%" height="300">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-between mt-4 pt-3">
                <div class="circle1"></div>
                <h2 class="heading mt-4 mb-sm-0 mb-4">Our Motto, Vision & Mission</h2>
                <div class="circle2"></div>
            </div>
            <div class="row mb-4 g-0 d-flex justify-content-between mt-n2 mx-sm-0 mx-4">
                <div class="col-lg-5 col-md-6 col-sm-12 col-12 text-center position-relative">
                    <img src="{{ asset('course_booking_site/img/girl.png') }}" width="82%" alt="">
                    <img src="{{ asset('course_booking_site/img/Other 01.png') }}" alt="" class="mail1"
                        width="18.2%">
                    <img src="{{ asset('course_booking_site/img/Other 15.png') }}" alt="" class="mail2"
                        width="18.2%">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 mt-5">
                    <p class="text-justify lh-1 my-5" style="font-size: 1.2rem;">
                        {!! $company_details->moto !!}
                    </p>
                </div>
            </div>
        </div>
    @else
        <div class="container px-0 pt-191">
            <div class="row mb-4 g-0 mx-sm-0 mx-4 d-flex justify-content-between">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <h3>Sorry we don't have any company detail to share right now <br>
                        Stay connected to know about us </h3>
                </div>
            </div>
        </div>
    @endif
@endsection
