@extends('frontend.themes.course_booking_site.layouts.front_master')
@section('container')
    <div class="container pt-191">
        <h2 class="heading text-start mb-4">{{ $career->title }}</h2>
        <div class="row pt-1">
            <div class="col-lg-9 col-sm-12 col-12">
                <div class="row g-0">
                    <div class="col-lg-3 col-5">
                        <ul class="p-0 ps-1">
                            <li class="d-flex justify-content-between">
                                <h5>Sub Title</h5>
                                <h5 class="me-5">:</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5>Job Type</h5>
                                <h5 class="me-5">:</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5>Job Level</h5>
                                <h5 class="me-5">:</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5>Company</h5>
                                <h5 class="me-5">:</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5>Location</h5>
                                <h5 class="me-5">:</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5>Posted</h5>
                                <h5 class="me-5">:</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5>Offered Salary</h5>
                                <h5 class="me-5">:</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5>Deadline to Apply</h5>
                                <h5 class="me-5">:</h5>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-9 col-7">
                        <ul class="p-0">
                            <li class="d-flex justify-content-between">
                                <h5 class="fw-bolder">{{ $career->sub_title ?? 'Null ' }}</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5 class="fw-bolder">{{ $career->job_type ?? 'Null' }}</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5 class="fw-bolder">{{ $career->job_level ?? 'Null' }}</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5 class="fw-bolder">{{ $career->company_name ?? 'Null' }}</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5 class="fw-bolder">{{ $career->location ?? 'Null' }}</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5 class="fw-bolder">{{ $career->job_create_date ?? 'Null' }}</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5 class="fw-bolder">{{ $career->offer_salary ?? 'Null' }}</h5>
                            </li>
                            <li class="d-flex justify-content-between">
                                <h5 class="fw-bolder">{{ $career->deadline ?? 'Null' }}</h5>
                            </li>
                        </ul>
                    </div>
                </div>
                <h2 class="fs-3 fw-bolder mb-4 mt-5">Job Description</h2>
                <p class="fs-8 me-5 text-justify">
                    {!! $career->detail !!}
                </p>
                <h2 class="fs-3 fw-bolder my-3">Responsibilities</h2>
                <p class="fs-8 me-5 text-justify">
                <ul class="responsibilities ps-4">
                    {!! $career->responsibility !!}
                </ul>
                </p>
                <h2 class="fs-3 fw-bolder my-3">Requirements</h2>
                <p class="fs-8 me-5 text-justify">
                <ul class="responsibilities ps-4">
                    {!! $career->requirement !!}
                </ul>
                </p>
                <a href="{{ route('course.booking.site.career.apply.form', ['careerform_slug' => $career->slug]) }}">
                    <button type="button" class="btn btn-outline-l rounded-pill px-6 fw-bolder mt-5 mb-3">APPLY</button>
                </a>
            </div>
            <div class="col-lg-3 col-sm-12 col-12">
                <div class="careerimg">
                    <img src="{{ asset('uploads/career/images/' . $career->image) }}" width="100%" height="100%" alt="">
                </div>
                <a href="{{ route('course.booking.site.career.apply.form', ['careerform_slug' => $career->slug]) }}">
                    <div class=" text-center">
                        <button type="button" class="btn btn-outline-l rounded-pill px-6 fw-bolder my-5">APPLY</button>
                    </div>
                </a>
                <div class="share text-center">
                    <h5 class="fw-bolder mt-3">Share This Job</h5>
                    <ul class="m-0 justify-content-around">
                        <a href=""></a>
                        <li><img src="{{ asset('course_booking_site/img/Facebook.png') }}" alt=""></li>
                        <li><img src="{{ asset('course_booking_site/img/Facebook.png') }}" alt=""></li>
                        <li><img src="{{ asset('course_booking_site/img/Facebook.png') }}" alt=""></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
@endsection
