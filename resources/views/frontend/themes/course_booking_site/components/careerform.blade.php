@extends('frontend.themes.course_booking_site.layouts.front_master')
@section('container')
    <div class="container pt-191">
        <h2 class="heading mb-4">Laravel Developer</h2>
        <div class="d-flex justify-content-center">
            <form action="{{ route('course.booking.site.career.store', $career->id) }}" enctype="multipart/form-data"
                method="POST">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Full Name <strong class="text-danger">*</strong></label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                    @if ($errors->has('name'))
                        <p style="color: red">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email Address <strong class="text-danger">*</strong></label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <p style="color: red">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                </div>
                <div class="mb-3">
                    <label class="form-label" for="inlineFormInputGroupUsername">Mobile Number <strong
                            class="text-danger">*</strong></label>
                    <div class="input-group">
                        <div class="input-group-text num">+977<i class="fas fa-sort-down ms-2 fs-3 mt-n1"></i></div>
                        <input type="tel" class="form-control rounded-start" id="inlineFormInputGroupUsername"
                            name="contact" value="{{ old('contact') }}">

                    </div>
                    @if ($errors->has('contact'))
                        <p style="color: red">
                            {{ $errors->first('contact') }}
                        </p>
                    @endif
                </div>
                <div class="mb-3">
                    <label for="url" class="form-label">Portfolio URL <strong class="text-danger">*</strong></label>
                    <input type="url" class="form-control" id="url" name="portfolio_url"
                        value="{{ old('portfolio_url') }}">
                    @if ($errors->has('url'))
                        <p style="color: red">
                            {{ $errors->first('url') }}
                        </p>
                    @endif
                </div>
                <div class="mb-2">
                    <div class="row g-0">
                        <label for="file" class="form-label fileupload">Upload Resume <strong
                                class="text-danger">*</strong></label>
                        <div class="col-4">
                            <input type="file" id="file" onchange="fileupload()" name="resume" /><label
                                for="file">Upload</label>

                        </div>
                        <div class="col-8">
                            <input class="form-control file-input" aria-label="file" id="file-input">
                        </div>
                        @if ($errors->has('resume'))
                            <p style="color: red">
                                {{ $errors->first('resume') }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="text-center mb-3">
                    <small>We accept PDF, DOC, DOCX, JPG & PNG Files.</small>
                </div>
                <div class="mb-5">
                    <label for="exampleFormControlTextarea1" class="form-label">Tell Us About Yourself <strong
                            class="text-danger">*</strong></label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" cols="50" name="about"
                        value="{{ old('about') }}"></textarea>
                    @if ($errors->has('about'))
                        <p style="color: red">
                            {{ $errors->first('about') }}
                        </p>
                    @endif
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-outline-l fw-bold fs-5 rounded-pill px-6 py-1 mb-5">SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
@endsection
<script>
    window.onload = function() {
        document.getElementById("file-input").value = "";
    }

    function fileupload() {
        var n = document.getElementById("file").value;
        var filename = n.replace(/^.*\\/, "");
        console.log(filename);
        document.getElementById('file-input').value = filename;
    }
</script>
