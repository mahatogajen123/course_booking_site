@extends('frontend.themes.course_booking_site.layouts.front_master')
@section('container')
    <section id="head" class="mb-5 pt-191">
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            @if (!$sliders->isEmpty())
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0"
                        class="active" aria-current="true" aria-label="Slide 1">
                    </button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                        aria-label="Slide 2">
                    </button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                        aria-label="Slide 3">
                    </button>
                </div>
            @endif

            <div class="carousel-inner">
                @php
                    $first_slider = \app\Models\Slider::select('id')->first();
                @endphp

                @foreach ($sliders as $slider)
                    <div class="carousel-item {{ $slider->id == $first_slider->id ? 'active' : ' ' }}">
                        <div class="slider"
                            style="background: url({{ asset('uploads/slider/images/' . $slider->image) }})"></div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section id="course" class="pt-5">
        <h2 class="heading">COURSES WE OFFER</h2>
        <div class="container">
            <div class="row">
                @foreach ($courses as $course)
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-around ">
                        <a href="{{ route('course.booking.site.individual.course', ['course_slug' => $course->title]) }}"
                            class="decoration-none">
                            <div class="course">
                                <img src="{{ asset('uploads/course/images/' . $course->image) }}" width="100%"
                                    height="240" alt="ui/ux">
                                <h4 class="fs-5 fw-bolder mt-2">{{ $course->title }}</h4>
                                <small>Duration:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</small><strong>{{ $course->duration }}</strong>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="text-center">
                <a href="{{ route('course.booking.site.courses') }}">
                    <button type="button" class="btn btn-outline-l fw-bold fs-5 rounded-pill px-4 py-1 mt-3">Explore All
                        Courses</button></a>
            </div>
        </div>
    </section>
    <section id="step" class="pt-5">
        <h2 class="heading">WHY CHOOSE US ?</h2>
        <div class="container">
            <div class="row mb-4">
                <div class="col-lg-3 col-md-6 d-flex d-md-block justify-content-center col-sm-12 col-12">
                    <div class="step mb-4 mb-md-0">
                        <img src="{{ asset('course_booking_site/img/indepth.png') }}" alt="">
                        <h2 class="fs-4 fw-bolder mt-5">Learn In-depth</h2>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-12 d-flex d-md-block justify-content-center">
                    <img src="{{ asset('course_booking_site/img/arrowdown.png') }}" class="arrow d-none d-md-block"
                        alt="">
                    <div class="step mb-4 mb-md-0">
                        <img src="{{ asset('course_booking_site/img/step.png') }}" alt="">
                        <h2 class="fs-4 fw-bolder mt-5 px-5">Step by Step
                            Guidance</h2>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-flex d-md-block justify-content-center col-sm-12 col-12">
                    <div class="step mb-4 mb-md-0">
                        <img src="{{ asset('course_booking_site/img/certified.png') }}" alt="">
                        <h2 class="fs-4 fw-bolder mt-5">Get Certified</h2>
                    </div>
                    <img src="{{ asset('course_booking_site/img/arrowup.png') }}" class="arrow d-none d-lg-block" alt="">
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-12 d-flex d-md-block justify-content-center">
                    <img src="{{ asset('course_booking_site/img/arrowdown.png') }}" class="arrow d-none d-md-block"
                        alt="">
                    <div class="step mb-4 mb-md-0">
                        <img src="{{ asset('course_booking_site/img/portfolio.png') }}" alt="">
                        <h2 class="fs-4 fw-bolder px-5 mt-5">Build Strong
                            Portfolio</h2>
                    </div>
                </div>
            </div>
            <p class="pt-4">We are one of the leading educational platform for the young generation to shape
                their career and secure their future. As the demands in IT sector is growing day to day, we help our
                students on focusing in their desired field of interest and guide them achieve their career path. </p>
            <div class="d-flex justify-content-around">
                <h1 id="grad1" class="mt-5">WE WILL HELP YOU ACHIEVE YOUR GOAL</h1>
            </div>
            <p class="pt-4">Once you get in touch with us, it’s our core responsibility to guide you in each
                and every step you take. Whenever someone comes and get affiliated with us, we help them realize their
                career of choice, it’s importance to them and the IT world, make them stronger and capable to crack
                tough interviews and get hired in the real-time jobs. </p>
            <h2 class="heading mt-5">MEET THE TEAM</h2>
            <div class="row mb-4">
                @foreach ($teams as $team)
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12 teammembers d-flex d-md-block justify-content-center">
                        <div class="step pt-4 mb-4 mb-md-0" style="background: url()">
                            <h2 class="fs-5 fw-bold">{{ $team->designation }}</h2>
                            <img src="{{ asset('uploads/team/images/' . $team->image) }}" width="150" height="150" alt=""
                                class="rounded-circle">
                            <h2 class="fs-5 fw-bolder mt-4">{{ $team->name }}</h2>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="text-center">
                <a href="{{ route('course.booking.site.team') }}">
                    <button type="button" class="btn btn-outline-l fw-bold fs-5 rounded-pill px-5 py-1 mt-3">Meet The
                        Team</button>
                </a>
            </div>
            <h2 class="heading mt-5">LOCATE US</h2>
        </div>
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1248.7671955330372!2d85.28319676173679!3d27.715834832975183!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb189003310b6f%3A0x7b5c1e7bb0027174!2sMachhapuchchhre%20Bank!5e0!3m2!1sen!2snp!4v1640841869863!5m2!1sen!2snp"
            width="100%" height="440" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        <div class="container mt-4">
            <div class="row mx-sm-0 mx-3">
                <div
                    class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-lg-around mb-2 justify-content-between align-items-center">
                    <div class="me-3 me-lg-0">
                        <img src="{{ asset('course_booking_site/img/location.png') }}" alt="">
                    </div>
                    <div>
                        <p class="fs-7 fw-bold m-0 text-center me-5">Swoyambhu - 15,<br>
                            Opp Buddha Park,<br>
                            Kathmandu, P3, Nepal</p>
                    </div>
                </div>
                <div
                    class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-lg-around mb-2 justify-content-between align-items-center">
                    <div class="me-3">
                        <img src="{{ asset('course_booking_site/img/mail.png') }}" alt="">
                    </div>
                    <div class="ms-sm-0 ms-5">
                        <p class="fs-7 fw-bold m-0">eeeinnovation369@gmail.com
                            info@eeeinnovation.com</p>
                    </div>
                </div>
                <div
                    class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-lg-around mb-2 justify-content-between align-items-center">
                    <div class="me-3">
                        <img src="{{ asset('course_booking_site/img/phone.png') }}" alt="">
                    </div>
                    <div>
                        <p class="fs-7 fw-bold m-0 text-center">Don’t hesitate to call <br>
                            +977-9863033916, +977-1-5247384
                    </div>
                </div>
                <div
                    class="col-lg-3 col-md-6 col-sm-6 col-12 d-flex justify-content-lg-around mb-2 justify-content-between align-items-center">
                    <div class="ms-lg-4 ms-0 me-3 me-lg-0">
                        <img src="{{ asset('course_booking_site/img/clock.png') }}" alt="">
                    </div>
                    <div>
                        <p class="fs-7 fw-bold m-0 text-center">Mon- Fri: 08:30 - 06:00
                    </div>
                </div>
            </div>
            <h2 class="heading mt-5">NEED HELP FIGURING OUT <br> WHICH CLASS IS RIGHT FOR YOU?</h2>
            <div class="text-center">
                <a href="{{ route('course.booking.site.enquiry') }}">
                    <button type="button" class="btn btn-outline-l fw-bold fs-5 rounded-pill px-5 py-1 mt-1">Find a class
                        for you</button></a>
            </div>
        </div>
    </section>
@endsection
