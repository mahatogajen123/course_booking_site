<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="enquiry" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Make your Enquiry</h5>

                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <small class="text-center"> (field with star are manditory)</small>

                <div class="d-flex justify-content-center">
                    <form class="enquiry-form" action="{{ route('course.booking.site.enquiry.store') }}">
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Full Name <strong
                                    class="text-danger">*</strong></label>
                            <input type="text" class="form-control" id="name" name="name">
                            <span class="text-danger error-text name_error"></span>

                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email Address <strong
                                    class="text-danger">*</strong></label>
                            <input type="email" class="form-control" id="email" name="email">
                            <span class="text-danger error-text email_error"></span>

                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="inlineFormInputGroupUsername">Mobile Number <strong
                                    class="text-danger">*</strong></label>
                            <div class="input-group">
                                <div class="input-group-text num">+977<i class="fas fa-sort-down ms-2 fs-3 mt-n1"></i>
                                </div>
                                <input type="tel" class="form-control rounded-start" id="contact" name="contact">
                            </div>
                            <span class="text-danger error-text contact_error"></span>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="inlineFormInputGroupUsername">Interested Course <strong
                                    class="text-danger">*</strong></label>
                            <select class="form-select form-control" aria-label="Default select example" id="course"
                                name="course">
                                <option selected disabled>Please Select Any Course</option>
                                @foreach ($courses as $course)
                                    <option value="{{ $course->title }}">{{ $course->title }}</option>
                                @endforeach
                            </select>
                            <span class="text-danger error-text course_error"></span>

                        </div>

                        <div class="mb-3">
                            <label for="optional_course" class="form-label">If Any Other Course (Optional)</label>
                            <input type="text" class="form-control" id="optional_course">
                        </div>

                        <div class="mb-5">
                            <label for="exampleFormControlTextarea1" class="form-label">Any Specific Message <strong
                                    class="text-danger">*</strong></label>
                            <textarea class="form-control" id="message" rows="5" cols="50" name="message"></textarea>
                            <span class="text-danger error-text message_error"></span>

                        </div>
                        <div class="text-center">
                            <button type="submit"
                                class="btn btn-outline-l fw-bold fs-5 rounded-pill px-6 py-1 mb-2">SUBMIT</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
