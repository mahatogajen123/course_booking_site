  <footer class="mt-5 text-white pt-4 align-bottom">
      <div class="container pb-4">
          <div class="row mx-2 mx-md-0">
              <div class="col-lg-3 g-0 mt-1 col-md-6 col-sm-6 col-6">
                  <h2 class="fw-bolder fs-4 mb-3 mb-lg-5">Company</h2>
                  <a href="{{ route('course.booking.site.about') }}" style="text-decoration:none; color:white; ">
                      <h6>About Us</h6>
                  </a>
                  <a href="{{ route('course.booking.site.contact') }}" style="text-decoration:none; color:white; ">
                      <h6>Contact Us</h6>
                  </a>
                  <a href="{{ route('course.booking.site.enquiry') }}" style="text-decoration:none; color:white; ">
                      <h6>Enquiry </h6>
                  </a>
                  <h6>Success Gallery</h6>
                  <h6>Testimonials</h6>
                  <a href="{{ route('course.booking.site.team') }}" style="text-decoration:none; color:white; ">
                      <h6>Our Team</h6>
                  </a>
                  <a href="{{ route('course.booking.site.enquiry') }}" style="text-decoration:none; color:white; ">
                      <h6>Our Partners</h6>
                  </a>
                  <a href="{{ route('course.booking.site.enquiry') }}" style="text-decoration:none; color:white; ">
                      <h6>Our Services</h6>
                  </a>
                  <h6>Privacy Policy</h6>
                  <h6>Terms & Conditions</h6>
              </div>
              <div class="col-lg-3 col-md-6 mt-1 g-0 col-sm-6 col-6">
                  <h2 class="fw-bolder fs-4  mb-3 mb-lg-5">Related Links</h2>
                  <h6>Become An Instructor</h6>
                  <h6>Verify Certificate</h6>
                  <h6>Become Placement Partner</h6>
                  <h6>Events</h6>
                  <h6>Notice </h6>
                  <h6>Help & FAQ</h6>
              </div>
              <div class="col-lg-3 col-md-6 mt-1 g-0 col-sm-6 col-6">
                  <h2 class="fw-bolder fs-4 mb-3 mt-4 mb-lg-5 mt-lg-0">latest from Blogs</h2>
                  <h6>Beginning as a UI/UX ... </h6>
                  <h6>How not to make errors... </h6>
                  <h6>Make an impactful prese...</h6>
                  <h6 class="mb-5">How to earn as freelance...</h6>
                  <div class="pt-lg-5">
                      <h2 class="fw-bolder ms-n4 fs-4 my-4 accept">We Accept</h2>
                      <ul class="pay">
                          <li><img src="{{ asset('course_booking_site/img/Esewa.png') }}" alt=""></li>
                          <li><img src="{{ asset('course_booking_site/img/khalti.png') }}" alt=""></li>
                          <li><img src="{{ asset('course_booking_site/img/imepay.png') }}" alt=""></li>
                      </ul>
                  </div>
              </div>
              <div class="col-lg-3 col-md-6 mt-1 g-0 col-sm-6 col-6">
                  <h2 class="fw-bolder fs-4 mb-3 mt-4 mb-lg-5 mt-lg-0">Sign up to our newsletter</h2>
                  <form action="" class="f mb-5">
                      <div class="input-group fmail my-4 me-5">
                          <input type="search" placeholder="Enter Your Email Address" aria-describedby="button-addon4"
                              class="text-center bg-dark form-control border-0">
                          <div class="input-group-prepend border-0">
                              <button id="button-addon4" type="button" class="btn btn-link text-info"><i
                                      class="fa fa-search"></i></button>
                          </div>
                      </div>
                  </form>
                  <h2 class="fw-bolder fs-4 my-4">Follow us on social media</h2>
                  @php
                      $socialmedia = \App\Models\SocialMedia::get();
                  @endphp
                  <ul>
                      @foreach ($socialmedia as $item)
                          <a href="{{ $item->url }}" target="blank">
                              <li><img src="{{ asset('uploads/socialmedia/logo/' . $item->logo) }}" alt="" width="30"
                                      height="30" style="border-radius: 50%; object-fit:cover"></li>
                          </a>
                      @endforeach
                  </ul>
              </div>
          </div>
      </div>
      <div class="footnote text-center">
          <p class="p-2 m-0">Designed & Developed by EEE Innovation Ghar Pvt. Ltd.</p>
      </div>
  </footer>
