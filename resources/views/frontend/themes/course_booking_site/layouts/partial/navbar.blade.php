<nav id="navbar-example" class="navbar navb1 navbar-expand-lg navbar-light bg-white fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#"><img src="{{ asset('course_booking_site/img/EEE.png') }}" alt="logo"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02"
            aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <form action="{{ route('course.search') }}" method="GET">
            <div class="input-group mb-3 sborder rounded-pill p-0 mt-4 me-5">
                <div class="input-group-prepend border-0 p-0">
                    <button id="button-addon4" type="button" class="btn btn-link text-secondary py-0"><i
                            class="fa fa-search"></i></button>
                </div>
                <input type="search" placeholder="What would you like to learn?" aria-describedby="button-addon4"
                    class="py-0 text-center form-control bg-none border-0"  name="search">
            </div>
        </form>

        <div>
            <button type="button" class="btn btn-outline-l rounded-pill me-4 px-5 py-0">Login</button>
            <button type="button" class="btn btn-grad rounded-pill px-5 py-1 me-2">Sign Up</button>
        </div>
    </div>
    <div class="collapse navbar-collapse justify-content-center mt-2" id="navbarTogglerDemo02">
        <ul class="navbar-nav">
            <li class="nav-item ms-lg-5 ms-0">
                <a class="nav-link" href="{{ url('/') }}">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('course.booking.site.courses') }}">Courses</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('course.booking.site.career') }}">Careers</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('course.booking.site.about') }}">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('course.booking.site.contact') }}">Contact</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('course.booking.site.blog') }}">Blog</a>
            </li>
        </ul>
    </div>
</nav>
<nav id="navbar2" class="navbar navb navbar-expand-lg navbar-light shadow bg-white fixed-top">
    <div class="container-fluid row">
        <div class="col-lg-1 col-3">
            <a class="navbar-brand" href="#"><img src="{{ asset('course_booking_site/img/EEE.png') }}"
                    alt="logo"></a>
        </div>
        <button class="navbar-toggler col-2 me-2" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <button class="navbar-toggler col-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo"
            aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-user"></i>
        </button>
        <div class="collapse navbar-collapse col-xl-5 col-lg-5 col-xxl-6 ps-lg-5" id="navbarTogglerDemo02">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('course.booking.site.courses') }}">Courses</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('course.booking.site.career') }}">Careers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('course.booking.site.about') }}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('course.booking.site.contact') }}">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('course.booking.site.blog') }}">Blog</a>
                </li>
            </ul>
        </div>

        <div class="collapse navbar-collapse col-lg-2" id="navbarTogglerDemo">
            <button type="button" class="btn btn-outline-l rounded-pill mx-lg-1 mx-xl-3 px-4 py-0">Login</button>
            <button type="button" class="btn btn-grad rounded-pill px-4 py-1">Sign Up</button>
        </div>
        <div class="col-lg-2 col-sm-4 col-12 d-flex justify-content-center">
            <form action="{{ route('course.search') }}" class="d-sm-block d-none">
                <div class="search-container">
                    <div class="search-icon-btn">
                        <i class="fa fa-search"></i>
                    </div>
                    <div class="search-input">
                        <input type="search" class="search-bar" placeholder="What would you like to learn?" name="search">
                    </div>
                </div>
            </form>
            <form action="" class="d-sm-none d-block search2">
                <div class="input-group mb-3 sborder rounded-pill p-0 mt-4 me-5">
                    <div class="input-group-prepend border-0 p-0">
                        <button id="button-addon4" type="button" class="btn btn-link text-secondary py-0"><i
                                class="fa fa-search"></i></button>
                    </div>
                    <input type="search" placeholder="What would you like to learn?" aria-describedby="button-addon4"
                        class="py-0 text-center form-control bg-none border-0">
                </div>
            </form>
        </div>
    </div>
</nav>
