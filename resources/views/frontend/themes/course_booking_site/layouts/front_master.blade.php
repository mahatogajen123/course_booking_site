<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('course_booking_site/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('course_booking_site/plugins/bootstrap/dist/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('course_booking_site/plugins/fontawesome-free/css/all.min.css') }}" />
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet'>
    {{-- <script src="{{ asset('course_booking_site/plugins/jquery/jquery.min.js') }}"></script> --}}
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    {{-- <script src="{{ asset('course_booking_site/plugins/jquery/jquery.slim.min.js') }}"></script> --}}


</head>

<body>
    @include('frontend.themes.course_booking_site.layouts.partial.navbar')
    <a href="{{ route('course.booking.site.enquiry') }}">
        <button type="button" class="btn btn-grad inquiry rounded-circle">Inquiry</button>
    </a>
    <div class="arrow-button" onclick="topFunction()">
        <button type="button" class="btn btn-outline-l arrow-up"><i class="fas fa-arrow-up"></i></button>
    </div>
    @if (Session::has('success'))
        <div class="alert alert-success fade show" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                <use xlink:href="#check-circle-fill" />
            </svg>
            <strong>{{ Session::get('success') }}!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger fade show" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                <use xlink:href="#check-circle-fill" />
            </svg>
            <strong>{{ Session::get('error') }}!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    {{-- container start here --}}
    @section('container')

    @show
    {{-- @yield('container') --}}
    {{-- container end here --}}

    @include('frontend.themes.course_booking_site.layouts.partial.footer')
    <script src="{{ asset('course_booking_site/plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('course_booking_site/js/script.js') }}"></script>
    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").alert('close');
            }, 2000);

            $('.statusUpdate').on("change", "input:checkbox", function() {
                $('#' + $(this).closest('form').attr('id')).submit();
            });
        });
    </script>

</body>

</html>
