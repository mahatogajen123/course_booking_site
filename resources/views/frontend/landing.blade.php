@extends('frontend.layouts.home')
@section('content')
    <main class="page-wrapper">
        <section class="bg-darker bg-size-cover background py-5 py-lg-9 mb-4 mb-md-5"
            style="padding-bottom: 7rem !important;">
            <div class="row g-0 d-flex justify-content-between">
                <div class="d-flex align-items-lg-center col-xl-7 col-lg-7 col-md-5 col-sm-5">
                    <img src="{{ asset('uploads/company/logo/' . $company_detail->image) }}" alt="" class="bannerimg">
                </div>
                <div class="col-xl-4 col-sm-5 col-md-5 g-0 container mt-2">
                    <h1 class="mb-5 me-4">The Best Home Delivery Platform Ever</h1>
                    <div class="mt-5">
                        <button type="button" class="btn btn-outline-light rounded-pill m-3">Learn More..</button>
                        <button type="button" class="btn btn-light rounded-pill m-3 px-5"
                            style="color: #0077B6 !important;">Download Now</button>
                    </div>
                </div>
            </div>
        </section>
        <section class="pt-5 mb-4 mb-md-5 mt-5">
            <div class="container">
                <div class="row g-0 bg-light rounded-3">
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="py-3">
                            {{-- @dd($company_detail) --}}
                            {{-- @foreach ($company_detail as $detail) --}}
                            <h2 class="mb-5">{{ $company_detail->title }}</h2>
                            @foreach ($course as $c)
                                <p class="p">
                                    {{ $c->description }}
                                </p>
                            @endforeach
                            {{-- @endforeach --}}

                        </div>
                    </div>
                    <div class="col-xl-1 col-lg-1 col-md-1">
                    </div>
                    <div class="col-xl-5 col-lg-5 col-md-5">
                        @foreach ($course as $c)
                            <div class="">
                                <img class="d-block" src="{{ asset('uploads/course/images/', $c->image) }}"
                                    width="479" alt="Delivery">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section class="pt-5 pt-md-5 mb-5 mb-md-5 ">
            <div class="container">
                <!-- Heading-->
                <div class="pt-3 mb-4">
                    <h2 class="pb-5">Earn With Your Vehicle</h2>
                    <div class="text-center">
                        <img src="{{ asset('frontend/dist/img/landing/Mile stone Group.png') }}" width="1010"
                            alt="Delivery">
                    </div>
                    <div class="text-end">
                        <button type="button" class="btn btn-outline-dark rounded-pill px-5 mt-5 me-5">Start
                            Earning</button>
                    </div>
                </div>
            </div>
        </section>
        <!-- Mobile app CTA-->
        <section class="pt-5 mb-4 mb-md-5 mt-5">
            <div class="container">
                <div class="row g-0 bg-light rounded-3">
                    <div class="col-xl-6 g-0 col-lg-6 col-sm-6 col-md-6 ps-4">
                        <div class="py-3 ps-5">
                            <h2>Download the App</h2>
                            <p>
                                Download and avail our service now.
                            </p>
                            <a href=""><img src="{{ asset('frontend/dist/img/landing/App Store Button.png') }}"
                                    width="300" alt=""></a><br>
                            <a href=""><img src="{{ asset('frontend/dist/img/landing/Play Store Button.png') }}"
                                    width="300" alt="" class="mt-n5"></a>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-5 col-sm-6 col-md-6">
                        <div class="mt-5">
                            <img class="d-block" src="{{ asset('frontend/dist/img/landing/screen.png') }}"
                                width="479" alt="Delivery">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Become Courier / Partner CTA-->
        <section class="pt-5 mb-4 mb-md-5 mt-5">
            <div class="container">
                <div class="d-flex justify-content-between align-items-center">
                    <h2 class="m-0">Blog</h2>
                    <h6 class="text-end m-0">Go to Blog<i class="fas fa-arrow-right ms-4"></i></h6>
                </div>
                <div class="container">
                    <div class="row g-0 d-flex justify-content-around bg-light rounded-3 m-5">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                            <div class="py-3">
                                <img class="d-block" src="{{ asset('frontend/dist/img/landing/blog1.png') }}"
                                    width="300" height="300" alt="Delivery">
                                <div class="d-flex justify-content-between align-items-center pt-3">
                                    <h6 class="fs-sm">Nov 12, 2021</h6>
                                    <h6 class="text-end fs-sm">2 min Read</h6>
                                </div>
                                <h6 class="fw-500">Bike Riders deliver our couriers
                                    faster than we expect. How?</h6>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                            <div class="py-3">
                                <img class="d-block" src="{{ asset('frontend/dist/img/landing/blog2.png') }}"
                                    width="300" height="300" alt="Delivery">
                                <div class="d-flex justify-content-between align-items-center pt-3">
                                    <h6 class="fs-sm">Oct 19, 2021</h6>
                                    <h6 class="text-end fs-sm">10 min Read</h6>
                                </div>
                                <h6 class="fw-500">Why shipments arrive late in
                                    land-locked countries like Nepal.</h6>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                            <div class="py-3">
                                <img class="d-block" src="{{ asset('frontend/dist/img/landing/blog3.png') }}"
                                    width="300" height="300" alt="Delivery">
                                <div class="d-flex justify-content-between align-items-center pt-3">
                                    <h6 class="fs-sm">Aug 23, 2020</h6>
                                    <h6 class="text-end fs-sm">5 min Read</h6>
                                </div>
                                <h6 class="fw-500">How to calculate your courier’s
                                    Air fare at home. Here’s How...</h6>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </main>
    <script>
        // (function () {
        //     var app = new Vue({
        //         el: '#distanceCalc',
        //         components:{},
        //         data: {
        //             stops: [],
        //             origin: '',
        //         },
        //
        //         // computed: {
        //         //     quantity: function () {
        //         //         return this.attendees.length;
        //         //     },
        //         //     checkoutTotal: function () {
        //         //         return this.cost * this.quantity;
        //         //     }
        //         // },
        //         // computed:{
        //         //   title(){
        //         //       return 'Book A Trip'+this.
        //         //   }
        //         // },
        //
        //         watch:{
        //             stops(val){
        //                 console.log(val)
        //             }
        //         },
        //         methods: {
        //             changeValue(value,event) {
        //                 setTimeout(()=>{
        //                  this.stops.push($('#'+event.target.id).val());
        //                 },0)
        //                 // this.stops.push(item);
        //             },
        //             calculateDistance: function (index) {
        //                 var pickup=$('#pickup_address').val();
        //                 // var destination = document.querySelectorAll('.orange.juice')
        //                 var destinations = new Array();
        //                 console.log($('.stop_location'));
        //                 $(".stop_location").each(function(){
        //                     destinations.push($(this).val());
        //                 });
        //                 console.log(destinations)
        //                 $.ajax({
        //                     url : 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + pickup + '&destinations=' + destinations +'&mode=driving&key=AIzaSyA8uYcKYhrmYNTjorWnmcJ6potH3xY4QWg',
        //                     type: 'GET',
        //                     success : function(data){
        //                         console.log(data)
        //                         $('#distance_calc').val(data.rows[0].elements[0].distance.text)
        //                         if($('#distance_calc').val() !=''){
        //                             $('#distance_calc').show()
        //                         }
        //                         else {
        //                             $('#distance_calc').hide()
        //                         }
        //
        //
        //                     }
        //                 });
        //
        //             },
        //             add:function () {
        //                 var new_chq_no = parseInt($('#total_chq').val()) +1;
        //                 var new_div = "<div class='mb-3' id='dest_div" + new_chq_no + "'>"
        //                 new_div += "<input class='form-control border border-transparent stop_location' v-model='stops' onchange='changeValue($event.target.value,$event)' type='text' name='destination_address[]' id='dest_address" + new_chq_no + "'  placeholder='DESTINATION ADDRESS " + parseInt($('#total_chq').val()) + "'>"
        //                 new_div += " <input type='hidden' class='stop_long'  name='dest_long[]'>"
        //                 new_div += " <input type='hidden' class='stop_lat' name='dest_lat[]'>"
        //                 new_div += "</div>"
        //
        //                 // var new_input="<input type='text' class='form-control border border-transparent mb-2' id='new_"+new_chq_no+"' placeholder='ADD STOP "+parseInt($('#total_chq').val())+"'>";
        //                 if (parseInt($('#total_chq').val()) == 2) {
        //                     $('.remove').show();
        //                 }
        //                 $('#new_chq').append(new_div);
        //                 $('#total_chq').val(new_chq_no)
        //             },
        //             remove:function (event) {
        //                 event.preventDefault()
        //                 var last_chq_no = $('#total_chq').val();
        //                 if (last_chq_no > 2) {
        //                     $('#dest_div' + last_chq_no).remove();
        //                     calculateDistance()
        //                     $('#total_chq').val(last_chq_no - 1);
        //                     $('.remove').show();
        //                 }
        //                 if (last_chq_no == 3) {
        //                     $('.remove').hide();
        //                 }
        //
        //             }
        //         }
        //     });
        // })();

        $("#distanceCalc").submit(function(e) {

            if (`{{ !$checkUser['isLogin'] }}`) {
                $('#signin-modal').modal('show')
            }
            e.preventDefault(); // avoid to execute the actual submit of the form.
            var form = $(this);
            console.log(form.serialize())
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function(data) {
                    alert(data); // show response from the php script.
                }
            });


        });

        function calculateDistance(pickup, destination) {
            var pickup = $('#pickup_address').val();
            // var destination = document.querySelectorAll('.orange.juice')
            var destinations = new Array();
            $(".stop_location").each(function() {
                destinations.push($(this).val() + '|');
            });
            $.ajax({
                // crossDomain: true,
                // headers: {
                //     "accept": "application/json",
                //     "Access-Control-Allow-Origin":"*"
                // },
                url: 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + pickup +
                    '&destinations=' + destinations + '&mode=driving&key=AIzaSyA8uYcKYhrmYNTjorWnmcJ6potH3xY4QWg',
                // dataType: 'jsonp',
                type: 'GET',
                // cors: true ,
                // contentType:'application/json',
                // secure: true,
                //
                // beforeSend: function(xhr) {
                //     xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
                // },
                success: function(data) {
                    console.log(this.url)
                    console.log(data)


                    var totalDist = 0;
                    var unit = ''

                    for (let i = 0; i < data.rows[0].elements.length; i++) {
                        totalDist += Number(data.rows[0].elements[i].distance.value);
                    }
                    if (totalDist >= 1000) {
                        unit = 'km'
                        totalDist = totalDist / 1000;

                    } else {
                        unit = 'm';
                    }
                    $('#distance_calc').val(totalDist.toFixed(1) + ' ' + unit)

                    if ($('#distance_calc').val() != '') {
                        $('#distance_calc').show()
                    } else {
                        $('#distance_calc').hide()
                    }


                }
            });
        }
        $(document).on('change', '.stop_location', function() {

            // var origins_address = ["Bhim Nidhi Tiwari Marg, Kathmandu, Nepal"] ;// technician locations]
            //  var   destinations_address= ["Ratna Park, रत्न पार्क पथ, Kathmandu, Nepal"]; // customer address
            var locations = $('.stop_location');
            calculateDistance(locations, destination)

            // $.ajax({
            //     url: "https://maps.googleapis.com/maps/api/distancematrix/json",
            //     type: "GET",
            //     data: {
            //         // origin_addresses: [Number($('#pickup_long').val()),Number($('#pickup_lat').val())], // technician locations]
            //         origins: ["Bhim Nidhi Tiwari Marg, Kathmandu, Nepal"], // technician locations]
            //         destinations: ["Rameshworam Hotel Nepal"], // customer address
            //         // origins: $("#origin").val(),
            //         // destination: $("#destinations").val(),
            //         mode: "driving",
            //         key: "AIzaSyA8uYcKYhrmYNTjorWnmcJ6potH3xY4QWg"
            //     },
            //     success: function(data) {
            //         console.log(data);
            //     }
            // });

        })
        $(document).on('keyup click', ".stop_location", function(event) {
            let parent = $(this).closest('div')
            let id = $(event.target.id)
            // var input =
            $(initialize(this));
            // google.maps.event.addDomListener(window, 'load', initialize(id));
            function initialize(e) {
                var autocomplete = new google.maps.places.Autocomplete(e);
                autocomplete.setComponentRestrictions({
                    country: ["np"],
                });
                var el = e.closest('div')
                autocomplete.addListener('place_changed', function() {
                    var place = autocomplete.getPlace();

                    // place variable will have all the information you are looking for.
                    //   console.log(place.formatted_address);
                    //    $(el).find('.stop_location').val(place.name);
                    var locations = $('.stop_location');
                    calculateDistance(pickup, locations)
                    $(el).find('.stop_lat').val(place.geometry['location'].lat());
                    $(el).find('.stop_long').val(place.geometry['location'].lng());
                });
            }
        });
        $("#pickup_address").on("keyup change", function(event) {
            let parent = $(event.target).parent().parent()
            let id = $(this).val();
            var input = $(this)
            calculateDistance()
            // google.maps.event.addDomListener(window, 'load', );
            $(initialize(input[0]))

            function initialize(e) {
                var autocomplete = new google.maps.places.Autocomplete(e);
                autocomplete.setComponentRestrictions({
                    country: ["np"],
                });
                autocomplete.addListener('place_changed', function() {
                    var place = autocomplete.getPlace();
                    // place variable will have all the information you are looking for.
                    $('#pickup_lat').val(place.geometry['location'].lat());
                    $('#pickup_long').val(place.geometry['location'].lng());
                });
            }

        })
        // $("#dest_address").on("keyup change", function (event) {
        //     let parent = $(event.target).parent().parent()
        //     let id = $(this).val();
        //     var input = $(this)
        //
        //     // google.maps.event.addDomListener(window, 'load', );
        //     $(initialize(input[0]))
        //
        //     function initialize(e) {
        //         var autocomplete = new google.maps.places.Autocomplete(e);
        //         autocomplete.setComponentRestrictions({
        //             country: ["np"],
        //         });
        //         autocomplete.addListener('place_changed', function () {
        //             var place = autocomplete.getPlace();
        //             // place variable will have all the information you are looking for.
        //             $('#dest_lat').val(place.geometry['location'].lat());
        //             $('#dest_long').val(place.geometry['location'].lng());
        //         });
        //     }
        //
        //
        // })

        function add() {

            var new_chq_no = parseInt($('#total_chq').val()) + 1;
            var new_div = "<div class='mb-3 form-group' id='dest_div" + new_chq_no + "'>"
            new_div +=
                "<input class='form-control border border-transparent stop_location'  type='text' name='destination_address[]' id='dest_address" +
                new_chq_no + "'  placeholder='&#xf041 &nbsp;DESTINATION ADDRESS " + parseInt($('#total_chq').val()) + "'>"
            new_div += " <input type='hidden' class='stop_long'  name='dest_long[]'>"
            new_div += " <input type='hidden' class='stop_lat' name='dest_lat[]'>"
            new_div += "</div>"

            // var new_input="<input type='text' class='form-control border border-transparent mb-2' id='new_"+new_chq_no+"' placeholder='ADD STOP "+parseInt($('#total_chq').val())+"'>";
            if (parseInt($('#total_chq').val()) == 2) {
                $('.remove').show();
            }
            if (new_chq_no <= 4) {
                $('#new_chq').append(new_div);
                $('#total_chq').val(new_chq_no)
                $('.msg').hide();
            } else {
                $('.msg').show();
            }

        }

        function remove(event) {
            event.preventDefault()
            var last_chq_no = $('#total_chq').val();
            if (last_chq_no > 2) {
                $('#dest_div' + last_chq_no).remove();
                calculateDistance()
                $('#total_chq').val(last_chq_no - 1);
                $('.remove').show();
            }
            if (last_chq_no == 3) {
                $('.remove').hide();
            }
            $('.msg').hide();
        }
    </script>


@endsection
{{-- @section('scripts') --}}
{{-- @endsection --}}
