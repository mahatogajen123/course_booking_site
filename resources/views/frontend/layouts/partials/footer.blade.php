a{{-- <footer class="footer mb-3 col-lg-12 sm-12">
    <div class="container">
        <div class="row mt-3">
            <div class="col-6">
                <div class="mt-n1"><a class="d-inline-block align-middle text-dark" href="#">
                        <img src="{{ asset('frontend/dist/img/landing/deliverylogo.png') }}" alt="footerimg" srcset=""
class="footerlogo">
</a>
</div>
<div>
    <p class="fs-6 mt-2">Visit Help Center</p>
</div>
</div>
<div class="col-6 text-end">
    <p class="fs-3" style="font-weight: 700">Delivery</p>
    <p class="fs-6">Kathmandu, Province 3, Nepal</p>
</div>
</div>

<div class="row mt-2">
    <div class="col-3">
        <p class="fs-xl">Company</p>
        <p class="fs-sm mb-1">About Us</p>
        <p class="fs-sm mb-1">Cpmpany</p>
        <p class="fs-sm mb-1">Career</p>
        <p class="fs-sm mb-1">Affilite With Us</p>
        <p class="fs-sm mb-1">Investors</p>
        <p class="fs-sm mb-1">Contact Us</p>
    </div>
    <div class="col-3">
        <p class="fs-xl">Services</p>
        <p class="fs-sm mb-1">Become a rider</p>
        <p class="fs-sm mb-1">Become a driver</p>
        <p class="fs-sm mb-1">Book courier</p>
    </div>
    <div class="col-3">
        <p class="fs-xl ps-1">Follow Us</p>
        <span class="fs-5 ps-3"><i class="ci-facebook"></i></span>
        <span class="fs-5 ps-2 pe-3"><i class="ci-instagram"></i></span>
        <span class="fs-5"><i class="ci-linkedin"></i></span>
    </div>
    <div class="col-3">
        <div class="text-end">
            <img src="{{ asset('frontend/dist/img/landing/appstore.png') }}" style="height: 70px; width:200px;" alt="">
        </div>
        <div class="mt-3 text-end">
            <img src="{{ asset('frontend/dist/img/landing/playstore.png') }}" style="height: 70px; width:200px" alt="">
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="d-md-flex justify-content-between">
        <div class=" fs-xs opacity-50 text-center text-md-start">© 2021 Delivery Inc.</div>
        <div class="widget widget-links">
            <ul class="widget-list d-flex flex-wrap justify-content-center justify-content-md-start">
                <li class="widget-list-item ms-4"><a class="widget-list-link fs-ms" href="#">Privacy Policy</a>
                </li>
                <li class="widget-list-item ms-4"><a class="widget-list-link fs-ms" href="#">Terms &amp;
                        Conditions</a></li>
                <li class="widget-list-item ms-4"><a class="widget-list-link fs-ms" href="#">Cookies Policy</a>
                </li>
            </ul>
        </div>
    </div>
    <hr class="mb-1">
    <div class="text-center fs-xs">Designed & Developed @ EEE Innovation Ghar Pvt. Ltd</div>
</div>

</div>
</footer>
<!-- Toolbar for handheld devices (Food delivery)--> --}}

<footer class="bg-secondary footer">
    <div class="container">
        <div class="row mt-3">
            <div class="col-xl-6 col-sm-6 col-md-6 col-6">
                <div class="mt-n1"><a class="d-inline-block align-middle text-dark" href="#">
                        <img src="{{ asset('uploads/company/logo/' . $company_detail->image) }}" alt="footerimg"
                            srcset="" class="footerlogo" style="height: 50px; width:90px; border-radius:20px;">
                    </a>
                </div>
                <div>
                    <p class="fs-6 mt-2">Visit Help Center</p>
                </div>
                <div class="row pb-2">
                    <div class="col-md-6 col-sm-6">
                        <div class="widget widget-links pb-2 mb-1">
                            <h3 class="widget-title text-white fs-xl">Company</h3>
                            <ul class="widget-list">
                                <li class="widget-list-item">
                                    <a href="#" class="widget-list-link">About Us</a>
                                </li>
                                <li class="widget-list-item">
                                    <a href="#" class="widget-list-link">Company</a>
                                </li>
                                <li class="widget-list-item">
                                    <a href="#" class="widget-list-link">Career</a>
                                </li>
                                <li class="widget-list-item">
                                    <a href="#" class="widget-list-link">Affilate With Us</a>
                                </li>
                                <li class="widget-list-item">
                                    <a href="#" class="widget-list-link">Investor</a>
                                </li>
                                <li class="widget-list-item">
                                    <a href="#" class="widget-list-link">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="widget widget-links pb-2 mb-1">
                            <h3 class="widget-title text-white fs-xl">Services</h3>
                            <ul class="widget-list">
                                <li class="widget-list-item">
                                    <a href="#" class="widget-list-link">Become a Driver</a>
                                </li>
                                <li class="widget-list-item">
                                    <a href="#" class="widget-list-link">Become a Rider</a>
                                </li>
                                <li class="widget-list-item">
                                    <a href="#" class="widget-list-link">Book a courier</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-sm-6 col-md-6 col-6 text-end">
                <p class="fs-3 footd" style="font-weight: 700">Delivery</p>
                <p class="fs-6">Kathmandu, Province 3, Nepal</p>
                <div class="row pb-2">
                    <div class="col-md-6 col-sm-6">
                        <div class="widget widget-links text-center pb-2 mb-4">
                            <h3 class="widget-title text-white fs-xl">Follow Us</h3>
                            <ul class="widget-list social d-flex justify-content-center">
                                @foreach ($socialmedia as $media)
                                    <li class="widget-list-item" style="background-image:url()">
                                        <a href="{{ $media->url }}" class="widget-list-link fs-5"><span
                                                class="fs-5"><img style="height: 50px;width:50px"
                                                    src="{{ asset('uploads/socialmedia/logo/' . $media->logo) }}"
                                                    alt=""></span></a>
                                    </li>
                                @endforeach



                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="widget pb-2">
                            <div class="flex-wrap text-end">
                                <div class="me-2 mb-2">
                                    <a class="btn-market btn-apple" href="#" role="button">
                                        <span class="btn-market-subtitle">Download on</span>
                                        <span class="btn-market-title">App Store</span>
                                    </a>
                                </div>
                                <div class="me-2 mb-2">
                                    <a class="btn-market btn-google" href="#" role="button">
                                        <span class="btn-market-subtitle">Download on</span>
                                        <span class="btn-market-title">Google Play</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row pb-2">
            <div class="col-md-3 col-sm-3">
                <div class="widget widget-links pb-2 mb-4">
                    <h3 class="widget-title text-white fs-xl">Company</h3>
                    <ul class="widget-list">
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link">About Us</a>
                        </li>
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link">Company</a>
                        </li>
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link">Career</a>
                        </li>
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link">Affilate With Us</a>
                        </li>
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link">Investor</a>
                        </li>
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link">Contact Us</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="widget widget-links pb-2 mb-4">
                    <h3 class="widget-title text-white fs-xl">Services</h3>
                    <ul class="widget-list">
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link">Become a Driver</a>
                        </li>
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link">Become a Rider</a>
                        </li>
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link">Book a courier</a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="widget widget-links pb-2 mb-4">
                    <h3 class="widget-title text-white fs-xl ps-4">Follow Us</h3>
                    <ul class="widget-list d-flex">
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link fs-5 ps-3"><span class="fs-5 ps-3"><i class="ci-facebook fs-5"></i></span></a>
                        </li>
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link fs-5 "><span class="fs-5 ps-3"><i class="ci-instagram"></i></span></a>
                        </li>
                        <li class="widget-list-item">
                            <a href="#" class="widget-list-link fs-5"><span class="fs-5 ps-3"><i class="ci-linkedin"></i></span></a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="widget pb-2 mb-4">
                    <div class="flex-wrap text-end">
                        <div class="me-2 mb-2">
                            <a class="btn-market btn-apple" href="#" role="button">
                                <span class="btn-market-subtitle">Download on</span>
                                <span class="btn-market-title">App Store</span>
                            </a>
                        </div>
                        <div class="me-2 mb-2">
                            <a class="btn-market btn-google" href="#" role="button">
                                <span class="btn-market-subtitle">Download on</span>
                                <span class="btn-market-title">Google Play</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div> -->
        <div class="row mt-2">
            <div class="d-md-flex justify-content-between">
                <div class=" fs-xs opacity-50 text-center text-md-start">© 2021 Delivery Inc.</div>
                <div class="widget widget-links">
                    <ul class="widget-list d-flex flex-wrap justify-content-center justify-content-md-start">
                        <li class="widget-list-item ms-4"><a class="widget-list-link fs-ms" href="#">Privacy
                                Policy</a>
                        </li>
                        <li class="widget-list-item ms-4"><a class="widget-list-link fs-ms" href="#">Terms &amp;
                                Conditions</a></li>
                        <li class="widget-list-item ms-4"><a class="widget-list-link fs-ms" href="#">Cookies
                                Policy</a>
                        </li>
                    </ul>
                </div>
            </div>
            <hr class="mb-1">
            <div class="text-center fs-xs">Designed & Developed @ EEE Innovation Ghar Pvt. Ltd</div>
        </div>
    </div>
</footer>
