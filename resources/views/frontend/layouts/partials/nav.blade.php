<header class="navbar d-block navbar-sticky navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <div class="title"
            style="background-image:url({{ asset('uploads/company/logo/' . $company_detail->image) }}); height:100px; width:150px; background-size:cover; border-radius:50%;">
            <a class="navbar-brand d-none d-sm-block me-4 order-lg-1" href="https://eeeinnovation.com/">
                <h2 class="text-center pt-4 pl-4 text-black">{{ $company_detail->title }}</h2>
            </a>
        </div>
        <div class="collapse navbar-collapse me-auto order-lg-2" id="navbarCollapse">
            <ul class="navbar-nav ms-lg-10 pe-lg-2 me-lg-2">
                @foreach ($menus as $menu)
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{ $menu->url }}" data-bs-toggle="dropdown"
                            data-bs-auto-close="outside">{{ $menu->title }}</a>
                        @if (count($menu->active_child_menu) > 0)
                            <ul class="dropdown-menu">
                                @foreach ($menu->active_child_menu as $child)
                                    {{-- @if (isset($menu->active_child_menu->title))dropdown @endif --}}
                                    <li class="{{ $child->child_menu ? 'dropdown' : '' }}">
                                        <a class="dropdown-item dropdown-toggle" href="#"
                                            data-bs-toggle="dropdown">{{ $child->title }}</a>
                                        <ul class="dropdown-menu">
                                            @foreach ($child->child_menu as $leaf)
                                                <li><a class="dropdown-item "
                                                        href="{{ $leaf->url }}">{{ $leaf->title }}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="handheld-toolbar">
            <div class="d-table table-layout-fixed w-100">
                <a class="d-table-cell handheld-toolbar-item align-middle" href="index.html">
                    <img src="{{ asset('frontend/dist/img/landing/deliverylogo.png') }}" width="100" alt="Delivery">
                </a>
                <a class="d-table-cell handheld-toolbar-item" href="javascript:void(0)" data-bs-toggle="collapse"
                    data-bs-target="#navbarCollapse" onclick="window.scrollTo(0, 0)">
                    <span class="handheld-toolbar-icon"><i class="ci-menu"></i></span>
                    <span class="handheld-toolbar-label">Menu</span>
                </a>
            </div>
        </div>
    </div>
</header>
