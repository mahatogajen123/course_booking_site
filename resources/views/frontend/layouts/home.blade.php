<!DOCTYPE html>
<html lang="en">
@php header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); @endphp

<head>
    <meta charset="utf-8">
    <title>Delivery</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Delivery - EEE Platform">
    <meta name="keywords" content="Delivery service">
    <meta name="author" content="EEE innovation ghar">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('frontend/dist/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('frontend/dist/frontend/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('frontend/dist/favicon-16x16.png') }}">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" color="#fe6a6a" href="{{ asset('frontend/dist/safari-pinned-tab.svg') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!-- Vendor Styles including: Font Icons, Plugins, etc.-->
    <link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet'>
    <link rel="stylesheet" media="screen"
        href="{{ asset('frontend/dist/vendor/simplebar/dist/simplebar.min.css') }}" />
    <link rel="stylesheet" media="screen"
        href="{{ asset('frontend/dist/vendor/tiny-slider/dist/tiny-slider.css') }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" media="screen" href="{{ asset('dist/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" media="screen"
        href="{{ asset('dist/plugins/datetimepicker/bootstrap-datetimepicker.css') }}">
    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="{{ asset('frontend/dist/css/theme.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('frontend/dist/css/style.css') }}">
    <script src="{{ asset('/dist/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/dist/plugins/jquery/jquery.slim.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('/dist/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/js/vue.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ env('Google_Places') }}&libraries=places&callback=initMap&channel=GMPSB_addressselection_v1_cABC"
        async defer></script>

</head>
<!-- Body-->

<body class="handheld-toolbar-enabled">
    {{-- @include('frontend.layouts.partials.nav') --}}
    <!-- Sign in / sign up modal-->
    {{-- @yield('content') --}}
    <!-- Footer-->
    {{-- @include('frontend.layouts.partials.footer') --}}
    <!-- Toolbar for handheld devices (Food delivery)-->
    Hii this is dashboard

    <!-- Back To Top Button--><a class="btn-scroll-top" href="#top" data-scroll><span
            class="btn-scroll-top-tooltip text-muted fs-sm me-2">Top</span><i class="btn-scroll-top-icon ci-arrow-up">
        </i></a>
    <!-- Vendor scrits: js libraries and plugins-->
    <script src="{{ asset('frontend/dist/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('dist/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('dist/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('frontend/dist/vendor/simplebar/dist/simplebar.min.js') }}"></script>
    <script src="{{ asset('frontend/dist/vendor/tiny-slider/dist/min/tiny-slider.js') }}"></script>
    <script src="{{ asset('frontend/dist/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js') }}"></script>

    <!-- Main theme script-->
    <script src="{{ asset('frontend/dist/js/theme.min.js') }}"></script>
    <script src="{{ asset('dist/plugins/bootstrap\js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('/dist/plugins/jquery/jquery.min.js') }}"></script>
    {{-- <!-- jQuery UI 1.11.4 --> --}}
    <script src="{{ asset('/dist/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    @yield('scripts')

</body>

</html>
