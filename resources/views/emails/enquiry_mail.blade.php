@component('mail::message')
<b>{{ $details['title'] }}</b>

{{ $details['body'] }}

@component('mail::button', ['url' => $details['url']])
        visit our website
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
