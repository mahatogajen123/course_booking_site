
@component('mail::message')
<b>{{$details['title']}}</b>

{{$details['body']}}
!['Should appear image'](https://assets-api.kathmandupost.com/thumb.php?src=https://assets-cdn.kathmandupost.com/uploads/source/news/2022/opinion/shutterstock1358850531-1642092669.jpg&w=900&height=601)
@component('mail::button', ['url'=>$details['url']])
Visit Our Website
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
