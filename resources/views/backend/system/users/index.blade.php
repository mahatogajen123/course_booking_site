@extends('backend.system.layouts.master')
@section('content')
    @include('flash-message')
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card border-info">
                        <div class="card-header border-info">
                            <div class='row'>
                                <div class="col-6 d-inline-flex">

                                    <a class="d-flex align-item-center mt-1 btn-outline-info rounded-pill btn-icon"
                                        href="{{ route('admin.users.index') }}" title="Refresh">
                                        <i class="fas fa-redo fa-1x"></i>
                                    </a>
                                    <form action="{{ route('admin.users.index') }}"
                                        class='d-flex align-items-center search' style="margin-left: 20px;" method="GET">

                                        <div>
                                            <input type="text" value="" placeholder="Search..." class="form-control"
                                                name="search" required />
                                        </div>
                                        <button type="submit" class="btn btn-info btn-sm btn-shadow me-1">Search</button>
                                    </form>
                                </div>
                                <div class="col-6">

                                    <a href="{{ route('admin.users.create') }}" class="btn btn-info btn-sm float-right">
                                        <i data-feather="plus"></i>
                                        New User </a>
                                </div>

                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-hover" id="datatable">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No.</th>
                                        <th scope="col"> Name</th>
                                        <th scope="col">Email Address</th>
                                        <th scope="col">Address</th>
                                        <th scope="col">Role</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $key => $user)
                                        <tr>
                                            {{-- <td>{{$users->currentPage() * $users->perPage() - $users->perPage() + 1}}</td> --}}
                                            <td class="align-middle">{{ ++$key }}</td>
                                            <td style="text-transform: capitalize;" class="align-middle">
                                                @if ($user->HasRole('admin'))
                                                    <a href="#"> {{ $user->full_name }}</a>
                                                @else
                                                    {{ $user->full_name }}
                                                @endif
                                            </td>
                                            <td class="align-middle">
                                                {{ $user->email }}

                                            </td>

                                            <td class="align-middle" style="text-transform: capitalize;">
                                                {{ $user->address }}

                                            </td>
                                            <td class="align-middle">
                                                @if (!empty($user->getRoleNames()))
                                                    @foreach ($user->getRoleNames() as $v)
                                                        <label class="badge badge-success">{{ ucfirst($v) }}</label>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td class="align-middle">
                                                <a href="{{ route('admin.users.edit', $user->id) }}"
                                                    class="btn btn-outline-accent btn-icon">
                                                    <i class="ci-edit"></i>
                                                </a>
                                                &nbsp;
                                                <a href="{{ route('admin.users.delete', $user->id) }}"
                                                    onclick="return confirm('Are you sure ?')"
                                                    class="btn btn-outline-danger btn-icon">
                                                    <i class="ci-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- {{-- Pagination --}} -->
                            {{-- <div class="d-flex justify-content-center"> --}}
                            {{-- {!! $users->links() !!} --}}
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content-wrapper -->
@endsection
