@extends('backend.system.layouts.master')
@section('content')

<div class="content-header">
    <div class="container">

        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">New User</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">
                            User </a></li>
                    <li class="breadcrumb-item active">New User</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
</div>

        <!-- /.content-header -->
       <section class="container">
           <section class="content">
               <div class="row">
                   <div class="col-12">
                       <div class="card">
                           <div class="card-body">
                               <form method="POST" enctype="multipart/form-data" action="{{ route('admin.users.store') }}">
                                   @csrf
                                   <div class="row">
                                       <div class="form-group col-6">
                                           <label for="en_name">First Name</label>
                                           <input type="text" name="first_name" class="form-control" id="en_name"
                                                  placeholder="User First Name" required>
                                           @if ($errors->has('first_name'))
                                               <p style="color: red">
                                                   {{ $errors->first('first_name') }}
                                               </p>
                                           @endif
                                       </div>
                                       <div class="form-group col-6">
                                           <label for="np_name"> Last Name</label>
                                           <input type="text" name="last_name" class="form-control" id="np_name"
                                                  placeholder="User Last Name" required>
                                           @if ($errors->has('last_name'))
                                               <p style="color: red">
                                                   {{ $errors->first('last_name') }}
                                               </p>
                                           @endif
                                       </div>
                                       <div class="form-group col-6">
                                           <label for="password">Password</label>
                                           <input type="password" name="password" class="form-control" id="password"
                                                  placeholder="Password">
                                           @if ($errors->has('password'))
                                               <p style="color: red">
                                                   {{ $errors->first('password') }}
                                               </p>
                                           @endif
                                       </div>
                                       <div class="form-group col-6">
                                           <label for="gender">Gender</label>
                                           <div>
                                               <select class="form-control select2" aria-label="Default select example" name="gender">
                                                   <option >Select a gender</option>
                                                   @foreach($genders as $key=>$value)
                                                       <option  value="{{$key}}">{{$value}}</option>
                                                   @endforeach
                                               </select>
                                               @if ($errors->has('gender'))
                                                   <p style="color: red">
                                                       {{ $errors->first('gender') }}
                                                   </p>
                                               @endif
                                           </div>
                                       </div>


                                       <div class="form-group col-6">
                                           <label for="designation">Contact No</label>
                                           <input type="text" name="contact_number" class="form-control" id="designation"
                                                  placeholder="User Contact Number" required>
                                           @if ($errors->has('contact_number'))
                                               <p style="color: red">
                                                   {{ $errors->first('contact_number') }}
                                               </p>
                                           @endif
                                       </div>
                                       <div class="form-group col-6">
                                           <label for="designation">Alt Contact No</label>
                                           <input type="text" name="alternate_contact_number" class="form-control" id="designation"
                                                  placeholder="Alternate Contact Number" required>
                                           @if ($errors->has('alternate_contact_number'))
                                               <p style="color: red">
                                                   {{ $errors->first('alternate_contact_number') }}
                                               </p>
                                           @endif
                                       </div>

                                       <div class="form-group col-6">
                                           <label for="address">Address</label>
                                           <input type="text" name="address" class="form-control" id="address"
                                                  placeholder="User address" required>
                                           @if ($errors->has('address'))
                                               <p style="color: red">
                                                   {{ $errors->first('address') }}
                                               </p>
                                           @endif
                                       </div>

                                       <div class="form-group col-6">
                                           <label for="email">Email Address</label>
                                           <input type="text" name="email" class="form-control" id="email"
                                                  placeholder="Email Address" required>
                                           @if ($errors->has('email'))
                                               <p style="color: red">
                                                   {{ $errors->first('email') }}
                                               </p>
                                           @endif
                                       </div>


                                       <div class="form-group col-6">
                                           <label for="role">Role</label>
                                           <div>
                                               <select class="form-control select2" aria-label="Default select example" name="roles">
                                                   <option selected>Select a role</option>
                                                   @foreach($roles as $role)
                                                       <option value="{{$role}}">{{$role}}</option>
                                                   @endforeach
                                               </select>
                                               @if ($errors->has('roles'))
                                                   <p style="color: red">
                                                       {{ $errors->first('roles') }}
                                                   </p>
                                               @endif
                                           </div>
                                       </div>
                                       <div class="form-group col-6">
                                           <label for="role"> Profile Image</label>
                                           <input type="file" name="profile_image" class="form-control"   id="role"
                                                  placeholder="image" >
                                           @if ($errors->has('profile_image'))
                                               <p style="color: red">
                                                   {{ $errors->first('profile_image') }}
                                               </p>
                                           @endif
                                       </div>
                                   </div>

                                   <button type="submit" class="btn btn-info">Create</button>
                                   <a href="{{ route('admin.users.index') }}" class="btn btn-info">Cancel</a>
                               </form>
                           </div>
                       </div>
                   </div>

               </div>
           </section>
        </section>
    <!-- /.content-wrapper -->
@endsection
