@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
            height: 245px;
            max-height: 250px;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Team Member</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">Edit Team Member</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.team.update', $team->id) }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="title">Name</label>
                                        <input type="text" name="name" class="form-control" id="name"
                                            placeholder="Enter name" value="{{ $team->name }}">
                                        @if ($errors->has('name'))
                                            <p style="color: red">
                                                {{ $errors->first('name') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Designation</label>
                                        <input type="text" name="designation" class="form-control" id="designation"
                                            placeholder="Enter meta title" value="{{ $team->designation }}">
                                        @if ($errors->has('designation'))
                                            <p style="color: red">
                                                {{ $errors->first('designation') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="designation">Contact</label>
                                        <input type="phone" name="contact" class="form-control" id="contact"
                                            placeholder="Enter description" value="{{ $team->contact }}">
                                        @if ($errors->has('contact'))
                                            <p style="color: red">
                                                {{ $errors->first('contact') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="designation">Education</label>
                                        <input type="text" name="education" class="form-control" id="education"
                                            placeholder="Enter education" value="{{ $team->education }}">
                                        @if ($errors->has('education'))
                                            <p style="color: red">
                                                {{ $errors->first('education') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="col col-6">
                                        <div class="row">
                                            <div class="form-group col-10">
                                                <label for="image">Image</label>
                                                <input type="file" name="image" class="form-control"
                                                    onchange="TeamImgPreview(this);" placeholder="image">
                                                @if ($errors->has('image'))
                                                    <p style="color: red">
                                                        {{ $errors->first('image') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-2">
                                                <label for="">Image</label>
                                                <img id="previewImg"
                                                    src="{{ asset('uploads/team/images/' . $team->image) }}" alt="icon"
                                                    style="height:40px; width:40px; border-radius:50%;object-fit:cover;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="designation">Description</label>
                                        <textarea name="description" id="summernote" cols="15"
                                            rows="6">{{ $team->description }}</textarea>
                                        @if ($errors->has('description'))
                                            <p style="color: red">
                                                {{ $errors->first('description') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Update</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.slider.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
    <script>
        function TeamImgPreview(input) {
            var file = $("input[type=file]").get(0).files[0];
            console.log(file);
            if (file) {
                var reader = new FileReader();

                reader.onload = function() {
                    $("#previewImg").attr("src", reader.result);
                }

                reader.readAsDataURL(file);
            }
        }
    </script>
@endsection
