@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">NewCompany Detail</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active"> New Company Detail</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.company.detail.store') }}">
                                @csrf
                                <div class="row">
                                    {{-- <div class="form-group col-6 ">
                                        <label for="role">Category </label>
                                        <select class="form-control select2" name="blogcategory_id" id="blogcategory_id">
                                            @foreach ($blogcategory as $type)
                                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <p style="color: red">
                                                {{ $errors->first('category_id') }}
                                            </p>
                                        @endif
                                    </div> --}}
                                    <div class="form-group col-6">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title"
                                            placeholder="Enter title" value="{{ old('title') }}">
                                        @if ($errors->has('title'))
                                            <p style="color: red">
                                                {{ $errors->first('title') }}
                                            </p>
                                        @endif
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="copyright">Copyright</label>
                                        <input type="text" name="copyright" class="form-control" id="copyright"
                                            placeholder="Enter meta title" value="{{ old('copyright') }}">
                                        @if ($errors->has('copyright'))
                                            <p style="color: red">
                                                {{ $errors->first('copyright') }}
                                            </p>
                                        @endif
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="image">Logo</label>
                                        <input type="file" name="logo" class="form-control" id="image" placeholder="image"
                                            value="{{ old('logo') }}">
                                        @if ($errors->has('logo'))
                                            <p style="color: red">
                                                {{ $errors->first('logo') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="slogan">Slogan</label>
                                        <input type="text" name="slogan" class="form-control" id="slogan"
                                            placeholder="Enter meta title" value="{{ old('slogan') }}">
                                        @if ($errors->has('slogan'))
                                            <p style="color: red">
                                                {{ $errors->first('slogan') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="slogan">About</label>
                                        {{-- <input type="text" name="slogan" class="form-control" id="copyright"
                                            placeholder="Enter meta title" value="{{ old('copyright') }}"> --}}
                                        <textarea name="about" id="summernote" cols="30" rows="4"></textarea>
                                        @if ($errors->has('about'))
                                            <p style="color: red">
                                                {{ $errors->first('about') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="slogan">Moto & Goals</label>
                                        {{-- <input type="text" name="slogan" class="form-control" id="copyright"
                                            placeholder="Enter meta title" value="{{ old('copyright') }}"> --}}
                                        <textarea name="moto" id="summernote1" cols="30" rows="4"></textarea>
                                        @if ($errors->has('moto'))
                                            <p style="color: red">
                                                {{ $errors->first('moto') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Create</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.company.detail.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
