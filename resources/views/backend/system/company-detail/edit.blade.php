@extends('backend.system.layouts.master')
@section('content')
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Company Detail</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">Edit Company Detail</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.company.detail.update', $company->id) }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title"
                                            placeholder="Enter title" value="{{ $company->title }}" required>
                                        @if ($errors->has('title'))
                                            <p style="color: red">
                                                {{ $errors->first('title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Slogan</label>
                                        <input type="text" name="slogan" class="form-control" id="slogan"
                                            placeholder="Enter meta title" value="{{ $company->slogan }}" required>
                                        @if ($errors->has('slogan'))
                                            <p style="color: red">
                                                {{ $errors->first('slogan') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Copyright</label>
                                        <input type="text" name="copyright" class="form-control" id="copyright"
                                            placeholder="Enter meta title" value="{{ $company->copyright }}" required>
                                        @if ($errors->has('copyright'))
                                            <p style="color: red">
                                                {{ $errors->first('copyright') }}
                                            </p>
                                        @endif
                                    </div>

                                    <div class="form-group col-6">
                                        <div class="row">
                                            <div class="form-group col-10">
                                                <label for="logo">Logo</label>
                                                <input type="file" name="logo" class="form-control "
                                                    onchange="bloImgpreview(this);" placeholder="logo">
                                                @if ($errors->has('logo'))
                                                    <p style="color: red">
                                                        {{ $errors->first('logo') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-2">
                                                <label for="">logo</label>
                                                <img id="previewImg"
                                                    src="{{ asset('uploads/company/logo/' . $company->logo) }}"
                                                    alt="icon"
                                                    style="height:40px; width:40px; border-radius:50%;object-fit:cover;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="slogan">About</label>
                                        {{-- <input type="text" name="slogan" class="form-control" id="copyright"
                                            placeholder="Enter meta title" value="{{ old('copyright') }}"> --}}
                                        <textarea name="about" id="summernote" cols="30" rows="4">
                                                                {{ $company->about }}
                                                            </textarea>
                                        @if ($errors->has('about'))
                                            <p style="color: red">
                                                {{ $errors->first('about') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="slogan">Moto & Goals</label>
                                        {{-- <input type="text" name="slogan" class="form-control" id="copyright"
                                            placeholder="Enter meta title" value="{{ old('copyright') }}"> --}}
                                        <textarea name="moto" id="summernote1" cols="30" rows="4">
                                                                {{ $company->moto }}
                                                            </textarea>
                                        @if ($errors->has('moto'))
                                            <p style="color: red">
                                                {{ $errors->first('moto') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Update</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.company.detail.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
    <script>
        function bloImgpreview(input) {
            var file = $("input[type=file]").get(0).files[0];
            // var file = document.getElementsByClassName('blogEditImg')[0].value.replace(/^.*\\/, "");
            // console.log(file);
            if (file) {
                var reader = new FileReader();

                reader.onload = function() {
                    $("#previewImg").attr("src", reader.result);
                }

                reader.readAsDataURL(file);
            }
        }
    </script>
@endsection
