@extends('backend.system.layouts.master')
@section('content')
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Blog</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">Edit Blog</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.social.media.update', $socialmedia->id) }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="title">Name</label>
                                        <input type="text" name="name" class="form-control" id="name"
                                            placeholder="Enter social media name" value="{{ $socialmedia->name }}"
                                            required>
                                        @if ($errors->has('name'))
                                            <p style="color: red">
                                                {{ $errors->first('name') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">URL</label>
                                        <input type="url" name="url" class="form-control" id="url"
                                            placeholder="Enter meta title" value="{{ $socialmedia->url }}" required>
                                        @if ($errors->has('url'))
                                            <p style="color: red">
                                                {{ $errors->first('url') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <div class="row">
                                            <div class="form-group col-10">
                                                <label for="logo">Logo</label>
                                                <input type="file" name="logo" class="form-control blogEditImg"
                                                    onchange="bloImgpreview(this);" placeholder="logo">
                                                @if ($errors->has('logo'))
                                                    <p style="color: red">
                                                        {{ $errors->first('logo') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-2">
                                                <label for="">Image</label>
                                                <img id="previewImg"
                                                    src="{{ asset('uploads/socialmedia/logo/' . $socialmedia->logo) }}"
                                                    alt="icon"
                                                    style="height:40px; width:40px; border-radius:50%;object-fit:cover;">
                                            </div>
                                        </div>
                                        @if ($errors->has('image'))
                                            <p style="color: red">
                                                {{ $errors->first('image') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Update</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.social.media.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
    <script>
        function bloImgpreview(input) {
            var file = $("input[type=file]").get(0).files[0];
            // var file = document.getElementsByClassName('blogEditImg')[0].value.replace(/^.*\\/, "");
            // console.log(file);
            if (file) {
                var reader = new FileReader();

                reader.onload = function() {
                    $("#previewImg").attr("src", reader.result);
                }

                reader.readAsDataURL(file);
            }
        }
    </script>
@endsection
