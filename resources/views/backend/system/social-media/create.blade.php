@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New Social Media</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active"> New Social Media</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.social.media.store') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="title">Name</label>
                                        <input type="text" name="name" class="form-control" id="name"
                                            placeholder="Enter social medai name" value="{{ old('name') }}">
                                        @if ($errors->has('name'))
                                            <p style="color: red">
                                                {{ $errors->first('name') }}
                                            </p>
                                        @endif
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="copyright">URL</label>
                                        <input type="url" name="url" class="form-control" id="url"
                                            placeholder="Enter url " value="{{ old('url') }}">
                                        @if ($errors->has('url'))
                                            <p style="color: red">
                                                {{ $errors->first('url') }}
                                            </p>
                                        @endif
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="image">Logo</label>
                                        <input type="file" name="logo" class="form-control" id="logo" placeholder="Logo"
                                            value="{{ old('logo') }}">
                                        @if ($errors->has('logo'))
                                            <p style="color: red">
                                                {{ $errors->first('logo') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Create</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.company.detail.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
