@extends('backend.system.layouts.master')
@section('content')
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Blog</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">Edit Blog</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.blog.update', $blog->id) }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title"
                                            placeholder="Enter title" value="{{ $blog->title }}" required>
                                        @if ($errors->has('title'))
                                            <p style="color: red">
                                                {{ $errors->first('title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Sub Title</label>
                                        <input type="text" name="sub_title" class="form-control" id="sub_title"
                                            placeholder="Enter meta title" value="{{ $blog->sub_title }}" required>
                                        @if ($errors->has('sub_title'))
                                            <p style="color: red">
                                                {{ $errors->first('sub_title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6 ">
                                        <label for="role">Blog Category </label>
                                        <select class="form-control select2" name="blogcategory_id" id="blogcategory_id">
                                            @foreach ($blogcategory as $type)
                                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('blogcategory_id'))
                                            <p style="color: red">
                                                {{ $errors->first('blogcategory_id') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <div class="row">
                                            <div class="form-group col-10">
                                                <label for="image">Image</label>
                                                <input type="file" name="image" class="form-control blogEditImg"
                                                    onchange="bloImgpreview(this);" placeholder="image">
                                                @if ($errors->has('image'))
                                                    <p style="color: red">
                                                        {{ $errors->first('image') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-2">
                                                <label for="">Image</label>
                                                <img id="previewImg"
                                                    src="{{ asset('uploads/blog/images/' . $blog->image) }}" alt="icon"
                                                    style="height:40px; width:40px; border-radius:50%;object-fit:cover;">
                                            </div>
                                        </div>
                                        @if ($errors->has('image'))
                                            <p style="color: red">
                                                {{ $errors->first('image') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Short Detail</label>
                                        {{-- <input type="text" name="short_detail" class="form-control" id=""
                                            placeholder="Enter Og meta title" required> --}}
                                        <textarea name="short_detail" cols="30" rows="10" value="" class="form-control"
                                            id="summernote1">{{ $blog->short_detail }} </textarea>
                                        @if ($errors->has('short_detail'))
                                            <p style="color: red">
                                                {{ $errors->first('short_detail') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Detail</label>
                                        {{-- <input type="text" name="short_detail" class="form-control" id=""
                                            placeholder="Enter Og meta title" required> --}}
                                        <textarea name="detail" cols="30" rows="10" value="" class="form-control"
                                            id="summernote">{{ $blog->detail }}</textarea>
                                        @if ($errors->has('detail'))
                                            <p style="color: red">
                                                {{ $errors->first('detail') }}
                                            </p>
                                        @endif
                                    </div>


                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Update</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.blog.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
    <script>
        function bloImgpreview(input) {
            var file = $("input[type=file]").get(0).files[0];
            // var file = document.getElementsByClassName('blogEditImg')[0].value.replace(/^.*\\/, "");
            // console.log(file);
            if (file) {
                var reader = new FileReader();

                reader.onload = function() {
                    $("#previewImg").attr("src", reader.result);
                }

                reader.readAsDataURL(file);
            }
        }
    </script>
@endsection
