<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard</title>

    <!-- Google Font: Source Sans Pro -->
    <!-- <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet'>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('/dist/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/styles.css') }}">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet"
        href="{{ asset('/dist/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- iCheck -->
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/dist/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/dist/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/dist/plugins/summernote/summernote-bs4.min.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('/dist/css/adminlte.min.css') }}">  -->
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('frontend/dist/vendor/chartist/dist/chartist.min.css') }}">

    <link rel="stylesheet" media="screen" href="{{ asset('frontend/dist/css/theme.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('/dist/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="{{ asset('dist/css/bootstrap-toggle.css') }}">
    {{-- dropzone --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
    <style>
        .table-hover tbody tr:hover {
            background-color: #4baadd;
        }

        body {
            font-family: 'Ubuntu';
        }

        .navbar {
            padding: .5rem .5rem;
        }

        .btn-info {
            background-color: #0077B6;
            border-color: #0077B6;
        }

        .nav-link:not(.dropdown-toggle).active {
            pointer-events: auto !important;
        }

        .btn-outline-info {
            color: #0077B6;
            border-color: #0077B6;
        }

        .border-info {
            border-color: #0077B6 !important;
        }

        .btn-outline-info:hover {
            /* color: rgba(8, 1, 1, 0.178); */
            color: rgba(68, 21, 177, 0.178);
            background-color: #0077B6;
            border-color: #0077B6;
        }

        .btn-cancel {
            background-color: #FD5401;
            border-color: #FD5401;
            color: #fff;
        }

        .navbar-expand .navbar-nav .nav-link {
            padding-top: .875rem;
            padding-bottom: .875rem;
            font-weight: normal;
        }

        .select2 {
            padding: 0rem 1rem;
        }

        .nav-pills.flex-column .nav-item {
            margin-right: 0;
            margin-bottom: 4px !important;
        }

        .nav-sidebar .nav-treeview {
            margin-top: 4px;
        }

        .nav-link {
            display: block;
            padding: 0.5rem 1.25rem !important;
        }

        .search input {
            border-top-left-radius: 0.3125rem;
            border-bottom-left-radius: 0.3125rem;
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .search .btn-sm {
            padding: 0.528rem 1rem;
            font-size: 0.8125rem;
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
            border-top-right-radius: 0.3125rem;
            border-bottom-right-radius: 0.3125rem;
        }

        .table td,
        .table th {
            padding: 0.1rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }

        .btn-icon {
            padding: 0.5rem .7rem;
        }

        .card {
            box-shadow: 0 0 1px rgba(40, 147, 255, 0.35), 0 1px 3px rgba(40, 147, 255, 0.35) !important;
        }

        .form-control:focus {
            color: #4b566b;
            background-color: #fff;
            border-color: rgba(40, 147, 255);
            outline: 0;
            box-shadow: inset 0 1px 2px transparent, 0 .375rem .625rem -0.3125rem rgba(254, 105, 106, .15);
        }

        .breadcrumb-item>a:hover {
            color: #0077B6;
        }

        .form-check-input:checked {
            background-color: #0077B6;
            border-color: #0077B6;
        }

        .form-switch .form-check-input:checked {
            background-color: #0077B6;
            box-shadow: 0 .375rem .875rem -0.3rem #0077B6;
        }

        .navbar-light .nav-item:hover .nav-link:not(.disabled),
        .navbar-light .nav-item:hover .nav-link:not(.disabled)>i,
        .dropdown-item:hover,
        .dropdown-item:active,
        .dropdown-item.active {
            color: #0077B6;
        }


        .layout-navbar-fixed .wrapper .sidebar-light-primary .brand-link:not([class*=navbar]) {
            background-color: #0077B6;
            height: 75px;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #0077B6;
            border-color: #0077B6;
            color: #fff;
            padding: 0 10px;
            margin-top: .31rem;
        }

        .content-header {
            padding-top: 33px;
        }

    </style>
    <script src="{{ asset('/dist/plugins/jquery/jquery.min.js') }}"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('/dist/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/dist/js/adminlte.js') }}"></script>
    <script src="{{ asset('/dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/dist/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('/dist/plugins/summernote/summernote-bs4.min.js') }}"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    {{-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> --}}
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>





</head>

{{-- <body class="sidebar-mini layout-fixed control-sidebar-slide-open layout-navbar-fixed"> --}}

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        @include('backend.system.layouts.partials.navbar')


        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('backend.system.layouts.partials.sidebar')

        <div class="content-wrapper">
            @yield('content')

        </div>
    </div>
    {{-- <script src="{{ asset('dist/js/bootstrap-toggle.min.js') }}"></script> --}}
    <script src="{{ asset('frontend/dist/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js') }}"></script>
    <script src="{{ asset('frontend/dist/vendor/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('frontend/dist/js/theme.min.js') }}"></script>
    <script src="{{ asset('/dist/js/demo.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>


    <script>
        $(document).ready(function() {
            setTimeout(function() {
                $(".alert").alert('close');
            }, 2000);

            $('.statusUpdate').on("change", function() {
                $('#' + $(this).closest('form').attr('id')).submit();
            });
        });

        $(document).ready(function() {
            $('#summernote').summernote({
                height: 150
            });
        });
        $(document).ready(function() {
            $('#summernote1').summernote({
                height: 150
            });
        });
    </script>
</body>

</html>
