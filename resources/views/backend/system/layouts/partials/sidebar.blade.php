<style>
    aside {
        background-color: #0077B6 !important;
    }

    .s-bar {
        color: white !important;
    }

    .user-panel {
        margin-top: 21px !important;
        padding-top: 3px;

    }

    .nav-pills .nav-link {
        background-color: transparent;
    }

    .nav-pills .nav-link:hover:not(.active):not([aria-expanded="true"]),
    .nav-pills .nav-link.show {
        color: black !important;
        background-color: #fff;
    }

    .nav-pills.flex-column .nav-item {
        margin-right: 0;
        margin-bottom: 0;
    }

    .nav-sidebar .nav-item>.nav-link {
        margin-bottom: 0;
    }

    [class*="sidebar-light-"] .nav-sidebar>.nav-item.menu-open>.nav-link,
    [class*="sidebar-light-"] .nav-sidebar>.nav-item:hover>.nav-link {
        background-color: #fff;
        color: black !important;
    }

    [class*="sidebar-light-"] .nav-treeview>.nav-item>.nav-link {
        padding-left: 42px !important;
    }

    .nav-sidebar .nav-link>p>.right {
        position: absolute;
        right: 1rem;
        top: 1.1rem;
    }

    .sidebar-light-primary .nav-sidebar>.nav-item>.nav-link.active {
        background-color: #f00e21;
        color: white !important;
    }

    [class*="sidebar-light-"] .nav-treeview>.nav-item>.nav-link.active {
        background-color: #FD5401;
        color: white;
    }

    .nav-pills .nav-link.active {
        box-shadow: none;
    }

</style>
<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{ asset('/dist/img/AdminLTELogo.png') }}" alt=" Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8; background-color:#0077B6">
        <span class="brand-text font-weight-light s-bar">{{ config('app.name') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class=" s-bar d-block">{{ __('setting.main_navigation') }}</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ url('/admin') }}" class="nav-link s-bar">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>
                            {{ __('setting.setting') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.roles.index') }}" class="nav-link s-bar">
                                <i class="fas fa-shield-alt nav-icon"></i>
                                <p>{{ __('setting.role_permission') }}</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('admin.email-setting.index') }}" class="nav-link s-bar">
                                <i class="ci-settings nav-icon"></i>
                                <p>Email settings</p>
                            </a>
                        </li>
                        <li class="nav-item">
                        <li class="nav-item">
                            <a href="{{ route('admin.company.detail.index') }}" class="nav-link s-bar">
                                <i class="nav-icon ci-menu-circle"></i>
                                Company
                            </a>
                        </li>
                </li>
            </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.users.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        {{ __('setting.manage_user') }}
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.users.index', ['role' => 'admin']) }}" class="nav-link s-bar">
                            <i class="fas fa-user-tie nav-icon"></i>
                            <p>{{ __('setting.admin') }}</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.users.index', ['role' => 'user']) }}" class="nav-link s-bar">
                            <i class="far fa-circle nav-icon"></i>
                            <p>User</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.photo.show') }}" class="nav-link s-bar">
                    <i class="nav-icon ci-menu-circle"></i>
                    <p>Gallery
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.photo.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add photos</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.photo.show') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>Show photos</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.menu.index') }}" class="nav-link s-bar">
                    <i class="nav-icon ci-menu-circle"></i>
                    <p>Manage Menu
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.menu.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add Menu</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.menu.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>List Menu</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.course.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-book-reader"></i>
                    <p>Manage Course
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.course.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add Course</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.course.category.index') }}" class="nav-link s-bar">
                            <i class="ci-document"></i>
                            <p>Category</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.course.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>List Course</p>
                        </a>
                    </li>

                </ul>
            </li>
            {{-- <li class="nav-item">
                <a href="{{ route('admin.course.detail.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-book-reader"></i>
                    <p>Manage Course Detail
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.course.detail.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add Course Detail</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.course.detail.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>Course Details</p>
                        </a>
                    </li>

                </ul>
            </li> --}}
            <li class="nav-item">
                <a href="{{ route('admin.partner.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-hands-helping"></i>
                    <p>Manage Partner
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.partner.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add Partner</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.partner.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>List Partner</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.slider.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-sliders-h"></i>
                    <p>Manage Slider
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.slider.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add Slider</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.slider.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>List Slider</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.blog.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-blog"></i>
                    <p>Manage Blog
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.blog.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add Blog</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.blog.category.index') }}" class="nav-link s-bar">
                            <i class="ci-document"></i>
                            <p>Blog Category</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.blog.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>List Blog</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.career.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-briefcase"></i>
                    <p>Manage Career
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.career.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add Career</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('admin.career.index') }}" class="nav-link s-bar">
                            <i class="ci-list"></i>
                            <p>List Career</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.career.applied.index') }}" class="nav-link s-bar">
                            <i class="ci-list"></i>
                            <p>List Applied Career</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.faqs.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-question-circle"></i>
                    <p>Manage Faqs
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.faqs.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add Faqs</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('admin.faqs.index') }}" class="nav-link s-bar">
                            <i class="ci-list"></i>
                            <p>List Faqs</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.lead.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-rainbow"></i>
                    <p>Manage Leads
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.lead.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add Lead</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.lead.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>List Leads</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.service.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-people-carry"></i>
                    <p>Manage Services
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.service.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add Service</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.service.category.index') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Service Category</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.service.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>List Service</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.team.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-sitemap"></i>
                    <p>Manage Team
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.team.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add Team Member</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.team.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>List Team Member</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.portfolio.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-people-arrows"></i>
                    <p>Manage Portfolio
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.portfolio.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.portfolio.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>List </p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.testimonial.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-people-arrows"></i>
                    <p>Manage Testimonial
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.testimonial.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.testimonial.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>List </p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.social.media.index') }}" class="nav-link s-bar">
                    <i class="nav-icon fas fa-people-arrows"></i>
                    <p>Social Media
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.social.media.create') }}" class="nav-link s-bar">
                            <i class="ci-add-circle"></i>
                            <p>Add </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.social.media.index') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>List </p>
                        </a>
                    </li>
                </ul>
            </li>


            <li class="nav-item">
                <a href="{{ route('admin.customer.enquiry') }}" class="nav-link s-bar">
                    <i class="ci-list"></i>
                    <p>Customer Response
                        <i class="fas fa-angle-left right "></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('admin.customer.enquiry') }}" class="nav-link s-bar">
                            <i class="ci-list"></i>
                            <p>Enquiry List</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.customer.getintouch') }}" class="nav-link s-bar">
                            <i class="ci-list
                                "></i>
                            <p>Get In Touch List </p>
                        </a>
                    </li>
                </ul>
            </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
<script>
    var url = window.location;
    // for sidebar menu entirely but not cover treeview
    $('ul.nav-sidebar a').filter(function() {
        //   console.log(this.href,url);

        return this.href == url;
    }).addClass('active');

    // for treeview
    $('ul.nav-treeview a').filter(function() {
        return this.href == url;
    }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
</script>
