<style>
    .navbar-expand {
        background-color: #0077B6;
        background-image: linear-gradient(315deg, #0077B6) 0%, #125a83 74%);
        height: 75px;
    }

    .nav-link {
        color: white !important;
    }

</style>


<nav class="main-header navbar navbar-expand">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route('admin.home') }}" class="nav-link">{{ __('nav.home') }}</a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <div class="nav-item dropdown bg-transparent">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="flag-icon flag-icon-{{ session()->get('locale') == null ? 'us' : session()->get('locale') }} mr-2"
                        id="lang"></i>

                </a>
                <div class="dropdown-menu dropdown-menu-right p-0">
                    @foreach (\Config::get('app.available_locales') as $locale)
                        <a href="{{ route('change.locale', $locale['code']) }}"
                            class="dropdown-item {{ session('locale') == $locale['code'] ? 'active' : '' }}">
                            <i class="flag-icon flag-icon-{{ $locale['flag'] }} mr-2"></i> {{ $locale['name'] }}
                        </a>
                    @endforeach
                </div>
            </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" href="{{ route('admin.logout') }}" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt me-sm-1 " aria-hidden="true"></i>
                <span class="d-sm-inline d-none " wire:click="logout">Sign Out</span>
            </a>
            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li>
    </ul>
</nav>
