@extends('backend.system.layouts.master')
@section('content')
<style>
  .card-body {
      padding: 10px;
  }
  .card [class^="ci-"]{
    font-size: 30px;
  }
  .box{height: 55px;
width: 55px;
padding: 10px;
border-radius: 10%;}
</style>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <!-- <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>

                <p>New Orders</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div> -->
            <!-- Horizontal card layout -->
            <div class="card" style="max-width: 540px;">
              <div class="row g-0">
                <div class="col-sm-4 text-center d-flex justify-content-center align-items-center">
                  <div class="box bg-info "><i class="ci-bag"></i></div>
                </div>
                <div class="col-sm-8">
                  <div class="card-body">
                    <h5 class="card-title" style="margin-bottom: 0;">150</h5>
                    <p class="card-text fs-sm text-muted" style="margin-bottom: 0;">New Orders</p>
                    <a href="#" class="small-box-footer text-info">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="card" style="max-width: 540px;">
              <div class="row g-0">
                <div class="col-sm-4 text-center d-flex justify-content-center align-items-center">
                  <div class="box bg-info "><i class="ci-coins"></i></div>
                </div>
                <div class="col-sm-8">
                  <div class="card-body">
                    <h5 class="card-title" style="margin-bottom: 0;">NRS 5000</h5>
                    <p class="card-text fs-sm text-muted" style="margin-bottom: 0;">Bounce Rate</p>
                    <a href="#" class="small-box-footer text-info">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="small-box bg-success">
              <div class="inner">
                <h3>53<sup style="font-size: 20px">%</sup></h3>

                <p>Bounce Rate</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div> -->
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <!-- <div class="small-box bg-warning">
              <div class="inner">
                <h3>44</h3>

                <p>User Registrations</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div> -->
            <div class="card" style="max-width: 540px;">
              <div class="row g-0">
                <div class="col-sm-4 text-center d-flex justify-content-center align-items-center">
                  <div class="box bg-info"><i class="ci-user"></i></div>
                </div>
                <div class="col-sm-8">
                  <div class="card-body">
                    <h5 class="card-title" style="margin-bottom: 0;">44</h5>
                    <p class="card-text fs-sm text-muted" style="margin-bottom: 0;">User Registrations</p>
                    <a href="#" class="small-box-footer text-info">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <!-- <div class="small-box bg-danger">
              <div class="inner">
                <h3>65</h3>

                <p>Unique Visitors</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div> -->
            <div class="card" style="max-width: 540px;">
              <div class="row g-0">
                <div class="col-sm-4 text-center d-flex justify-content-center align-items-center">
                  <div class="box bg-info"><i class="ci-user-circle"></i></div>
                </div>
                <div class="col-sm-8">
                  <div class="card-body">
                    <h5 class="card-title" style="margin-bottom: 0;">65</h5>
                    <p class="card-text fs-sm text-muted" style="margin-bottom: 0;">Unique Visitors</p>
                    <a href="#" class="small-box-footer text-info">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-chart-pie mr-1"></i>
                  Sales
                </h3>
              </div><!-- /.card-header -->
              <!-- Bar chart: Multiple bars of different color + Legend -->
              <!-- Legend -->
              <div class="ct-chart ct-perfect-fourth" data-line-chart="{&quot;labels&quot;: [&quot;22 Jul&quot;, &quot;&quot;, &quot;24 Jul&quot;, &quot;&quot;, &quot;26 Jul&quot;, &quot;&quot;, &quot;28 Jul&quot;, &quot;&quot;, &quot;30 Jul&quot;, &quot;&quot;, &quot;01 Aug&quot;, &quot;&quot;, &quot;03 Aug&quot;, &quot;&quot;, &quot;05 Aug&quot;], &quot;series&quot;: [[0, 100, 200, 150, 50, 0, 0, 50, 0, 50, 50, 50, 0, 100, 0]]}"></div>
            </div>
            <div class="file-drop-area mb-3">
                    <div class="file-drop-icon ci-cloud-upload"></div><span class="file-drop-message">Drag and drop here to upload product screenshot</span>
                    <input class="file-drop-input" type="file">
                    <button class="file-drop-btn btn btn-primary btn-sm mb-2" type="button">Or select file</button>
                    <div class="form-text">1000 x 800px ideal size for hi-res displays</div>
                  </div>

            <!-- TO DO List -->
            
            <!-- /.card -->
          </section>
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

            <!-- Map card -->
            <div class="card bg-info">
              <div class="card-header border-0">
                <h3 class="card-title">
                  <i class="fas fa-map-marker-alt mr-1"></i>
                  Visitors
                </h3>
                <!-- card tools -->
                <div class="card-tools">
                  <button type="button" class="btn btn-primary btn-sm daterange" title="Date range">
                    <i class="far fa-calendar-alt"></i>
                  </button>
                  <button type="button" class="btn btn-primary btn-sm" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <div class="card-body">
                <div id="world-map" style="height: 250px; width: 100%;"></div>
              </div>
              <!-- /.card-body-->
              <div class="card-footer bg-transparent">
                <div class="row">
                  <div class="col-4 text-center">
                    <div id="sparkline-1"></div>
                    <div class="text-white">Visitors</div>
                  </div>
                  <!-- ./col -->
                  <div class="col-4 text-center">
                    <div id="sparkline-2"></div>
                    <div class="text-white">Online</div>
                  </div>
                  <!-- ./col -->
                  <div class="col-4 text-center">
                    <div id="sparkline-3"></div>
                    <div class="text-white">Sales</div>
                  </div>
                  <!-- ./col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.card -->

            <!-- solid sales graph -->
            <div class="card `">
              <div class="card-header border-0">
                <h3 class="card-title">
                  <i class="fas fa-th mr-1"></i>
                  Sales Graph
                </h3>

                <div class="card-tools">
                  <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn bg-info btn-sm" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <!-- Pie chart: Multiple slices of different color + Legend -->

                <!-- Legend -->
                <div class="d-flex flex-wrap justify-content-center fs-ms">
                  <div class="border rounded py-1 px-2 me-2 mb-2">
                    <div class="d-inline-block align-middle me-1" style="width: .75rem; height: .75rem; background-color: #69b3fe;"></div>
                    <span class="d-inline-block align-middle">Company 1</span>
                  </div>
                  <div class="border rounded py-1 px-2 me-2 mb-2">
                    <div class="d-inline-block align-middle me-1" style="width: .75rem; height: .75rem; background-color: #42d697;"></div>
                    <span class="d-inline-block align-middle">Company 2</span>
                  </div>
                  <div class="border rounded py-1 px-2 me-2 mb-2">
                    <div class="d-inline-block align-middle me-1" style="width: .75rem; height: .75rem; background-color: #f34770;"></div>
                    <span class="d-inline-block align-middle">Company 3</span>
                  </div>
                </div>

                <!-- Chart -->
                <div class="ct-chart ct-perfect-fourth" data-pie-chart='{"series": [5, 3, 4]}', data-series-color='{"colors": ["#69b3fe", "#42d697", "#f34770"]}'></div>              </div>
              <!-- /.card-body -->
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->

            <!-- Calendar -->
            <div class="card bg-gradient-success">
              <div class="card-header border-0">

                <h3 class="card-title">
                  <i class="far fa-calendar-alt"></i>
                  Calendar
                </h3>
                <!-- tools card -->
                <div class="card-tools">
                  <!-- button with a dropdown -->
                  <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" data-offset="-52">
                      <i class="fas fa-bars"></i>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a href="#" class="dropdown-item">Add new event</a>
                      <a href="#" class="dropdown-item">Clear events</a>
                      <div class="dropdown-divider"></div>
                      <a href="#" class="dropdown-item">View calendar</a>
                    </div>
                  </div>
                  <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
                <!-- /. tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body pt-0">
                <!--The calendar -->
                <div id="calendar" style="width: 100%"></div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- /.content -->
@endsection
