@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit FAQs</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">Edit FAQs</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="" method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.faqs.update', $faq->id) }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-12 ">
                                        <label for="title">Question</label>
                                        <input type="text" name="question" value="{{ $faq->question }}"
                                            class="form-control" id="meta_title" placeholder="Enter Question" required>
                                    </div>
                                    <div class="form-group col-12 ">
                                        <label for="title">Answer</label>
                                        {{-- <input type="text" name="answer" value="{!! $v->answer !!}" class="form-control"
                                            id="meta_title" placeholder="Enter Question" required> --}}
                                        <textarea name="answer" id="summernote1" cols="20"
                                            rows="6">{{ $faq->answer }}</textarea>
                                    </div>
                                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                        <button class="btn btn-info me-md-2" type="submit">Update</button>
                                        <a class="btn btn-info" type="button"  href="{{ route('admin.faqs.index') }}">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
