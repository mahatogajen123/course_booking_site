@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New FAQs</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">New FAQs</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data" action="{{ route('admin.faqs.store') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-12 ">
                                        <label for="title">Question</label>
                                        <input type="text" name="question" class="form-control" id="question"
                                            placeholder="Enter Question" value="{{ old('question') }}">
                                        {{-- <textarea name="question" id="" cols="30" rows="7"
                                            class="form-control"></textarea> --}}
                                    </div>
                                    <div class="form-group col-12 ">
                                        <label for="title">Answer</label>
                                        {{-- <input type="text" name="answer" class="form-control" id="answer"
                                            placeholder="Enter Answer" required> --}}
                                        <textarea name="answer" id="summernote" cols="30" rows="7"
                                            class="form-control" value="{{ old('answer') }}"></textarea>
                                    </div>
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Create</button>
                                    <a class="btn btn-info" type="button"   href="{{ route('admin.faqs.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
