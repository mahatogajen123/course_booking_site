@extends('backend.system.layouts.master')
@section('content')
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Partner</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">Edit Partner</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.partner.update', $partner->id) }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="title">Name</label>
                                        <input type="text" name="name" class="form-control" id="name"
                                            placeholder="Enter title" value="{{ $partner->name }}" required>
                                        @if ($errors->has('name'))
                                            <p style="color: red">
                                                {{ $errors->first('name') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="designation">Url</label>
                                        <input type="url" name="url" class="form-control" id="url"
                                            placeholder="Enter description" value="{{ $partner->url }}" required>
                                        @if ($errors->has('url'))
                                            <p style="color: red">
                                                {{ $errors->first('url') }}
                                            </p>
                                        @endif
                                    </div>
                                    {{-- <div class="form-group col-6">
                                        <label for="designation">Image</label>
                                        <input type="file" name="image" class="form-control" id="image"
                                            placeholder="Enter description" value="{{ $partner->image }}" >
                                        @if ($errors->has('image'))
                                            <p style="color: red">
                                                {{ $errors->first('image') }}
                                            </p>
                                        @endif
                                    </div> --}}
                                    <div class="col col-6">
                                        <div class="row">
                                            <div class="form-group col-10">
                                                <label for="image">Image</label>
                                                <input type="file" name="image" class="form-control"
                                                    onchange="serviceImgPreview(this);" placeholder="image">
                                                @if ($errors->has('image'))
                                                    <p style="color: red">
                                                        {{ $errors->first('image') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-2">
                                                <label for="">Image</label>
                                                <img id="previewImg"
                                                    src="{{ asset('uploads/partner/images/' . $partner->image) }}"
                                                    alt="icon"
                                                    style="height:40px; width:40px; border-radius:50%;object-fit:cover;">
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group col-6">
                                        <label for="designation">Logo</label>
                                        <input type="file" name="logo" class="form-control" id="logo"
                                            placeholder="Enter description" value="{{ $partner->logo }}" >
                                        @if ($errors->has('logo'))
                                            <p style="color: red">
                                                {{ $errors->first('logo') }}
                                            </p>
                                        @endif
                                    </div> --}}
                                    <div class="col col-6">
                                        <div class="row">
                                            <div class="form-group col-10">
                                                <label for="icon">Logo</label>
                                                <input type="file" name="logo" class="form-control"
                                                    onchange="serviceLogoPreview(this);">
                                                @if ($errors->has('logo'))
                                                    <p style="color: red">
                                                        {{ $errors->first('logo') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-2">
                                                <label for=""> Logo</label>
                                                <img id="previewicon"
                                                    src="{{ asset('uploads/partner/logo/' . $partner->logo) }}" alt="icon"
                                                    style="height:40px; width:40px; border-radius:50%;object-fit:cover;">
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Update</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.partner.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
    <script>
        function serviceImgPreview(input) {
            var file = $("input[type=file]").get(0).files[0];
            console.log(file);
            if (file) {
                var reader = new FileReader();

                reader.onload = function() {
                    $("#previewImg").attr("src", reader.result);
                }

                reader.readAsDataURL(file);
            }
        }

        function serviceLogoPreview(input) {
            var file = $("input[type=file]").get(1).files[0];
            console.log(file);
            if (file) {
                var reader = new FileReader();

                reader.onload = function() {
                    $("#previewicon").attr("src", reader.result);
                }

                reader.readAsDataURL(file);
            }
        }
    </script>
@endsection
