@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Service</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">Edit Service</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.service.update', $service->id) }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-6 ">
                                        <label for="role">Category </label>
                                        <select class="form-control select2" name="category_id" id="category_id">
                                            @foreach ($servicecategory as $type)
                                                <option
                                                    {{ $service->service_category_id == $type->id ? 'selected' : ' ' }}
                                                    value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <p style="color: red">
                                                {{ $errors->first('category_id') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title"
                                            placeholder="Enter title" value="{{ $service->title }}" required>
                                        @if ($errors->has('title'))
                                            <p style="color: red">
                                                {{ $errors->first('title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Sub Title</label>
                                        <input type="text" name="sub_title" class="form-control" id="sub_title"
                                            placeholder="Enter sub title" value="{{ $service->sub_title }}" required>
                                        @if ($errors->has('sub_title'))
                                            <p style="color: red">
                                                {{ $errors->first('sub_title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="col col-6">
                                        <div class="row">
                                            <div class="form-group col-10">
                                                <label for="image">Image</label>
                                                <input type="file" name="image" class="form-control"
                                                    onchange="serviceImgPreview(this);" placeholder="image">
                                                @if ($errors->has('image'))
                                                    <p style="color: red">
                                                        {{ $errors->first('image') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-2">
                                                <label for="">Image</label>
                                                <img id="previewImg"
                                                    src="{{ asset('uploads/service/images/' . $service->image) }}"
                                                    alt="icon"
                                                    style="height:40px; width:40px; border-radius:50%;object-fit:cover;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-6">
                                        <div class="row">
                                            <div class="form-group col-10">
                                                <label for="icon">Icon</label>
                                                <input type="file" name="icon" class="form-control"
                                                    onchange="serviceIconPreview(this);" placeholder="Icon">
                                                @if ($errors->has('icon'))
                                                    <p style="color: red">
                                                        {{ $errors->first('icon') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-2">
                                                <label for=""> Icon</label>
                                                <img id="previewicon"
                                                    src="{{ asset('uploads/service/icon/' . $service->icon) }}"
                                                    alt="icon"
                                                    style="height:40px; width:40px; border-radius:50%;object-fit:cover;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="password">Short Detail</label>
                                        <input type="text" name="short_detail" class="form-control" id=""
                                            placeholder="Short detail here" value="{{ $service->short_detail }}">
                                        @if ($errors->has('short_detail'))
                                            <p style="color: red">
                                                {{ $errors->first('short_detail') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="designation">Description</label>
                                        <textarea name="description" id="summernote" cols="20" rows="8"
                                            value={{ old('description') }}>{{ $service->description }}</textarea>
                                        {{-- <input type="text" name="description" class="form-control" id=""
                                            placeholder="Enter description" value="{{ $service->description }}"> --}}
                                        @if ($errors->has('description'))
                                            <p style="color: red">
                                                {{ $errors->first('description') }}
                                            </p>
                                        @endif
                                    </div>

                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Update</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.course.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
    <script>
        function serviceImgPreview(input) {
            var file = $("input[type=file]").get(0).files[0];
            console.log(file);
            if (file) {
                var reader = new FileReader();

                reader.onload = function() {
                    $("#previewImg").attr("src", reader.result);
                }

                reader.readAsDataURL(file);
            }
        }

        function serviceIconPreview(input) {
            var file = $("input[type=file]").get(1).files[0];
            console.log(file);
            if (file) {
                var reader = new FileReader();

                reader.onload = function() {
                    $("#previewicon").attr("src", reader.result);
                }

                reader.readAsDataURL(file);
            }
        }
    </script>
@endsection
