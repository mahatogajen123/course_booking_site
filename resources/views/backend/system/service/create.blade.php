@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New Service</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">New Service</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data" action="{{ route('admin.service.store') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-6 ">
                                        <label for="service_category_id">Service Category </label>
                                        <select class="form-control select2" name="service_category_id"
                                            id="service_category_id">
                                            @foreach ($category as $type)
                                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('service_category_id'))
                                            <p style="color: red">
                                                {{ $errors->first('service_category_id') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title"
                                            placeholder="Enter title" value="{{ old('title') }}">
                                        @if ($errors->has('title'))
                                            <p style="color: red">
                                                {{ $errors->first('title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Sub Title</label>
                                        <input type="text" name="sub_title" class="form-control" id="sub_title"
                                            placeholder="Enter sub title" value="{{ old('sub_title') }}">
                                        @if ($errors->has('sub_title'))
                                            <p style="color: red">
                                                {{ $errors->first('sub_title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="image">Image</label>
                                        <input type="file" name="image" class="form-control" id="image"
                                            placeholder="image">
                                        @if ($errors->has('image'))
                                            <p style="color: red">
                                                {{ $errors->first('image') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="image">Icon</label>
                                        <input type="file" name="icon" class="form-control" id="icon" placeholder="icon">
                                        @if ($errors->has('icon'))
                                            <p style="color: red">
                                                {{ $errors->first('icon') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="password">Short Detail</label>
                                        <input type="text" name="short_detail" class="form-control" id="short_detail"
                                            placeholder="Short detail here" value="{{ old('short_detail') }}">
                                        @if ($errors->has('short_detail'))
                                            <p style="color: red">
                                                {{ $errors->first('short_detail') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="designation">Description</label>
                                        <textarea name="description" id="summernote" cols="20" rows="8"></textarea>
                                        @if ($errors->has('description'))
                                            <p style="color: red">
                                                {{ $errors->first('description') }}
                                            </p>
                                        @endif
                                    </div>

                                    <!-- Drag and drop file upload -->
                                    {{-- <div class="form-group col-3">
                                        <div class="file-drop-area">
                                            <div class="file-drop-icon ci-cloud-upload"></div>
                                            <span class="file-drop-message">Drag and drop here to upload</span>
                                            <input type="file" class="file-drop-input" name="image">
                                            <button type="button" class="file-drop-btn btn btn-info btn-sm">Or select
                                                file</button>
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Create</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.service.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
