@extends('backend.system.layouts.master')
@section('content')

    <!-- Content Wrapper. Contains page content -->

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('role.new_role') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">{{ __('setting.dashboard') }}</a>
                        </li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.roles.index') }}">{{ __('setting.role_permission') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('role.new_role') }}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('admin.roles.store') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="role">{{ __('role.role_name') }}</label>
                                    <input type="text" name="name" class="form-control" id="role"
                                        placeholder="{{ __('role.role_name') }}" required>
                                    @if ($errors->has('name'))
                                        <p style="color: red">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="role">{{ __('role.role_permission') }}</label>
                                    <table class="table table-hover">
                                        <tr>
                                            <th>{{ __('role.module') }}</th>
                                            <th class="text-center">{{ __('role.view') }}</th>
                                            <th class="text-center">{{ __('role.create') }}</th>
                                            <th class="text-center">{{ __('role.update') }}</th>
                                            <th class="text-center">{{ __('role.delete') }}</th>
                                        </tr>
                                        @foreach ($modules as $key => $item)
                                            <tr>
                                                <td>{{ $item->name }}</td>
                                                @foreach ($item->permissions as $index => $permission)
                                                    <td class="text-center">
                                                        <div class="form-check">
                                                            {{-- onclick="checkView({{$permission->name}})" --}}
                                                            <input class="form-check-input" type="checkbox"
                                                                name="permissions[]" value="{{ $permission->id }}"
                                                                id="pview-role" style="cursor: pointer"><label
                                                                class='font-weight-normal' for="pview-role"></label>
                                                        </div>
                                                    </td>
                                                @endforeach

                                            </tr>
                                        @endforeach


                                    </table>
                                </div>
                                <button type="submit" class="btn btn-info">{{ __('role.create') }}</button>
                                <a href="{{ route('admin.roles.index') }}"
                                    class="btn btn-info">{{ __('role.cancel') }}</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </section>

    <!-- /.content-wrapper -->
@endsection
{{-- <script>
    function checkView(name){
        console.log(name)
        var str = name;
        var res = str.split('-');
        if(res[0]=='create' || res[0]=='update' || res[0]=='delete'){
            var checkId='';
           $.each(res,function(key,value){
               if(key !=0){
                   checkId +='-' + value
               }
           });
            $("#pview"+checkId).prop("checked", true);
            // console.log($("#pview"+checkId).is(':checked'));
            // console.log();
            if($("#pcreate"+checkId).is(':checked') || $("#pupdate"+checkId).is(':checked') || $("#pdelete"+checkId).is(':checked')) {
                // $("#pview"+checkId).prop("disabled", true);
                $("#pview"+checkId).attr("onclick","return false;");

            }else {
                // $("#pview"+checkId).prop("disabled", false);
                $("#pview"+checkId).attr("onclick","return true;");
                // $("#pview"+checkId).prop("checked", false);
            }
        }
        // if()
    }
    // $("#createRoleForm").submit(function() {
    //     $("input").removeAttr("disabled");
    // });
</script> --}}
