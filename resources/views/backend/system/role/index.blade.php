@extends('backend.system.layouts.master')
@section('content')
    @include('flash-message')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('setting.role_permission') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">{{ __('setting.dashboard') }}</a>
                        </li>
                        <li class="breadcrumb-item active">{{ __('setting.role_permission') }}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class='row'>
                                <div class="col-6 d-inline-flex">

                                    <a class="d-flex align-item-center mt-1 btn-outline-info rounded-pill btn-icon"
                                        href="{{ route('admin.roles.index') }}" title="Refresh">
                                        <i class="fas fa-redo fa-1x"></i>
                                    </a>
                                    <form action="#" class='d-flex align-items-center search' style="margin-left: 20px;"
                                        method="GET">
                                        <div>
                                            <input type="text" value="" placeholder="Search..." class="form-control"
                                                name="search" required />
                                        </div>
                                        <button type="submit"
                                            class="btn btn-info btn-sm btn-shadow me-1">{{ __('setting.search') }}</button>
                                    </form>
                                </div>
                                <div class="col-6">

                                    <a href="{{ route('admin.roles.create') }}" class="btn btn-info btn-sm float-right">
                                        <i data-feather="plus"></i>
                                        {{ __('role.new_role') }}</a>
                                </div>

                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">{{ __('setting.name') }}</th>
                                        <th scope="col">{{ __('setting.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($roles as $key => $role)
                                        <tr>
                                            {{-- <td>{{ ++$key }}</td> --}}
                                            <td class="align-middle">{{ $role->name }}</td>
                                            <td class="align-middle">
                                                <a href="{{ route('admin.roles.edit', $role->id) }}"
                                                    class="btn btn-outline-accent btn-icon">
                                                    <i class="ci-edit"></i>
                                                </a>
                                                &nbsp;
                                                <a href="{{ route('admin.roles.delete', $role->id) }}"
                                                    onclick="return confirm('Are you sure ?')"
                                                    class="btn btn-outline-danger btn-icon">
                                                    <i class="ci-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- /.content-wrapper -->
@endsection
