@extends('backend.system.layouts.master')
@section('content')
    <!-- Content Wrapper. Contains page content -->

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('role.update_role') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">{{ __('setting.dashboard') }}</a>
                        </li>
                        <li class="breadcrumb-item"><a
                                href="{{ route('admin.roles.index') }}">{{ __('setting.role_permission') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('role.update_role') }}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('admin.roles.update', $role->id) }}">
                                @csrf
                                <input type="hidden" name="_method" value="PUT">
                                <div class="form-group">
                                    <label for="role">{{ __('role.role_name') }}</label>
                                    <input type="text" name="name" class="form-control" id="role"
                                        value="{{ $role->name }}" placeholder="Role Name" required>
                                    @if ($errors->has('name'))
                                        <p style="color: red">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>{{ __('role.module') }}</th>
                                            <th class="text-center">{{ __('role.view') }}</th>
                                            <th class="text-center">{{ __('role.create') }}</th>
                                            <th class="text-center">{{ __('role.update') }}</th>
                                            <th class="text-center">{{ __('role.delete') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($modules as $key => $item)
                                            <tr>
                                                <td>{{ $item->name }}</td>
                                                @foreach ($item->permissions as $index => $permission)
                                                    <td>
                                                        <div class="form-check text-center">
                                                            {{-- onclick="checkView({{$permission->name}})" --}}
                                                            <input class="form-check-input" type="checkbox"
                                                                name="permissions[]" value="{{ $permission->id }}"
                                                                id="pview-role" style="cursor: pointer" @if (in_array($permission->id, $rolePermissions))
                                                            checked
                                                @endif><label class='font-weight-normal'
                                                    for="pview-role"></label>
                        </div>
                        </td>
                        @endforeach

                        </tr>
                        @endforeach

                        </tbody>
                        </table>
                        <button type="submit" class="btn btn-info btn-shadow">{{ __('role.update') }}</button>
                        <a href="{{ route('admin.roles.index') }}" class="btn btn-info btn-shadow">{{ __('role.cancel') }}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

    <!-- /.content-wrapper -->
@endsection
