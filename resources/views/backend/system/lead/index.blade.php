@extends('backend.system.layouts.master')
@section('content')
    <section class="content-header">
        <div class="container">
            <div style="z-index: 200; position:absolute; top:90px; right:30px;">
                @include('flash-message')
            </div>
            <div class="row mb-2">
                <div class="col-6">
                    <h1 class="m-0 text-dark">
                        {!! $page_title ?? 'Page Title' !!}
                        <small style="color: grey; font-size: xx-small">{!! $page_description ?? 'Page description' !!}</small>
                    </h1>
                </div><!-- /.col -->
                <div class="col-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">{{ __('vehicle_type.dashboard') }}</a>
                        </li>
                        <li class="breadcrumb-item active">{!! $page_title ?? 'Page Title' !!}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>

    </section>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card border-info">
                        <div class="card-header border-info">
                            <div class='row'>
                                <div class="col-sm-12 col-md-6 mb-1 d-inline-flex">

                                    <a class="d-flex align-item-center mt-1 btn-outline-info rounded-pill btn-icon"
                                        href="{{ route('admin.course.index') }}" title="Refresh">
                                        <em class="fas fa-redo fa-1x"></em>
                                    </a>
                                    <form action="#" class='d-flex align-items-center search' style="margin-left: 20px;"
                                        method="GET">
                                        <div>
                                            <input type="text" value="" placeholder="Search" class="form-control"
                                                name="search" required />
                                        </div>
                                        <button class="btn btn-info btn-sm btn-shadow me-1"
                                            type="submit">{{ __('vehicle_type.search') }}</button>
                                    </form>
                                </div>
                                <div class="col-sm-12 col-md-6">

                                    <a href="{{ route('admin.lead.create') }}" class="btn btn-info btn-sm float-right">
                                        <em data-feather="plus"></em>New Lead</a>
                                </div>

                            </div>
                        </div>
                        <div class="card-body table-responsive">
                            <table id="example2" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">Sno</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Gender</th>
                                        <th scope="col">Course</th>
                                        <th scope="col">Personal Contact</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $k => $v)
                                        <tr>
                                            <td class="align-middle">{{ ++$k }}</td>
                                            <td class="align-middle">{{ $v->first_name . '  ' }}{{ $v->last_name }}
                                            </td>
                                            <td class="align-middle">{{ $v->email }}</td>
                                            <td class="align-middle">{{ $v->gender }}</td>
                                            <td class="align-middle">{{ $v->course->title ?? ' ' }}</td>
                                            <td class="align-middle">{{ $v->personal_contact }}</td>
                                            <td> <a href="#" data-toggle="modal" data-target="#view{{ $v->id }}"
                                                    class="btn btn-outline-success btn-icon btn-sm">
                                                    <em class="fas fa-eye fa-xs"></em>
                                                </a>
                                                &nbsp;
                                                <a href="{{ route('admin.lead.edit', $v->id) }}"
                                                    class="btn btn-outline-accent btn-icon ">
                                                    <i class="ci-edit "></i>
                                                </a>
                                                &nbsp;
                                                <a href="{{ route('admin.lead.delete', $v->id) }}"
                                                    class="btn btn-outline-danger btn-icon "
                                                    onclick="return confirm('Are you sure you want to delete this item?')">
                                                    <i class="ci-trash"></i>
                                                </a>
                                            </td>

                                        </tr>
                                        @include('modal.lead.lead-detail')
                                    @endforeach
                                </tbody>
                            </table>
                            <nav class="d-flex justify-content-around pt-3" aria-label="Page navigation">
                                {{ $data->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.row -->
    <script>
        $(function() {
            $('.form-check-input').change(function() {
                var status = $(this).prop('checked') == true ? 1 : 0;
                var course_id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: '/course-status',
                    data: {
                        'status': status,
                        'course_id': course_id
                    },
                    // success: function(data) {
                    //     console.log(data.success)

                    // }
                })
            })
        });
    </script>

@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
    <!-- DataTables -->
@endsection

@section('cdn')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.css"
        integrity="sha512-9tISBnhZjiw7MV4a1gbemtB9tmPcoJ7ahj8QWIc0daBCdvlKjEA48oLlo6zALYm3037tPYYulT0YQyJIJJoyMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"
        integrity="sha512-F636MAkMAhtTplahL9F6KmTfxTmYcAcjcCkyu0f0voT3N/6vzAuJ4Num55a0gEJ+hRLHhdz3vDvZpf6kqgEa5w=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection
