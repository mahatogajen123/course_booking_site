@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Lead</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">Edit lead</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.lead.update', $lead->id) }}">
                                @csrf
                                <div class="row">
                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label for="title">First Name</label>
                                            <input type="text" name="f_name" class="form-control" id="f_name"
                                                placeholder="Ex: Joe " value="{{ $lead->first_name }}">
                                            @if ($errors->has('f_name'))
                                                <p style="color: red">
                                                    {{ $errors->first('f_name') }}
                                                </p>
                                            @endif
                                        </div>

                                        <div class="form-group col-6">
                                            <label for="title">Last Name</label>
                                            <input type="text" name="l_name" class="form-control" id="l_name"
                                                placeholder="Ex: Marlo" value="{{ $lead->last_name }}">
                                            @if ($errors->has('l_name'))
                                                <p style="color: red">
                                                    {{ $errors->first('l_name') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label for="designation"> Home Contact </label>
                                            <input type="tel" name="home_contact" class="form-control" id="home_contact"
                                                placeholder="Ex: 01-4217444" value="{{ $lead->home_contact }}" required>
                                            @if ($errors->has('home_contact'))
                                                <p style="color: red">
                                                    {{ $errors->first('home_contact') }}
                                                </p>
                                            @endif
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="designation">Personal Contact</label>
                                            <input type="tel" name="personal_contact" class="form-control"
                                                id="personal_contact" placeholder="Ex: 98********"
                                                value="{{ $lead->personal_contact }}" required>
                                            @if ($errors->has('personal_contact'))
                                                <p style="color: red">
                                                    {{ $errors->first('personal_contact') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="title">Student Email</label>
                                            <input type="email" name="email" class="form-control" id="email"
                                                placeholder="Ex: Example@mail.com" value="{{ $lead->email }}" required>
                                            @if ($errors->has('email'))
                                                <p style="color: red">
                                                    {{ $errors->first('email') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label for="permanent_address">Permanent Address</label>
                                            <input type="text" name="permanent_address" class="form-control"
                                                id="permanent_address" placeholder="Enter Permanent Address"
                                                value="{{ $lead->permanent_address }}" required>
                                            @if ($errors->has('permanent_address'))
                                                <p style="color: red">
                                                    {{ $errors->first('permanent_address') }}
                                                </p>
                                            @endif
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="current_address">Current Address</label>
                                            <input type="text" name="current_address" class="form-control"
                                                id="current_address" placeholder="Enter Current Address"
                                                value="{{ $lead->current_address }}" required>
                                            @if ($errors->has('current_address'))
                                                <p style="color: red">
                                                    {{ $errors->first('current_address') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-4">
                                            <label for="dob">Date Of Birth</label>
                                            <input type="date" name="dob" class="form-control" id="dob"
                                                placeholder="Enter date of birth" value="{{ $lead->dob }}" required>
                                            @if ($errors->has('dob'))
                                                <p style="color: red">
                                                    {{ $errors->first('dob') }}
                                                </p>
                                            @endif
                                        </div>
                                        <div class="form-group col-4">
                                            <label for="dob">Gender</label>
                                            @php
                                                if (old('gender') !== null) {
                                                    $option = old('gender');
                                                } else {
                                                    $option = $lead->gender;
                                                }
                                            @endphp
                                            <select name="gender" id="gender" class="form-control py-0">
                                                <option disabled>Select Gender</option>
                                                <option value="Male" {{ $option == 'Male' ? 'selected' : '' }}>Male
                                                </option>
                                                <option value="Female" {{ $option == 'Female' ? 'selected' : '' }}>Female
                                                </option>
                                                <option value="Other" {{ $option == 'Other' ? 'selected' : '' }}>Others
                                                </option>
                                            </select>
                                            @if ($errors->has('dob'))
                                                <p style="color: red">
                                                    {{ $errors->first('dob') }}
                                                </p>
                                            @endif
                                        </div>
                                        <div class="form-group col-4">
                                            <label for="dob">Choose Course</label>
                                            <select name="course_id" id="course" class="form-control py-0">
                                                <option disabled>Select Course</option>
                                                @foreach ($course as $type)
                                                    <option {{ $lead->Course->id == $type->id ? 'selected' : '' }}
                                                        value="{{ $type->id }}">
                                                        {{ $type->title }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('dob'))
                                                <p style="color: red">
                                                    {{ $errors->first('dob') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                        <button class="btn btn-info me-md-2" type="submit">Create</button>
                                        <a class="btn btn-info" type="button"
                                            href="{{ route('admin.lead.index') }}">Cancel</a>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
