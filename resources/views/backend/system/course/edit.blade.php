@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Course</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">Edit Course</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.course.update', $course->id) }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-3 ">
                                        <label for="role">Category </label>
                                        <select class="form-control select2" name="category_id" id="category_id">
                                            @foreach ($coursecategory as $type)
                                                <option {{ $course->category_id == $type->id ? 'selected' : ' ' }}
                                                    value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <p style="color: red">
                                                {{ $errors->first('category_id') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-3 ">
                                        <label for="role">Duration </label>
                                        <select class="form-control pt-1" name="duration" id="duration">
                                            <option value="{{ $course->duration }}" selected>{{ $course->duration }}
                                            </option>
                                            <option value="1 Month">1-Month</option>
                                            <option value="3 Month">3-Month</option>
                                            <option value="6 Month">6-Month</option>
                                            <option value="9 Month">9-Month</option>
                                            <option value="1 Year">1-Year</option>
                                        </select>
                                        @if ($errors->has('duration'))
                                            <p style="color: red">
                                                {{ $errors->first('duration') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title"
                                            placeholder="Enter title" value="{{ $course->title }}" required>
                                        @if ($errors->has('title'))
                                            <p style="color: red">
                                                {{ $errors->first('title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Meta Title</label>
                                        <input type="text" name="meta_title" class="form-control" id="meta_title"
                                            placeholder="Enter meta title" value="{{ $course->meta_title }}" required>
                                        @if ($errors->has('meta_title'))
                                            <p style="color: red">
                                                {{ $errors->first('meta_title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">OG Meta Title</label>
                                        <input type="text" name="og_meta_title" class="form-control" id="og_meta_title"
                                            placeholder="Enter Og meta title" value="{{ $course->og_meta_title }}"
                                            required>
                                        @if ($errors->has('og_meta_title'))
                                            <p style="color: red">
                                                {{ $errors->first('og_meta_title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Start Date</label>
                                        <input type="date" name="start_date" class="form-control" id="start_date"
                                            placeholder="Enter start_date" value="{{ $course->start_date }}" required>
                                        @if ($errors->has('start_date'))
                                            <p style="color: red">
                                                {{ $errors->first('start_date') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">End Date</label>
                                        <input type="date" name="end_date" class="form-control" id="end_date"
                                            placeholder="Enter end_date" value="{{ $course->end_date }}" required>
                                        @if ($errors->has('end_date'))
                                            <p style="color: red">
                                                {{ $errors->first('end_date') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Price</label>
                                        <input type="text" name="price" class="form-control" id="price"
                                            placeholder="Enter price" value="{{ $course->price }}" required>
                                        @if ($errors->has('price'))
                                            <p style="color: red">
                                                {{ $errors->first('price') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="password">Short Detail</label>
                                        <input type="text" name="short_detail" class="form-control" id="short_detail"
                                            placeholder="Short detail here" value="{{ $course->short_detail }}">
                                        @if ($errors->has('short_detail'))
                                            <p style="color: red">
                                                {{ $errors->first('short_detail') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="designation">Description</label>
                                        <input type="text" name="description" class="form-control" id="description"
                                            placeholder="Enter description" value="{{ $course->description }}" required>
                                        @if ($errors->has('description'))
                                            <p style="color: red">
                                                {{ $errors->first('description') }}
                                            </p>
                                        @endif
                                    </div>
                                   
                                    <div class="col col-6">
                                        <div class="row">
                                            <div class="form-group col-10">
                                                <label for="icon">Image</label>
                                                <input type="file" name="image" class="form-control"
                                                    onchange="previewFile(this);" placeholder="image">
                                                @if ($errors->has('icon'))
                                                    <p style="color: red">
                                                        {{ $errors->first('icon') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-2">
                                                <label for="">Image</label>
                                                <img src="{{ asset('uploads/course/images/' . $course->image) }}" alt="icon"
                                                    style="height:40px; width:40px; border-radius:50%;object-fit:cover;"
                                                    id="previewImg">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Update</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.course.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
