@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New Course</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">New Course</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data" action="{{ route('admin.course.store') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-3 ">
                                        <label for="role">Category </label>
                                        <select class="form-control select2" name="category_id" id="category_id">
                                            @foreach ($category as $type)
                                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <p style="color: red">
                                                {{ $errors->first('category_id') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-3 ">
                                        <label for="role">Duration </label>
                                        {{-- <input type="text" name="duration" id="duration" class="form-control"> --}}
                                        <select name="duration" id="duration" class="form-control pt-1">
                                            <option value="" selected>Choose Course Duration</option>
                                            <option value="1-Month">1-Month</option>
                                            <option value="3-Month" selected>3-Month</option>
                                            <option value="6-Month">6-Month</option>
                                            <option value="9-Month">9-Month</option>
                                            <option value="1-Year">1-Year</option>
                                        </select>
                                        @if ($errors->has('duration'))
                                            <p style="color: red">
                                                {{ $errors->first('duration') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title"
                                            placeholder="Enter title" value="{{ old('title') }}">
                                        @if ($errors->has('title'))
                                            <p style="color: red">
                                                {{ $errors->first('title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Meta Title</label>
                                        <input type="text" name="meta_title" class="form-control" id="meta_title"
                                            placeholder="Enter meta title" value="{{ old('meta_title') }}">
                                        @if ($errors->has('meta_title'))
                                            <p style="color: red">
                                                {{ $errors->first('meta_title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">OG Meta Title</label>
                                        <input type="text" name="og_meta_title" class="form-control" id="og_meta_title"
                                            placeholder="Enter Og meta title" value="{{ old('og_meta_title') }}">
                                        @if ($errors->has('og_meta_title'))
                                            <p style="color: red">
                                                {{ $errors->first('og_meta_title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="start_Date">Start Date</label>
                                        <input type="date" name="start_date" class="form-control" id="start_date"
                                            placeholder="Enter start_date" value="{{ old('start_date') }}" min="2022-01-02">
                                        @if ($errors->has('start_date'))
                                            <p style="color: red">
                                                {{ $errors->first('start_date') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="end_date">End Date</label>
                                        <input type="date" name="end_date" class="form-control" id="end_date"
                                            placeholder="Enter end_date" value="{{ old('end_date') }}" min="2022-01-02">
                                        @if ($errors->has('end_date'))
                                            <p style="color: red">
                                                {{ $errors->first('end_date') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="price">Price</label>
                                        <input type="text" name="price" class="form-control" id="price"
                                            placeholder="Enter price" value="{{ old('price') }}">
                                        @if ($errors->has('price'))
                                            <p style="color: red">
                                                {{ $errors->first('price') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="password">Short Detail</label>
                                        <input type="text" name="short_detail" class="form-control" id="short_detail"
                                            placeholder="Short detail here" value="{{ old('short_detail') }}">
                                        @if ($errors->has('short_detail'))
                                            <p style="color: red">
                                                {{ $errors->first('short_detail') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="designation">Description</label>
                                        <input type="text" name="description" class="form-control" id="description"
                                            placeholder="Enter description" value="{{ old('description') }}">
                                        @if ($errors->has('description'))
                                            <p style="color: red">
                                                {{ $errors->first('description') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="image">Image</label>
                                        <input type="file" name="image" class="form-control" id="image"
                                            placeholder="image">
                                        @if ($errors->has('image'))
                                            <p style="color: red">
                                                {{ $errors->first('image') }}
                                            </p>
                                        @endif
                                    </div>
                                    <!-- Drag and drop file upload -->
                                    {{-- <div class="form-group col-3">
                                        <div class="file-drop-area">
                                            <div class="file-drop-icon ci-cloud-upload"></div>
                                            <span class="file-drop-message">Drag and drop here to upload</span>
                                            <input type="file" class="file-drop-input" name="image">
                                            <button type="button" class="file-drop-btn btn btn-info btn-sm">Or select
                                                file</button>
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Create</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.course.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
