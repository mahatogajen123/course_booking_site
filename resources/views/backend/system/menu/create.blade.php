@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New Menu</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">New Menu</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data" action="{{ route('admin.menu.store') }}">
                                @csrf
                                <div class="row">
                                    <div class="row">
                                        <div class="form-group col-6 ">
                                            <label for="role">Parent Id </label>
                                            <select class="form-control select2" name="parent_id" id="category_id">
                                                <option value="">Parent</option>
                                                @foreach ($parent as $type)
                                                    <option value="{{ $type->id }}">{{ $type->title }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('category_id'))
                                                <p style="color: red">
                                                    {{ $errors->first('category_id') }}
                                                </p>
                                            @endif
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="title">Title</label>
                                            <input type="text" name="title" class="form-control" id="title"
                                                placeholder="Enter title" value="{{ old('title') }}">
                                            @if ($errors->has('title'))
                                                <p style="color: red">
                                                    {{ $errors->first('title') }}
                                                </p>
                                            @endif
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="url">URL</label>
                                            <input type="url" name="url" class="form-control" id="url"
                                                placeholder="Enter url" value="{{ old('url') }}">
                                            @if ($errors->has('url'))
                                                <p style="color: red">
                                                    {{ $errors->first('url') }}
                                                </p>
                                            @endif
                                        </div>

                                        <div class="form-group col-6">
                                            <label for="icon">Icon</label>
                                            <input type="file" name="icon" class="form-control" id="icon"
                                                placeholder="image" value="{{ old('icon') }}">
                                            @if ($errors->has('icon'))
                                                <p style="color: red">
                                                    {{ $errors->first('icon') }}
                                                </p>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                        <button class="btn btn-info me-md-2" type="submit">Create</button>
                                        <a class="btn btn-info" type="button"
                                            href="{{ route('admin.menu.index') }}">Button</a>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
