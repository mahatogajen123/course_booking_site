@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Menu</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">Edit Menu</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.menu.update', $menu->id) }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-6 ">
                                        <label for="role">Parent Id </label>
                                        <select class="form-control select2" name="parent_id" id="category_id">
                                            <option value="">Parent</option>
                                            @foreach ($parent as $type)
                                                <option @if (isset($parent_info->id))
                                                    {{ $type->id == $parent_info->id ? 'selected' : '' }}
                                            @endif
                                            value="{{ $type->id }}">
                                            {{ $type->title }}
                                            </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <p style="color: red">
                                                {{ $errors->first('category_id') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title"
                                            placeholder="Enter title" value="{{ $menu->title }}" required>
                                        @if ($errors->has('title'))
                                            <p style="color: red">
                                                {{ $errors->first('title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">URL</label>
                                        <input type="url" name="url" class="form-control" id="url" placeholder="Enter url"
                                            value="{{ $menu->url }}" required>
                                        @if ($errors->has('url'))
                                            <p style="color: red">
                                                {{ $errors->first('url') }}
                                            </p>
                                        @endif
                                    </div>

                                    <div class="col col-6">
                                        <div class="row">
                                            <div class="form-group col-10">
                                                <label for="icon">Icon</label>
                                                <input type="file" name="icon" class="form-control"
                                                    onchange="previewFile(this);" placeholder="image">
                                                @if ($errors->has('icon'))
                                                    <p style="color: red">
                                                        {{ $errors->first('icon') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-2">
                                                <label for="">Image</label>
                                                <img src="{{ asset('uploads/menu/icon/' . $menu->icon) }}" alt="icon"
                                                    style="height:40px; width:40px; border-radius:50%;object-fit:cover;"
                                                    id="previewImg">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Update</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.menu.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
