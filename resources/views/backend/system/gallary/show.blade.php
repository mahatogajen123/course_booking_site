@extends('backend.system.layouts.master')
@section('content')


    <section class="content-header">
        <div class="container">
            <div class="col-6 d-flex justify-content-end" style="z-index: 200; position:absolute">
                @include('flash-message')
            </div>
            <div class="row mb-2">
                <div class="col-6">
                    <h1 class="m-0 text-dark">
                        {!! $page_title ?? 'Page Title' !!}
                        <small style="color: grey; font-size: xx-small">{!! $page_description ?? 'Page description' !!}</small>
                    </h1>
                </div><!-- /.col -->
                <div class="col-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">{{ __('vehicle_type.dashboard') }}</a>
                        </li>
                        <li class="breadcrumb-item active">{!! $page_title ?? 'Page Title' !!}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>

    </section>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card border-info">
                        <div class="card-header border-info">
                            <div class='row'>
                                <div class="col-sm-12 col-md-6 mb-1 d-inline-flex">

                                    <form action="#" class='d-flex align-items-center search' style="margin-left: 20px;"
                                        method="GET">
                                        <div>
                                            <input type="text" value="" placeholder="Search" class="form-control"
                                                name="search" required />
                                        </div>
                                        <button class="btn btn-info btn-sm btn-shadow me-1"
                                            type="submit">{{ __('vehicle_type.search') }}</button>
                                    </form>
                                </div>
                                <div class="col-sm-12 col-md-6">

                                    <a href="{{ route('admin.photo.create') }}" class="btn btn-info btn-sm float-right">
                                        <em data-feather="plus"></em>New Photos</a>
                                </div>

                            </div>
                        </div>
                        <h4><i class="glyphicon glyphicon-picture"></i> Display Data Image </h4>
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Picture</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $k => $items)
                                    <tr>
                                        <td>{{ ++$k }}</td>
                                        @foreach ($items->images as $v)
                                            <td>
                                                <img src="{{ URL::to($v) }}" alt="" width="60" height="60">
                                            </td>
                                        @endforeach
                                        <td>
                                            <a href="{{ route('admin.photo.delete', $items->id) }}"
                                                class="btn btn-outline-danger btn-icon"
                                                onclick="return confirm('Are you sure you want to delete this item?')">
                                                <i class="ci-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>

    </section>
    <!-- /.row -->


@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
    <!-- DataTables -->
@endsection
