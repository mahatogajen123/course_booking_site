@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New Career</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">New Career</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data" action="{{ route('admin.career.store') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title"
                                            placeholder="Enter title" value="{{ old('title') }}">
                                        @if ($errors->has('title'))
                                            <p style="color: red">
                                                {{ $errors->first('title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Sub Title</label>
                                        <input type="text" name="sub_title" class="form-control" id="sub_title"
                                            placeholder="Enter meta title" value="{{ old('sub_title') }}">
                                        @if ($errors->has('sub_title'))
                                            <p style="color: red">
                                                {{ $errors->first('sub_title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Job Type</label>
                                        <input type="text" name="job_type" class="form-control" id="job_type"
                                            placeholder="Enter job type" value="{{ old('job_type') }}">
                                        @if ($errors->has('job_type'))
                                            <p style="color: red">
                                                {{ $errors->first('job_type') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Job Level</label>
                                        <input type="text" name="job_level" class="form-control" id="job_level"
                                            placeholder="Enter job level" value="{{ old('job_level') }}">
                                        @if ($errors->has('job_level'))
                                            <p style="color: red">
                                                {{ $errors->first('job_level') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Company Name</label>
                                        <input type="text" name="company_name" class="form-control" id="company_name"
                                            placeholder="Enter Company name" value="{{ old('company_name') }}">
                                        @if ($errors->has('company_name'))
                                            <p style="color: red">
                                                {{ $errors->first('company_name') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Location</label>
                                        <input type="text" name="location" class="form-control" id="location"
                                            placeholder="Enter location" value="{{ old('location') }}">
                                        @if ($errors->has('location'))
                                            <p style="color: red">
                                                {{ $errors->first('location') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Offer Salary</label>
                                        <input type="text" name="offer_salary" class="form-control" id="offer_salary"
                                            placeholder="Enter offer salary" value="{{ old('offer_salary') }}">
                                        @if ($errors->has('offer_salary'))
                                            <p style="color: red">
                                                {{ $errors->first('offer_salary') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Job Created Date</label>
                                        <input type="date" name="job_create_date" class="form-control"
                                            id="job_created_date" placeholder="Enter job created date"
                                            value="{{ old('job_create_date') }}">
                                        @if ($errors->has('job_create_date'))
                                            <p style="color: red">
                                                {{ $errors->first('job_create_date') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Deadline</label>
                                        <input type="date" name="deadline" class="form-control" id="deadline"
                                            placeholder="Enter job deadline" value="{{ old('deadline') }}">
                                        @if ($errors->has('deadline'))
                                            <p style="color: red">
                                                {{ $errors->first('deadline') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="image">Image</label>
                                        <input type="file" name="image" class="form-control" id="image"
                                            placeholder="image">
                                        @if ($errors->has('image'))
                                            <p style="color: red">
                                                {{ $errors->first('image') }}
                                            </p>
                                        @endif
                                    </div>

                                    <div class="form-group col-6">
                                        <label for="password">Short Detail</label>
                                        {{-- <input type="text" name="short_detail" class="form-control" id="short_detail"
                                            placeholder="Short detail here"> --}}
                                        <textarea name="short_detail" id="" cols="30" rows="4"
                                            value="{{ old('short_detail') }}" class="form-control"></textarea>
                                        @if ($errors->has('short_detail'))
                                            <p style="color: red">
                                                {{ $errors->first('short_detail') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="designation">Detail</label>
                                        <textarea name="detail" id="" cols="30" rows="4" value="{{ old('detail') }}"
                                            class="form-control"></textarea>

                                        @if ($errors->has('detail'))
                                            <p style="color: red">
                                                {{ $errors->first('detail') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="designation">Respondibility</label>
                                        <textarea name="responsibility" id="summernote1" cols="30" rows="4"
                                            value="{{ old('responsibility') }}"></textarea>

                                        @if ($errors->has('responsibility'))
                                            <p style="color: red">
                                                {{ $errors->first('responsibility') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="designation">Requirement</label>
                                        <textarea name="requirement" id="summernote" cols="30" rows="4"
                                            value="{{ old('requirement') }}"></textarea>

                                        @if ($errors->has('requirement'))
                                            <p style="color: red">
                                                {{ $errors->first('requirement') }}
                                            </p>
                                        @endif
                                    </div>

                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Create</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.career.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
