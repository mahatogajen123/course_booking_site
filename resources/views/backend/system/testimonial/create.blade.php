@extends('backend.system.layouts.master')
@section('content')
    <style>
        .file-drop-area {
            position: relative;
            padding: 0.4rem 0rem;
            height: 245px;
            max-height: 250px;
        }

        .file-drop-area .file-drop-preview {
            max-width: 6rem;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New Testimonial</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">New Testimonial</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" enctype="multipart/form-data"
                                action="{{ route('admin.testimonial.store') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="company_name">Company Name</label>
                                        <input type="text" name="company_name" class="form-control" id="company_name"
                                            placeholder="Enter company_name" value="{{ old('company_name') }}">
                                        @if ($errors->has('company_name'))
                                            <p style="color: red">
                                                {{ $errors->first('company_name') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Person Name</label>
                                        <input type="text" name="person_name" class="form-control" id="person_name"
                                            placeholder="Enter meta title" value="{{ old('person_name') }}">
                                        @if ($errors->has('person_name'))
                                            <p style="color: red">
                                                {{ $errors->first('person_name') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="title">Title</label>
                                        <input type="test" name="title" class="form-control" id="title"
                                            placeholder="Enter title" value="{{ old('title') }}">
                                        @if ($errors->has('title'))
                                            <p style="color: red">
                                                {{ $errors->first('title') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="designation">Position</label>
                                        <input type="text" name="position" class="form-control" id="position"
                                            placeholder="Enter description" value="{{ old('position') }}">
                                        @if ($errors->has('position'))
                                            <p style="color: red">
                                                {{ $errors->first('position') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="designation">Image</label>
                                        <input type="file" name="image" class="form-control" id="image" placeholder=""
                                            required>
                                        @if ($errors->has('image'))
                                            <p style="color: red">
                                                {{ $errors->first('image') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="detail">Detail</label>
                                        <textarea name="detail" id="summernote" cols="15" rows="6"></textarea>
                                        @if ($errors->has('detail'))
                                            <p style="color: red">
                                                {{ $errors->first('detail') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button class="btn btn-info me-md-2" type="submit">Create</button>
                                    <a class="btn btn-info" type="button"
                                        href="{{ route('admin.slider.index') }}">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
@endsection
