@extends('backend.system.layouts.master')
@section('content')
    <section class="content-header">
        <div class="container">
            <div  style="z-index: 200; position:absolute; top:90px; right:30px;">
                @include('flash-message')
            </div>
            <div class="row mb-2">
                <div class="col-6">
                    <h1 class="m-0 text-dark">
                        {!! $page_title ?? 'Page Title' !!}
                        <small style="color: grey; font-size: xx-small">{!! $page_description ?? 'Page description' !!}</small>
                    </h1>
                </div><!-- /.col -->
                <div class="col-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">{{ __('vehicle_type.dashboard') }}</a>
                        </li>
                        <li class="breadcrumb-item active">{!! $page_title ?? 'Page Title' !!}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>

    </section>

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card border-info">
                        <div class="card-header border-info">
                            <div class='row'>
                                <div class="col-sm-12 col-md-6 mb-1 d-inline-flex">

                                    <a class="d-flex align-item-center mt-1 btn-outline-info rounded-pill btn-icon"
                                        href="{{ route('admin.career.index') }}" title="Refresh">
                                        <em class="fas fa-redo fa-1x"></em>
                                    </a>
                                    <form action="#" class='d-flex align-items-center search' style="margin-left: 20px;"
                                        method="GET">
                                        <div>
                                            <input type="text" value="" placeholder="Search" class="form-control"
                                                name="search" required />
                                        </div>
                                        <button class="btn btn-info btn-sm btn-shadow me-1"
                                            type="submit">{{ __('vehicle_type.search') }}</button>
                                    </form>
                                </div>
                                <div class="col-sm-12 col-md-6">

                                    <a href="{{ route('admin.career.create') }}" class="btn btn-info btn-sm float-right">
                                        <em data-feather="plus"></em>New Career</a>
                                </div>

                            </div>
                        </div>
                        <div class="card-body table-responsive">
                            <table id="example2" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">Sno</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Contact</th>
                                        <th scope="col">Portfolio_url</th>
                                        <th scope="col">File</th>
                                        <th scope="col">About</th>
                                        <th scope="col">Actions</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $k => $v)
                                        <tr>
                                            <td class="align-middle">{{ ++$k }}</td>
                                            <td class="align-middle">{{ $v->name }}</td>
                                            <td class="align-middle">{{ $v->email }}</td>
                                            <td class="align-middle">{{ $v->contact }}</td>
                                            <td class="align-middle">{{ $v->portfolio_url }}</td>

                                            <td class="align-middle">
                                                <a href="{{ asset('uploads/appliedCareer/resume/' . $v->resume) }}"
                                                    target="_blank">{{ $v->resume }}</a>
                                            </td>
                                            <td class="align-middle">{{ $v->about }}</td>

                                            <td>
                                                &nbsp;
                                                <a href="{{ route('admin.career.edit', $v->id) }}"
                                                    class="btn btn-outline-accent btn-icon ">
                                                    <i class="ci-edit "></i>
                                                </a>

                                                &nbsp;
                                                <a href="{{ route('admin.career.applied.delete', $v->id) }}"
                                                    class="btn btn-outline-danger btn-icon "
                                                    onclick="return confirm('Are you sure you want to delete this item?')">
                                                    <i class="ci-trash"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <div class="form-check form-switch ml-2 mt-2">
                                                    <input type="checkbox" class="form-check-input" id="customSwitch2"
                                                        data-id="{{ $v->id }}" {{ $v->status ? 'checked' : '' }}>
                                                </div>
                                            </td>
                                        </tr>
                                        @include('modal.career.career-detail')
                                    @endforeach
                                </tbody>
                            </table>
                            <nav class="d-flex justify-content-around pt-3" aria-label="Page navigation">
                                {{ $data->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.row -->

    <script>
        $(function() {
            $('.form-check-input').change(function() {
                var status = $(this).prop('checked') == true ? 1 : 0;
                var career_applied_id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: '/career-applied-status',
                    data: {
                        'status': status,
                        'career_applied_id': career_applied_id
                    },
                    // success: function(data) {
                    //     console.log(data.success)

                    // }
                })
            })
        });
    </script>

@endsection


<!-- Optional bottom section for modals etc... -->
@section('body_bottom')
    <!-- DataTables -->
@endsection

@section('cdn')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.css"
        integrity="sha512-9tISBnhZjiw7MV4a1gbemtB9tmPcoJ7ahj8QWIc0daBCdvlKjEA48oLlo6zALYm3037tPYYulT0YQyJIJJoyMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"
        integrity="sha512-F636MAkMAhtTplahL9F6KmTfxTmYcAcjcCkyu0f0voT3N/6vzAuJ4Num55a0gEJ+hRLHhdz3vDvZpf6kqgEa5w=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@endsection
