@extends('backend.system.layouts.master')
@section('content')

    <section class="content-header">

        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">
                        {{ __('setting.email_setting') }}
                        <small style="color: grey; font-size: xx-small">{{ __('setting.email_setting') }}</small>
                    </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active">Email setting</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>

    </section>

    <!-- /.content-header -->
    <section class="content">
        <div id="email-setting" class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body d-inline-grid">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <div class="container d-block">
                                            <a href="#" data-toggle="modal" data-target="#my-modal"
                                                class="btn btn-icon btn-outline-info float-right"><i
                                                    class="ci-mail me-2"></i>Send Test Email
                                            </a>

                                            <div class="modal" id="my-modal" tabindex="-1" role="dialog"
                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog  modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header" style="margin-left:0; margin-right:0">
                                                            <h5 class="modal-title" id="exampleModalLabel">Send Test Mail
                                                            </h5>
                                                            <button type="button" class="close"
                                                                data-dismiss="modal" aria-label="Close"><span
                                                                    aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form method="POST"
                                                                action="{{ route('admin.email-setting.sendMail') }}"
                                                                id="emailForm">
                                                                @csrf
                                                                <div class="mb-3">
                                                                    <input type="text" name="to_email"
                                                                        class="form-control" id="to_email"
                                                                        placeholder="To:" required>
                                                                </div>
                                                                <div class="mb-3">
                                                                    <textarea class="form-control" id="summernote"
                                                                        placeholder="Your message"
                                                                        style="height: 150px"></textarea>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" form="emailForm"
                                                                class="btn btn-info">Send</button>
                                                            <button type="button" class="btn btn-info"
                                                                onclick="$('#mail').modal('hide');"
                                                                aria-label="Close">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form method="POST" action="{{ route('admin.email-setting.update') }}">
                                @csrf
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="mailer">Mailer</label>
                                            <input type="text" name="mailer" class="form-control" id="mailer"
                                                placeholder="Mailer" value="{{ $emailSetting->mailer }}" required>
                                            @if ($errors->has('mailer'))
                                                <p style="color: red">
                                                    {{ $errors->first('mailer') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col">

                                        <div class="form-group">
                                            <label for="host">Host</label>
                                            <input type="text" name="host" class="form-control" id="host"
                                                placeholder="host" value="{{ $emailSetting->host }}" required>
                                            @if ($errors->has('host'))
                                                <p style="color: red">
                                                    {{ $errors->first('host') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">

                                        <div class="form-group">
                                            <label for="port">Port</label>
                                            <input type="number" name="port" class="form-control" id="port"
                                                placeholder="port" value="{{ $emailSetting->port }}" required>
                                            @if ($errors->has('port'))
                                                <p style="color: red">
                                                    {{ $errors->first('port') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col">

                                        <div class="form-group">
                                            <label for="user_name">Username</label>
                                            <input type="text" value="{{ $emailSetting->user_name }}" name="user_name"
                                                class="form-control" id="user_name" placeholder="user_name">
                                            @if ($errors->has('user_name'))
                                                <p style="color: red">
                                                    {{ $errors->first('user_name') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">

                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="text" value="{{ $emailSetting->password }}" name="password"
                                                class="form-control" id="password" placeholder="password" required>
                                            @if ($errors->has('password'))
                                                <p style="color: red">
                                                    {{ $errors->first('password') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="encryption">Encryption</label>
                                            <input type="text" value="{{ $emailSetting->encryption }}" name="encryption"
                                                class="form-control" id="encryption" placeholder="encryption" required>
                                            @if ($errors->has('encryption'))
                                                <p style="color: red">
                                                    {{ $errors->first('encryption') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="from_address">From Address</label>
                                            <input type="text" value="{{ $emailSetting->from_address }}"
                                                name="from_address" class="form-control" id="from_address"
                                                placeholder="from_address" required>
                                            @if ($errors->has('from_address'))
                                                <p style="color: red">
                                                    {{ $errors->first('from_address') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col">

                                        <div class="form-group">
                                            <label for="from_name">From Name</label>
                                            <input type="text" value="{{ $emailSetting->from_name }}" name="from_name"
                                                class="form-control" id="from_name" placeholder="from_name" required>
                                            @if ($errors->has('from_name'))
                                                <p style="color: red">
                                                    {{ $errors->first('from_name') }}
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-info">
                                    Update
                                </button>
                                <a href="{{ route('admin.email-setting.index') }}" class="btn btn-info">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content-wrapper -->
@endsection
@section('scripts')
    {{-- @include('backend.components.js.spinner-on-submit') --}}
@endsection
