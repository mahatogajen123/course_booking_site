@extends('backend.system.layouts.master')
@section('content')

    <style>
        .dropzone {
            background: #e3e6ff;
            border-radius: 13px;
            max-width: 550px;
            margin-left: auto;
            margin-right: auto;
            border: 2px dotted #1833FF;
            margin-top: 50px;
        }

    </style>
    {{-- @dd($category) --}}
    <div class="content-header">
        <div class="container">
            <div style="z-index: 200; position:absolute; top:90px; right:30px;">
                @include('flash-message')
            </div>
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">New Photos</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">
                                User </a></li>
                        <li class="breadcrumb-item active">New Photos</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>

    <!-- /.content-header -->
    <section class="container">
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div id="dropzone">
                                <form action="{{ route('admin.about.image.store') }}" class="dropzone"
                                    id="file-upload" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="dz-message">
                                        Drag and Drop Single/Multiple Files Here<br>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach ($images as $image)
                    <div class="col-md-2">
                        <div class="card">
                            <a href="{{ route('admin.about.image.delete', $image->id) }}"
                                class="btn btn-sm btn-outline-danger py-0" style="font-size: 0.8em;" id="deleteImage"
                                data-id="{{ $image->id }}">
                                <em class="fa fa-trash"></em>
                            </a>
                            <div class="card-body">
                                <img src="{{ URL::to($image->image) }}" alt=""
                                    style="max-height: 100px; min-height:100px;">
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            </div>
        </section>
    </section>
    <!-- /.content-wrapper -->
    <script>
        var dropzone = new Dropzone('#file-upload', {
            previewTemplate: document.querySelector('#preview-template').innerHTML,
            parallelUploads: 3,
            thumbnailHeight: 150,
            thumbnailWidth: 150,
            maxFilesize: 5,
            filesizeBase: 1500,
            thumbnail: function(file, dataUrl) {
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (var i = 0; i < images.length; i++) {
                        var thumbnailElement = images[i];
                        thumbnailElement.alt = file.name;
                        thumbnailElement.src = dataUrl;
                    }
                    setTimeout(function() {
                        file.previewElement.classList.add("dz-image-preview");
                    }, 1);
                }
            }
        });

        var minSteps = 6,
            maxSteps = 60,
            timeBetweenSteps = 100,
            bytesPerStep = 100000;

        dropzone.uploadFiles = function(files) {
            var self = this;

            for (var i = 0; i < files.length; i++) {

                var file = files[i];
                totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));

                for (var step = 0; step < totalSteps; step++) {
                    var duration = timeBetweenSteps * (step + 1);
                    setTimeout(function(file, totalSteps, step) {
                        return function() {
                            file.upload = {
                                progress: 100 * (step + 1) / totalSteps,
                                total: file.size,
                                bytesSent: (step + 1) * file.size / totalSteps
                            };

                            self.emit('uploadprogress', file, file.upload.progress, file.upload
                                .bytesSent);
                            if (file.upload.progress == 100) {
                                file.status = Dropzone.SUCCESS;
                                self.emit("success", file, 'success', null);
                                self.emit("complete", file);
                                self.processQueue();
                            }
                        };
                    }(file, totalSteps, step), duration);
                }
            }
        }

        //delete image script here 

        $(document).ready(function() {

            $("body").on("click", "#deleteImage", function(e) {

                if (!confirm("Do you really want to delete this?")) {
                    return false;
                }

                e.preventDefault();
                var id = $(this).data("id");
                // var id = $(this).attr('data-id');
                var token = $("meta[name='csrf-token']").attr("content");
                var url = e.target;

                $.ajax({
                    url: url.href, //or you can use url: "company/"+id,
                    type: 'DELETE',
                    data: {
                        _token: token,
                        id: id
                    },
                    success: function(response) {

                        $("#success").html(response.message)

                        Swal.fire(
                            'Remind!',
                            'Image deleted successfully!',
                            'success'
                        )
                    }
                });
                return false;
            });


        });
    </script>
@endsection
