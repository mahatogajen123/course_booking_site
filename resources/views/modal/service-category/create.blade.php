<div class="modal" data-backdrop="static" data-keyboard="false" id="view" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="margin-left:0; margin-right:0">
                <h5 class="modal-title" id="exampleModalLabel">Create Service Category</h5>
                <button class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close"
                    style="font-size: 20px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- <img src="{{ asset('uploads/course/images/' . $v->image) }}" alt="" srcset=""> --}}
                <!-- Horizontal card layout -->
                <form id="categoryform" method="POST" enctype="multipart/form-data"
                    action="{{ route('admin.service.category.store') }}">
                    @csrf
                    <div class="row">
                        <div class="form-group col-6 ">
                            <label for="title">Category Name</label>
                            <input type="text" name="name" class="form-control" id="name"
                                placeholder="Enter category name " required>
                        </div>

                        <div class="form-group col-6 ">
                            <label for="title">Status</label>
                            <select name="status" id="status" class="form-control pb-1">
                                <option value="" selected>Choose Status</option>
                                @foreach ($options as $option)
                                    <option value="{{ $option }}">{{ $option }}</option>
                                @endforeach
                            </select>
                            {{-- <input type="text" name="status" class="form-control" id="status"
                                placeholder="Enter meta title" required> --}}
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info me-md-2" form="categoryform" type="submit">Create</button>
                <button type="button" class="btn btn-info" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
