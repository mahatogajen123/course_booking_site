<div class="modal" data-backdrop="static" data-keyboard="false" id="view{{ $v->id }}" tabindex="-1"
    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="margin-left:0; margin-right:0">
                <h5 class="modal-title" id="exampleModalLabel">{{ $v->first_name }}{{ ' ' . $v->last_name }}</h5>
                <button class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close"
                    style="font-size: 20px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ">
                {{-- <img src="{{ asset('uploads/course/images/' . $v->image) }}" alt="" srcset=""> --}}
                <!-- Horizontal card layout -->
                <div class="row">
                    <div class="col-lg-12 px-5">
                        <div class="row mt-1 ml-5">
                            <div class="col-lg-5">
                                <h3 class="fs-base">Name :-</h3>
                                <h3 class="fs-base">Email:-</h3>
                                <h3 class="fs-base">Gender :-</h3>
                                <h3 class="fs-base">Date of Birth :-</h3>
                                <h3 class="fs-base">Course :-</h3>
                                <h3 class="fs-base">Home Contact :-</h3>
                                <h3 class="fs-base">personal Contact :-</h3>
                                <h3 class="fs-base">Permanent Address :-</h3>
                                <h3 class="fs-base">Current Address :-</h3>
                            </div>
                            <div class="col-lg-7 m-0 p-0">
                                <h3 class="fs-sm text-muted">{{ $v->first_name . ' ' }} {{ $v->last_name }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->email }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->gender }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->dob }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->course->title ?? ' Not Mentioned' }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->home_contact }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->personal_contact }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->permanent_address }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->current_address }}</h3>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
