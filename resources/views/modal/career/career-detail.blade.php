<div class="modal" data-backdrop="static" data-keyboard="false" id="view{{ $v->id }}" tabindex="-1"
    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="margin-left:0; margin-right:0">
                <h5 class="modal-title" id="exampleModalLabel">{{ $v->title }}</h5>
                <button class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close"
                    style="font-size: 20px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- <img src="{{ asset('uploads/course/images/' . $v->image) }}" alt="" srcset=""> --}}
                <!-- Horizontal card layout -->
                <div class="row">
                    <div class="col-lg-8 pl-5">
                        <div class="row mt-1">
                            <div class="col-lg-5">
                                <h3 class="fs-base">Title:</h3>
                                <h3 class="fs-base">Sub Title:</h3>
                                <h3 class="fs-base">Job Type:</h3>
                                <h3 class="fs-base">Job Level:</h3>
                                <h3 class="fs-base">Company Name:</h3>
                                <h3 class="fs-base">Location:</h3>
                                <h3 class="fs-base">Offer Salary:</h3>
                                <h3 class="fs-base">Job Created Date:</h3>
                                <h3 class="fs-base">Deadline:</h3>
                                <h3 class="fs-base">Short Detail:</h3>
                                <h3 class="fs-base">Detail:</h3>
                            </div>
                            <div class="col-lg-5">
                                <h3 class="fs-base text-muted">{{ $v->title }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->sub_title }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->job_type }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->job_level }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->company_name }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->location }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->offer_salary }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->job_create_date }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->deadline }}</h3>
                                <h3 class="fs-base text-muted">{!! $v->short_detail !!}</h3>
                                <h3 class="fs-base text-muted">{!! $v->detail !!}</h3>

                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                    </div>
                    <div class="col-lg-4 p-3 text-center">
                        <div class=" justify-content-center align-items-center mb-3">
                            <img src="{{ asset('uploads/career/images/' . $v->image) }}"
                                style="width: 316px;height: 198px" class="img-thumbnail rounded mb-3 mt-3"
                                alt="Rounded image"><br>
                            <p>{{ $v->short_detail }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
