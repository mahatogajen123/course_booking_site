<div class="modal" data-backdrop="static" data-keyboard="false" id="view{{ $v->id }}" tabindex="-1"
    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header" style="margin-left:0; margin-right:0">
                <h5 class="modal-title" id="exampleModalLabel">{{ $v->title }}</h5>
                <button class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close"
                    style="font-size: 20px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- <img src="{{ asset('uploads/course/images/' . $v->image) }}" alt="" srcset=""> --}}
                <!-- Horizontal card layout -->
                <div class="row">
                    <div class="col-lg-8 pl-5">
                        <div class="row mt-1">
                            <div class="col-lg-5">
                                <h3 class="fs-base">Title:</h3>
                                <h3 class="fs-base">Meta Title:</h3>
                                <h3 class="fs-base">Og Meta Title:</h3>
                                <h3 class="fs-base">Slug:</h3>
                                <h3 class="fs-base">Short Detail:</h3>
                                <h3 class="fs-base">Duration:</h3>
                                <h3 class="fs-base">Start Date:</h3>
                                <h3 class="fs-base">End Date:</h3>
                                <h3 class="fs-base">Price:</h3>

                                {{-- <a href="{{ route('admin.provider.edit', ['id' => $user->id, 'type' => 'vehicle']) }}"
                                    class="btn btn-info btn-shadow"><i class="ci-edit-alt me-2"></i>Edit</a> --}}
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-5">
                                <h3 class="fs-base text-muted">{{ $v->title }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->meta_title }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->og_meta_title }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->slug }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->short_detail }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->duration }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->start_date }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->end_date }}</h3>
                                <h3 class="fs-base text-muted">{{ $v->price }}</h3>

                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                    </div>
                    <div class="col-lg-4 p-3 text-center">
                        <div class=" justify-content-center align-items-center mb-3">
                            <img src="{{ asset('uploads/course/images/' . $v->image) }}"
                                style="width: 316px;height: 198px" class="img-thumbnail rounded mb-3 mt-3"
                                alt="Rounded image"><br>
                            <p>{!! $v->description !!}</p>
                        </div>
                    </div>
                </div>
                <hr style="border: 2px solid black">
                <div class="row">
                    <div class="col-lg-8 pl-5">
                        <div class="row mt-1">
                            <div class="col-lg-5">
                                @foreach ($v->courseDetail as $item)
                                    <h3 class="fs-base">Title:</h3>
                                    <h3 class="fs-base">Description:</h3>
                                @endforeach
                                {{-- <a href="{{ route('admin.provider.edit', ['id' => $user->id, 'type' => 'vehicle']) }}"
                                    class="btn btn-info btn-shadow"><i class="ci-edit-alt me-2"></i>Edit</a> --}}
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-5">
                                @foreach ($v->courseDetail as $item)
                                    <h3 class="fs-base text-muted">{{ $item->title }}</h3>
                                    <h3 class="fs-base text-muted">{!! $item->description !!}</h3>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
