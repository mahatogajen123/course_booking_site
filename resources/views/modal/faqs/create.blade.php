<div class="modal" data-backdrop="static" data-keyboard="false" id="view" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="margin-left:0; margin-right:0">
                <h5 class="modal-title" id="exampleModalLabel">Create FAQs</h5>
                <button class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close"
                    style="font-size: 20px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- <img src="{{ asset('uploads/course/images/' . $v->image) }}" alt="" srcset=""> --}}
                <!-- Horizontal card layout -->
                <form id="category" method="POST" enctype="multipart/form-data"
                    action="{{ route('admin.faqs.store') }}" >
                    @csrf
                    <div class="row">
                        <div class="form-group col-12 ">
                            <label for="title">Question</label>
                            <input type="text" name="question" class="form-control" id="question"
                                placeholder="Enter Question" required>
                        </div>
                        <div class="form-group col-12 ">
                            <label for="title">Answer</label>
                            {{-- <input type="text" name="answer" class="" id="summernote"
                                placeholder="Enter Answer" required> --}}
                                <textarea name="answer" id="summernote" cols="20" rows="10"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info me-md-2" form="category" type="submit">Create</button>
                <button type="button" class="btn btn-info" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
