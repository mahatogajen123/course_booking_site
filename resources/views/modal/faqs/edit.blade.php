<div class="modal" data-backdrop="static" data-keyboard="false" id="edit{{ $v->id }}" tabindex="-1"
    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="margin-left:0; margin-right:0">
                <h5 class="modal-title" id="exampleModalLabel">Edit FAQs</h5>
                <button class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close"
                    style="font-size: 20px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- <img src="{{ asset('uploads/course/images/' . $v->image) }}" alt="" srcset=""> --}}
                <!-- Horizontal card layout -->
                <form id="faqs{{ $v->id }}" method="POST" enctype="multipart/form-data"
                    action="{{ route('admin.faqs.update', $v->id) }}">
                    @csrf
                    <div class="row">
                        <div class="form-group col-12 ">
                            <label for="title">Question</label>
                            <input type="text" name="question" value="{{ $v->question }}" class="form-control"
                                id="meta_title" placeholder="Enter Question" required>
                        </div>
                        <div class="form-group col-12 ">
                            <label for="title">Answer</label>
                            {{-- <input type="text" name="answer" value="{!! $v->answer !!}" class="form-control"
                                id="meta_title" placeholder="Enter Question" required> --}}
                            <textarea name="answer" id="summernote1" cols="20"
                                rows="6">{!! $v->answer !!}</textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info me-md-2" form="faqs{{ $v->id }}" type="submit">Update</button>
                <button type="button" class="btn btn-info" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
{{-- <script>
     $(document).ready(function() {
            $('#summernote').summernote1({
                height: 150
            });
        });
</script> --}}
