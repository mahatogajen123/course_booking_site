<div class="modal" data-backdrop="static" data-keyboard="false" id="edit{{ $v->id }}" tabindex="-1"
    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header" style="margin-left:0; margin-right:0">
                <h5 class="modal-title" id="exampleModalLabel">Create Blog Category</h5>
                <button class="close bg-transparent border-0" data-dismiss="modal" aria-label="Close"
                    style="font-size: 20px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- <img src="{{ asset('uploads/course/images/' . $v->image) }}" alt="" srcset=""> --}}
                <!-- Horizontal card layout -->
                <form id="categoryform{{ $v->id }}" method="POST" enctype="multipart/form-data"
                    action="{{ route('admin.blog.category.update', $v->id) }}">
                    @csrf
                    <div class="row">
                        <div class="form-group col-12 ">
                            <label for="title">Category Name</label>
                            <input type="text" name="name" value="{{ $v->name }}" class="form-control"
                                id="meta_title" placeholder="Enter meta title" required>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info me-md-2" form="categoryform{{ $v->id }}"
                    type="submit">Update</button>
                <button type="button" class="btn btn-info" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>
