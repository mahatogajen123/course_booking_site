<?php
return [
    'new_industry' => 'New Industry',
    'industry' => 'Industry',
    'update_industry' => 'Update Industry',

    'new_employer' => 'New Employer',
    'update_employer' => 'Update Employer',

    'url' => 'URL',
    'social_media' => 'Social Media',
    'facebook' => 'Facebook',
    'youtube' => 'Youtube',
    'twitter' => 'Twitter',
    'logo' => 'Logo',
    'image' => 'Image'
];
