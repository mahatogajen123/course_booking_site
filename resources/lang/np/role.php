<?php
return [

    //Role
    'update_role' => 'भूमिका अपडेट गर्नुहोस्',
    'read-only' => 'Read-only',
    'new_role' => 'नयाँ भूमिका',
    'role_name' => 'भूमिका को नाम',
    'role_permission' => 'भूमिका अनुमति',
    'module' => 'मोड्युल',
    'view' => 'दृश्य',
    'create' => 'सिर्जना',
    'update' => 'अपडेट',
    'delete' => 'मेटाउन',
    'role' => 'भूमिका',
    'cancel' => 'रद्द गर्नुहोस्',

];
