<?php
return [
    //Tab Heading
    //=====================================================
    'basic_information'=>'Basic Information',
    'educational_qualification'=>'Educational Qualification',
    'work_history'=>'Work History',
    'extra_qualification'=>'Extra Qualification',
    'interested_field'=>'Interested Field',
    'strength'=>'Strength',
    'select_one' => 'Select One',
    'motivation'=>'Motivation',
    'others'=>'Others',
    //=====================================================
    'full_name'=>'Full Name',
    'name_katakana'=>'Name in Katakana',
    'sex'=>'Sex',
    'male'=>'Male',
    'female'=>'Female',
    'birth_date'=>'Date of Birth',
    'age'=>'Age',
    'email_address'=>'Email Address',
    'main_address'=>'Address',
    'main_address_katakana'=>'Katakana Address',
    'zip_code'=>'Zip Code',
    'prefecture'=>'Prefecture',
    'municipality'=>'Municipality',
    'other_address'=>'Other Address',
    'building_name'=>'Building Name Room No.',
    'mobile_no'=>'Mobile No',
    'tel_no'=>'Tel No',
    'contact_address'=>'Contact Address',
    'permanent_address' =>'Permanent Address',
    'temporary_address' =>'Emergency Contact',
    //Education Qualification
    //==================================================================
    'college'=>'College',
    'country'=>'Country',
    'date'=>'Date',
    'status'=>'Status',
    'start_date' => 'Start Date',
    'end_date' => 'End Date',
    'admission_date' => 'Admission Date',
    'graduation_date' => 'Graduation Date',
    'company_name' => 'Company Name',
    'remarks' => 'remarks',
    'qualification' => 'Qualification',
    'details' => 'Details',
    'interested_subjects' => 'What I have studied at school',
    'focus_on' => 'Focused on so far',
    'special_skills' => 'Special skills',
    'fill_if_request' => 'Fill in, if there is a request for',
    'upload_image' => 'Upload Image',
    'same_as_permanent' => 'Same As The Address',
    'minor_case' => 'If a minor, fill parent information in Katakana',
    'translate' => 'Translate',
    'go_back_to_main' => 'Go back to main page',
    //Status
    'running' => 'Running',
    'graduation' => 'Graduated',
    'drop_college' => 'Drop Out',
    'expected_graduation' => 'Expected Graduation',
    'new_joined' => 'New Joined',
    'join' => 'Join',
    'left' => 'Left',
    'retirement' => 'Retirement',
    'during_employment' => 'During Employment',

    //WOrk History
    //==================================================================
    'company_name'=>'Company Name',
    //Extra Qualification
    //==================================================================
    'your_qualification'=>'Your Qualification',
    'qualification_list'=>'List of qualifications',
    //Strength
    //==================================================================
    'your_strength'=>'Self PR',
    'self_pr_title' => 'Example Sentence',
    'appeal'=>'Appeal',
    'job_type'=>'Job Type',
    'job_level'=>'Job Level',
    'job_location'=>'Preferred Workplace',
    'status_of_residence' => 'Status Of Residence',
    'desired_salary' => 'Desired Salary',
    'nearest_station' => 'Nearest Station',
    'period_of_stay' => 'Period Of Stay',
    'nationality' => 'Nationality',
    //Motivation
    //==================================================================
    'appeal'=>'Appeal',
    'job_type'=>'Job Type',
    'job_level'=>'Job Level',
    //Others
    //==================================================================
    'commuting_time'=>'Commuting time from work location',
    'dependents_number'=>'Dependents Number',
    'excluding_spouse'=>'Excluding Spouse',
    'have_spouse'=>'Have Spouse?',
    'yes'=>'Yes',
    'no'=>'No',
    'support_spouse'=>'Do you live with your family',

    'save_cv'=>'Save your CV',
    'preview'=>'Preview',
    'share'=>'Share',
    'if_you_wish' => 'Fill in if you wish for anything else',
    'language' => 'Language',
    'native_language' => 'Native Language',
    'known_languages' => 'Known Languages',
    'personal_request' => 'Personal Request',
    'conversation_level' => 'Conversation Level',
    'mother_tongue' => 'Native Language',
    'native' => 'Native',
    'business' => 'Business Level',
    'daily_conversation' => 'Daily Conversation',
    'katakoto' => 'Katakoto',

    //curriculum form
    'curriculum_vitae' => 'Curriculum Vitae',
    'make_curriculum_vitae' => 'Make Curriculum Vitae',
    'job_summary' => 'Job Summary',
    'experience_knowledge' => 'Experience & Knowledge',
    'work_details' => 'Work Details',
    'main_duties' => 'Main Duties',
    'create_curriculum_vitae' => 'Create Curriculum Vitae',
    'edit_curriculum_vitae' => 'Edit Curriculum Vitae',
    'can_not_speak' => 'Cannot Speak',
    'other' => 'Other',
    'curriculum_full_name' => 'Full Name',
];
