<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login' => 'Login',
    'signup' => 'Sign up',
    'login_page'=> 'Login Page',
    'admin_panel_login'=>'Admin Panel Login',
    'save_password'=>'Save Password',
    'forgot_password'=>'Forgot Password',

    'login_with_google' => 'Login with Google',
    'login_with_facebook' => 'Login with Facebook',
    'donot_have_an_account' => 'Donot have an account',
    'you_have_an_account' => 'you have an account',
    'your_name' => 'Your Name',
    'your_email' => 'Your Email',
    'password' => 'Password',

     //Forgot password page
     'forgot_your_password' => 'Forgot your password',
     'instruction_1' => 'Enter your email address below and we will send',
     'instruction_2' => 'instructions on how to change your password.',
     'recovery_link_btn' => 'Send Recovery Link',
     'return_to_login' => 'Return to Login',

];
