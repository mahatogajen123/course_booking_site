<?php
return [
    //Banner
    'banner_h2' => 'Discover',
    'banner_h3' => 'your amazing potential',
    'banner_p' => 'build your amazing resume for the dream Job you always wanted',
    'make_your_cv' => 'Make Your Cv',


    //Main container Section
    'main_container_h2' => 'Why Resume Maker is Loved by Millions',
    'main_container_p' => 'The toolkit you need to get hired faster.',

    //Reason Why Resume Maker section
    'reason_1_h3' => 'Language can be selected',
    'reason_1_p' => 'CVpro is available in English, Chinese, Korean, Japanese and Vietnam.
    When writing a resume, you can write while translating.
    There is a job for foreigners in the app, which is also translated
    If you can apply from within the app and a new job appears
    Will report by email.',
    'reason_2_h3' => 'PDF upload function',
    'reason_2_p' => 'You can quickly move from your existing resume to our app.
    Using AI such as aspiration motives, create aspiration motives that suit the person',
    'reason_3_h3' => 'Job matching function',
    'reason_3_p' => 'You can find a job that suits you in the app,
    You can notify the matching work.',

    //Dashboard
    'total_number_of_users' => 'Total number of users',
    'more_users' => 'More Users',
    'total_cv' => 'Total CV',
    'more_cv' => 'More CV',

    //CV Form
    'build_your_resume' => 'Build your Resume',
    'terms_&_conditions' => 'Terms & Conditions',
    'privacy_policy' => 'Privacy Policy',

    //Container
    'total_learning' => 'Total Learning',
    'more_learning' => 'More Learning',

    'cv' => 'Resume',
    'curriculum' => 'Curriculum',
];
