<?php

use App\Models\Vehicle;

return [
    //Email Setting
    'email_setting' => 'Email Setting',
    'event' => 'Event',
    'update_email_module' => 'Update Email Module',
    'templates' => 'Templates',
    'edit_template' => 'टेम्प्लेट सम्पादन गर्नुहोस्',
    'setting' => 'सेटिंग',
    //vehicle-type index
    'vehicle_name' => 'सवारीको नाम',
    'capacity' => 'क्षमता',
    'price_per_km' => 'मूल्य/किमी',
    'actions' => 'कार्यहरु',
    'serial_no' => 'क्र.सं',
    'search' => 'खोज',
    'new_vehicle_type' => 'नयाँ वाहन को प्रकार',
    'dashboard' => 'ड्यासबोर्ड',
    'vehicle_type' => 'सवारी साधन को प्रकार',
    'vehicle' => 'सवारी साधन',
    'delivery_type' => 'वितरण प्रकार',
    'delivery_order' => 'वितरण आदेश',
    'manage_user' => 'प्रयोगकर्ता व्यवस्थापन',
    'admin' => 'व्यवस्थापक',
    'employer' => 'रोजगारदाता',
    'normal' => 'सामान्य',
    'role_permission' => 'भूमिका र अनुमति',
    'main_navigation' => 'मुख्य नेभिगेसन',
    'vehicle_type_create'=> 'सवारी साधन सिर्जना',
    //vehicle type create
    'name' => 'नाम',
    'save' => 'सेभ गर्नुस ',
    'placeholder_name'=> 'सवारी साधन को नाम प्रविष्ट गर्नुहोस्',
    'placeholder_capacity'=> 'वाहन को क्षमता प्रविष्ट गर्नुहोस्',
    'placeholder_price_per_km'=> 'प्रति किलोमीटर मूल्य प्रविष्ट गर्नुहोस्',

    //role index
   

];
