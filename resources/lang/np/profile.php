<?php
return [

//Profile
    'change_password'=>'Change Password',
    'current_password'=>'Current Password',
    'new_password'=>'New Password',
    'confirm_new_password'=>'Confirm New Password',
    'update_profile' => 'Update Profile',
    'upload' => 'Upload',
    'choose_file' => 'Choose File',
    'profile_picture' => 'Profile Picture',

];