<?php
return [
    //Banner section
    'banner_h2' => '発見する',
    'banner_h3' => 'あなたの驚くべき可能性',
    'banner_p' => 'あなたがいつも望んでいた夢の仕事のためにあなたの素晴らしい再開を構築する',
    'make_your_cv' => 'あなたのCVを作る',
    'build_your_resume' => '履歴書を作成する',


    //Main container Section
    'main_container_h2' => 'Resume Maker が何百万人もの人々に愛されている理由',
    'main_container_p' => 'あなたがより早く雇われるために必要なツールキット。',

    //Reason Why Resume Maker section
    'reason_1_h3' => '言語選択出来る',
    'reason_1_p' => 'CVproは英語,中国語,韓国語,日本語,ベトナムで利用出来ます。
    履歴書書く時翻訳しながら書く事出来ます。
    アプリ内に外国人専用の求人があり、それも翻訳され、
    そのアプリ内から応募ができ、新しい求人がでたら
    メールにて報告してくれる。',
    'reason_2_h3' => 'PDFアップロード機能',
    'reason_2_p' => 'すでにある履歴書からすぐに弊社のアプリに移行できるまた、
    　志望動機などAIを使いその人に合った志望動機を作ってくれる',
    'reason_3_h3' => '仕事をマッチング機能',
    'reason_3_p' => 'アプリ内で自分に合うな仕事を探す事出来て、
    　マッチングした仕事通知できます。',

    //Footer
    'footer_description' => '直感的なレジュームビルダーを使用して、レジュームコンテンツをすばやく追加したり、テンプレートを変更したり、フォントをカスタマイズしたりできます。ダウンロードは必要ありません。',

    'about_us' => '私たちに関しては',
    'faq' => 'よくある質問',
    'privacy_policy' => '個人情報保護方針',
    'job_category' => '職種',
    'footer_input_name' => '名前を入力してください',
    'footer_input_email' => 'Eメール',
    'footer_textarea_message' => 'あなたのメッセージ',
    'footer_send_btn' => 'メッセージを送る',
    'terms_&_conditions' => '規約と条件',

    //Container
    'total_learning' => 'トータルラーニング',
    'more_learning' => '詳細学習',


    //Dashboard
    'total_number_of_users' => 'ユーザーの総数',
    'more_users' => 'より多くのユーザー',
    'total_cv' => '合計CV',
    'more_cv' => 'より多くの履歴書',
    'cv' => '履歴書',
    'curriculum' => '職務経歴書',
];
