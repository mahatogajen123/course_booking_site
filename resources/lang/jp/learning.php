<?php
return [
    'new_resource'=>'新しいリソース',
    'update_resource'=>'リソースの更新',
    'title'=>'題名',
    'description'=>'説明',
    'link'=>'リンク',
    'video_url'=>'ビデオURL',
    'publish_date'=>'公開日',
    'category'=>'カテゴリ'
];
