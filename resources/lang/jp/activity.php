<?php
return [
    'user_name' => 'User Name',
    'user_type' => 'User Type',
    'device' => 'Device',
    'login_ip' => 'Login Ip',
    'browser' => 'Browser',
    'os' => 'Os',
    'login_time' => 'Login Time',
    'logout_time' => 'Logout Time',

    'actor' => 'Actor',
    'description' => 'Description',
    'table' => 'Table',
];