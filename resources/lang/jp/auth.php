<?php
return [
    'login' => 'ログインする',
    'signup' => 'サインアップ',
    'login_page' => 'ログインページ',
    'admin_panel_login'=>'管理パネルログイン',
    'save_password'=>'パスワードを保存する',
    'forgot_password'=>'パスワードをお忘れですか',
    'logout' => 'ログアウト',


    'login_with_google' => 'Googleでログイン',
    'login_with_facebook' => 'Facebookでログイン',
    'donot_have_an_account' => 'アカウントを持っていないのですか',
    'you_have_an_account' => 'あなたはアカウントを持っています',
    'your_name' => 'あなたの名前',
    'your_email' => 'あなたのメール',
    'password' => 'パスワード',
    
     //Forgot password page
     'forgot_your_password' => 'パスワードをお忘れですか',
     'instruction_1' => '以下にメールアドレスを入力してください。',
     'instruction_2' => 'パスワードを変更する方法の説明。',
     'recovery_link_btn' => 'リカバリリンクを送信',
     'return_to_login' => 'ログインに戻る',

];
