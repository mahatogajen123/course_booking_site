<?php

return [
    'dashboard' => 'ダッシュボード',
    'main_navigation' => 'メインナビゲーション',
    'role_permissions' => '役割と権限',
    'manage_profile' => 'プロフィールを管理',
    'view_profile' => 'プロフィールを見る',
    'manage_user' => 'ユーザーを管理',
    'admin' => '管理者',
    'normal' => '正常',
    'add_new_user' => '新しいユーザーを追加',
    'settings' => '設定',
    'industry' => '業界',
    'email_settings' => 'メール設定',
    'email_module' => 'メールモジュール',
    'email_template' => 'メールテンプレート',
    'inquiry' => '問い合わせ',
    'locale' => 'ロケール',
    'employer_profile' => 'Employer Profile',
    'jobs_profile' => 'Jobs Profile',
    //    Top Menu
    //=======================
    'home' => 'ホーム',
    'about_us' => '私たちに関しては',
    'faq' => 'よくある質問',
    'learning' => '学習',
    'job_category' => '求人カテゴリ',
    //=======================
    'activity_logs' => 'アクティビティログ',
    'login_activity' => 'ログインアクティビティ',
    'user_activity' => 'ユーザーアクティビティ',

    'user_since' => '以来のユーザー',
    'change_password' => 'パスワードを変更する',
    'logout' => 'ログアウト',
    'contact_us' => 'お問い合わせ',
    // Sidebar
    'school_manage' => '学校管理',
    'add_new_school' => '学校登録',
    'list_school' => '学校マスター',
    'type_school' => '学校区分',

    'shops_manage' => 'メーカー管理',
    'add_new_shop' => 'メーカー登録',
    'list_shops' => 'メーカーマスター',

    'settings' => '設定',

];
