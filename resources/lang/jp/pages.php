<?php
return [
    'roles' => '役割',
    'new_role' => '新しい役割',
    'update_role'=>'役割の更新',
    'role_name' => 'ロール名',
    'permissions'=>'権限',
    'module' => 'モジュール',
    'create' => '作成する',
    'view' => '見る',
    'update' => '更新',
    'delete' => '削除する',
    'update_profile' => 'プロファイルの更新',
    'admin_users' => '管理ユーザー',
    'normal_users' => '通常のユーザー',
    'new_user' => '新しいユーザー',
    'type' => 'タイプ',
    'email_settings' => 'メール設定',
    'new_email_module' => '新しい電子メールモジュール',
    'namespace' => '名前空間',
    'usefull_resources' => '役立つリソース',
    'resource'=>'資源',
    'new_resource'=>'新しいリソース',
    'title' => '題名',
    'description' => '説明',
    'link' => 'リンク',
    'video_url' => 'ビデオURL',
    'publish_date' => '公開日',

    //Profile
    'change_password'=>'パスワードを変更する',
    'current_password'=>'現在のパスワード',
    'new_password'=>'新しいパスワード',
    'confirm_new_password'=>'新しいパスワードを確認',

    //Activity logs
    //Activity logs
    'user_activity'=>'ユーザーアクティビティ',
    'login_activity'=>'ログインアクティビティ',
    'activity_logs' => 'アクティビティログ',

    'user_name' => 'ユーザー名',
    'user_type' => 'ユーザータイプ',
    'device' => '端末',
    'login_ip' => 'ログインIp',
    'browser' => 'ブラウザ',
    'os' => 'Os',
    'login_time' => 'ログイン時間',
    'logout_time' => 'ログアウト時間',
    'actor' => '俳優',
    'table' => 'テーブル',
    'created_at' => 'で作成',

    //Role
    'roles_and_permission' => '役割と許可',

    //Sidebar
    'resume_maker' => 'レジュームメーカー',
    'main_navigation' => '主なナビゲーション',
    'useful_resources'=>'役立つリソース',

    //Users
    'users' => 'ユーザー',
    //Email Setting
    'email_setting'=>'メール設定',

    //Dashboard
    'total_number_of_users' => 'ユーザーの総数',
    'more_users' => 'より多くのユーザー',
    'total_cv' => '総CV',
    'more_cv' => 'より多くのCV',

    //Mailable
    'email_modules' => 'メールモジュール',
];
