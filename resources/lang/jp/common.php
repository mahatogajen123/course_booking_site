<?php
return [
    'name' => '名前',
    'create' => '作成する',
    'update' => 'アップデイト',
    'cancel' => 'キャンセル',
    'status' => '状態',
    'actions' => '行動',
    'details' => '詳細',
    'search' => '探す',
    'created_at' => '作成場所',
    'namespace'=>'名前空間',
    'language' => '言語',
    'create_cv' => '履歴書を作成する',
    'edit_cv' => '履歴書を編集する',
    'save' => '保存する',
    'type' => 'タイプ',
    'Please choose the format you want your document to be downloaded!'=>'ドキュメントをダウンロードする形式を選択してください。'
];
