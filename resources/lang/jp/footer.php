<?php
return [

    //Footer
    'footer_description' => 'CVPro 職務経歴書は、求職者の皆様の自己表現をサポートする職務経歴書作成サービスです。所定のフォームに入力するだけで、美しく整形された紙面が完成。PDFでダウンロードして頂けます。また紙面デザインを多数用意しており、求職者の皆様の感性に合った紙面をお選び頂けます',
    'quick_links' => 'クイックリンク',
    'privacy_policy' => '個人情報保護方針',
    'footer_input_name' => '名前を入力してください',
    'footer_input_email' => 'Eメール',
    'footer_textarea_message' => 'あなたのメッセージ',
    'footer_send_btn' => 'メッセージを送る',
    'name' => '名前',
    'phone_no' => '電話番号',
    'contact_detail' => '連絡先の詳細'
];
