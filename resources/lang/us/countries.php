<?php
return [
    "abkhazia_republic_of" => "Abkhazia, Republic of",
    "afghanistan" => "Afghanistan",
    "albania" => "Albania",
    "algeria" => "Algeria",
    "andorra" => "Andorra",
    "angola" => "Angola",
    "antigua_and_barbuda" => "Antigua and Barbuda",
    "argentina" => "Argentina",
    "armenia" => "Armenia",
    "australia" => "Australia",
    "austria" => "Austria",
    "azerbaijan" => "Azerbaijan",
    "bahamas" => "Bahamas",
    "bahrain" => "Bahrain",
    "bangladesh" => "Bangladesh",
    "barbados" => "Barbados",
    "belarus" => "Belarus",
    "belgium" => "Belgium",
    "belize" => "Belize",
    "benin" => "Benin",
    "bhutan" => "Bhutan",
    "bolivia_plurinational_state_of" => "Bolivia, Plurinational State of",
    "bosnia_and_herzegovina" => "Bosnia and Herzegovina",
    "botswana" => "Botswana",
    "brazil" => "Brazil",
    "brunei_darussalam" => "Brunei Darussalam",
    "bulgaria" => "Bulgaria",
    "burkina_faso" => "Burkina Faso",
    "burundi" => "Burundi",
    "cambodia" => "Cambodia",
    "cameroon" => "Cameroon",
    "canada" => "Canada",
    "cape_verde" => "Cape Verde",
    "central_african_republic" => "Central African Republic",
    "chad" => "Chad",
    "chile" => "Chile",
    "china" => "China",
    "colombia" => "Colombia",
    "comoros" => "Comoros",
    "congo" => "Congo",
    "congo_the_democratic_republic_of_the" => "Congo, the Democratic Republic of the",
    "cook_islands" => "Cook Islands",
    "costa_rica" => "Costa Rica",
    "cte_divoire" => "Côte d\'Ivoire",
    "croatia" => "Croatia",
    "cuba" => "Cuba",
    "cyprus" => "Cyprus",
    "czechia" => "Czechia",
    "denmark" => "Denmark",
    "djibouti" => "Djibouti",
    "dominica" => "Dominica",
    "dominican_republic" => "Dominican Republic",
    "ecuador" => "Ecuador",
    "egypt" => "Egypt",
    "el_salvador" => "El Salvador",
    "equatorial_guinea" => "Equatorial Guinea",
    "eritrea" => "Eritrea",
    "estonia" => "Estonia",
    "ethiopia" => "Ethiopia",
    "fiji" => "Fiji",
    "finland" => "Finland",
    "france" => "France",
    "gabon" => "Gabon",
    "gambia" => "Gambia",
    "georgia" => "Georgia",
    "germany" => "Germany",
    "ghana" => "Ghana",
    "greece" => "Greece",
    "grenada" => "Grenada",
    "guatemala" => "Guatemala",
    "guinea" => "Guinea",
    "guineabissau" => "Guinea-Bissau",
    "guyana" => "Guyana",
    "haiti" => "Haiti",
    "holy_see_vatican_city_state" => "Holy See (Vatican City State)",
    "honduras" => "Honduras",
    "hungary" => "Hungary",
    "iceland" => "Iceland",
    "india" => "India",
    "indonesia" => "Indonesia",
    "iran_islamic_republic_of" => "Iran, Islamic Republic of",
    "iraq" => "Iraq",
    "ireland" => "Ireland",
    "israel" => "Israel",
    "italy" => "Italy",
    "jamaica" => "Jamaica",
    "japan" => "Japan",
    "jordan" => "Jordan",
    "kazakhstan" => "Kazakhstan",
    "kenya" => "Kenya",
    "kiribati" => "Kiribati",
    "korea_democratic_peoples_republic_of" => "Korea, Democratic People's Republic of",
    "korea_republic_of" => "Korea, Republic of",
    "kosovo" => "Kosovo",
    "kuwait" => "Kuwait",
    "kyrgyzstan" => "Kyrgyzstan",
    "lao_peoples_democratic_republic" => "Lao People's Democratic Republic",
    "latvia" => "Latvia",
    "lebanon" => "Lebanon",
    "lesotho" => "Lesotho",
    "liberia" => "Liberia",
    "libya" => "Libya",
    "liechtenstein" => "Liechtenstein",
    "lithuania" => "Lithuania",
    "luxembourg" => "Luxembourg",
    "macedonia_the_former_yugoslav_republic" => "Macedonia, the former Yugoslav Republic",
    "madagascar" => "Madagascar",
    "malawi" => "Malawi",
    "malaysia" => "Malaysia",
    "maldives" => "Maldives",
    "mali" => "Mali",
    "malta" => "Malta",
    "marshall_islands" => "Marshall Islands",
    "mauritania" => "Mauritania",
    "mauritius" => "Mauritius",
    "mexico" => "Mexico",
    "micronesia_federated_states_of" => "Micronesia, Federated States of",
    "moldova_republic_of" => "Moldova,
 Republic of",
    "monaco" => "Monaco",
    "mongolia" => "Mongolia",
    "montenegro" => "Montenegro",
    "morocco" => "Morocco",
    "mozambique" => "Mozambique",
    "myanmar" => "Myanmar",
    "nagorno_karabagh_republic" => "Nagorno Karabagh Republic",
    "namibia" => "Namibia",
    "nauru" => "Nauru",
    "nepal" => "Nepal",
    "netherlands" => "Netherlands",
    "new_zealand" => "New Zealand",
    "nicaragua" => "Nicaragua",
    "niger" => "Niger",
    "nigeria" => "Nigeria",
    "niue" => "Niue",
    "northern_cyprus" => "Northern Cyprus",
    "norway" => "Norway",
    "oman" => "Oman",
    "pakistan" => "Pakistan",
    "palau" => "Palau",
    "palestinian_territory_occupied" => "Palestinian Territory, Occupied",
    "panama" => "Panama",
    "papua_new_guinea" => "Papua New Guinea",
    "paraguay" => "Paraguay",
    "peru" => "Peru",
    "philippines" => "Philippines",
    "poland" => "Poland",
    "portugal" => "Portugal",
    "pridnestrovian_moldavian_republic" => "Pridnestrovian Moldavian Republic",
    "qatar" => "Qatar",
    "romania" => "Romania",
    "russian_federation" => "Russian Federation",
    "rwanda" => "Rwanda",
    "saint_kitts_and_nevis" => "Saint Kitts and Nevis",
    "saint_lucia" => "Saint Lucia",
    "saint_vincent_and_the_grenadines" => "Saint Vincent and the Grenadines",
    "samoa" => "Samoa",
    "san_marino" => "San Marino",
    "sao_tome_and_principe" => "Sao Tome and Principe",
    "saudi_arabia" => "Saudi Arabia",
    "senegal" => "Senegal",
    "serbia" => "Serbia",
    "seychelles" => "Seychelles",
    "sierra_leone" => "Sierra Leone",
    "singapore" => "Singapore",
    "slovakia" => "Slovakia",
    "slovenia" => "Slovenia",
    "solomon_islands" => "Solomon Islands",
    "somalia" => "Somalia",
    "somaliland_republic_of" => "Somaliland, Republic of",
    "south_africa" => "South Africa",
    "south_ossetia" => "South Ossetia",
    "south_sudan" => "South Sudan",
    "spain" => "Spain",
    "sri_lanka" => "Sri Lanka",
    "sudan" => "Sudan",
    "suriname" => "Suriname",
    "swaziland" => "Swaziland",
    "sweden" => "Sweden",
    "switzerland" => "Switzerland",
    "syrian_arab_republic" => "Syrian Arab Republic",
    "taiwan_province_of_china" => "Taiwan, Province of China",
    "tajikistan" => "Tajikistan",
    "tanzania_united_republic_of" => "Tanzania, United Republic of",
    "thailand" => "Thailand",
    "timorleste" => "Timor-Leste",
    "togo" => "Togo",
    "tonga" => "Tonga",
    "trinidad_and_tobago" => "Trinidad and Tobago",
    "tunisia" => "Tunisia",
    "turkey" => "Turkey",
    "turkmenistan" => "Turkmenistan",
    "tuvalu" => "Tuvalu",
    "uganda" => "Uganda",
    "ukraine" => "Ukraine",
    "united_arab_emirates" => "United Arab Emirates",
    "united_kingdom" => "United Kingdom",
    "united_states" => "United States",
    "uruguay" => "Uruguay",
    "uzbekistan" => "Uzbekistan",
    "vanuatu" => "Vanuatu",
    "venezuela_bolivarian_republic_of" => "Venezuela, Bolivarian Republic of",
    "viet_nam" => "Viet Nam",
    "western_sahara" => "Western Sahara",
    "yemen" => "Yemen",
    "zambia" => "Zambia",
    "zimbabwe" => "Zimbabwe"
];
