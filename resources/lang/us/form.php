<?php

return [
    'driver'=>'Driver',
    'host'=>'Host',
    'from_name'=>'From Name',
    'from_address'=>'From Address',
    'username'=>'Username',
    'encryption'=>'Encryption',
    'port'=>'Port',
    'active'=>'Active',
    'inactive'=>'Inactive',
    'password' => 'Password',
    'confirm_password' => 'Retype Password',
    'email' => 'Email',

    'name' => 'Name',
    'address' => 'Address',
    'phone' => 'Phone',
    'gender' => 'Gender',
    'male' => 'Male',
    'female' => 'Female',
    'other' => 'Other',
    'contact_no'=>'Contact No',
    'fax'=>'Fax',

    'city'=>'City',
    'prefecture'=>'Prefecture',
    'postal_code'=>'Postal Code',
    'country_name'=>'Country Name',
    'email_address'=>'Email Address',
    'message' => 'Message',
    
     //japanese era
     'era_name' => 'Era Name',
     'begins_at' => 'Begins At',
     'ends_at' => 'Ends At',
     'japanese_era' => 'Japanese Era',
     'add_era' => 'Add Era',
     'update_era' => 'Update Era',
     'create_era' => 'Create Era',
     'edit_era' => 'Edit Era',
];
