<?php
return [
    ///Questions
    'faq_1' => 'How to sign up to the application?',
    'faq_2' => 'How to reset the password?',
    'faq_3' => 'How to change the language of the application?',
    'faq_4' => 'How to send resume and CV to mail address?',
    'faq_5' => 'What is the size of the photo you put on your resume?',
    'faq_6' => 'Is it okay to write a part-time job history in the work history column of my    resume?',

    ///Answers
    'faq_ans_1' => 'Please click the  signup button in the right side of the website or signup
    button in the application .A page with signup  form will open .Enter  your name ,mail address, password and re-enter  the password.Once you click the signup  button  link for verification your account is send in your mail.Once you verify the mail address  you can login your application.You can also signup with  google.',

    'faq_ans_2' => 'To reset the password , click forget password button in the login page.
    Enter your mail address  used  in the application.Click reset button after entering the mail.Password reset link will sent to your mail.You can reset the password through the link .',

    'faq_ans_3' => 'Please  go to the setting page in the application .There is the language page ,
    you can change the language from that page.',

    'faq_ans_4' => 'In the top right page of the resume and resume and CV making ,there is  share button .
    You can send your cv to different mail from there.',

    'faq_ans_5' => 'The recommended size is 36-40mm in length and 24-30mm in width.',
    
    'faq_ans_6' => 'Basically, the experience of a part-time job is not written in the work history column of the resume.',

];