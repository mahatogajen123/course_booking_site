<?php
return [
    'dane' => 'Dane',
    'british' => 'British',
    'estonian' => 'Estonian',
    'finn' => 'Finn',
    'icelander' => 'Icelander',
    'irish' => 'Irish',
    'latvian' => 'Latvian',
    'lithuanian' => 'Lithuanian',
    'northern_irish' => 'Northern Irish',
    'norwegian' => 'Norwegian',
    'scot' => 'Scot',
    'swede' => 'Swede',
    'briton' => 'Briton',
    'welsh' => 'Welsh',
    'austrian' => 'Austrian',
    'belgian' => 'Belgian',
    'french' => 'French',
    'german' => 'German',
    'dutch' => 'Dutch',
    'swiss' => 'Swiss',
    'albanian' => 'Albanian',
    'croatian' => 'Croatian',
    'cypriot' => 'Cypriot',
    'greek' => 'Greek',
    'italian' => 'Italian',
    'portuguese' => 'Portuguese',
    'serbian' => 'Serbian',
    'slovenian' => 'Slovenian',
    'spaniard' => 'Spaniard',
    'belarusian' => 'Belarusian',
    'bulgarian' => 'Bulgarian',
    'czech' => 'Czech',
    'hungarian' => 'Hungarian',
    'pole' => 'Pole',
    'romanian' => 'Romanian',
    'russian' => 'Russian',
    'slovakian' => 'Slovakian',
    'ukrainian' => 'Ukrainian',
    'canadian' => 'Canadian',
    'mexican' => 'Mexican',
    'american' => 'American',
    'cuban' => 'Cuban',
    'guatemalan' => 'Guatemalan',
    'jamaican' => 'Jamaican',
    'argentinian' => 'Argentinian',
    'bolivian' => 'Bolivian',
    'brazilian' => 'Brazilian',
    'chilean' => 'Chilean',
    'colombian' => 'Colombian',
    'ecuadorian' => 'Ecuadorian',
    'paraguayan' => 'Paraguayan',
    'peruvian' => 'Peruvian',
    'uruguayan' => 'Uruguayan',
    'venezuelan' => 'Venezuelan',
    'georgian' => 'Georgian',
    'iranian' => 'Iranian',
    'iraqi' => 'Iraqi',
    'israeli' => 'Israeli',
    'jordanian' => 'Jordanian',
    'kuwaiti' => 'Kuwaiti',
    'lebanese' => 'Lebanese',
    'palestinian' => 'Palestinian',
    'saudi_arabian' => 'Saudi Arabian',
    'syrian' => 'Syrian',
    'turk' => 'Turk',
    'yemeni' => 'Yemeni ',
    'afghani' => 'Afghani',
    'bangladeshi' => 'Bangladeshi',
    'indian' => 'Indian',
    'kazakhstani' => 'Kazakhstani',
    'nepalese' => 'Nepalese',
    'pakistani' => 'Pakistani',
    'sri_lankan' => 'Sri Lankan',
    'chinese' => 'Chinese',
    'japanese' => 'Japanese',
    'mongolian' => 'Mongolian ',
    'north_korean' => 'North Korean',
    'south_korean' => 'South Korean',
    'taiwanese' => 'Taiwanese',
    'cambodian' => 'Cambodian',
    'indonesian' => 'Indonesian',
    'laotian' => 'Laotian',
    'malaysian' => 'Malaysian',
    'burmese' => 'Burmese',
    'filipino' => 'Filipino',
    'singaporean' => 'Singaporean',
    'thai' => 'Thai',
    'vietnamese' => 'Vietnamese',
    'australian' => 'Australian',
    'fijian' => 'Fijian',
    'new_zealander' => 'New Zealander',
    'algerian' => 'Algerian',
    'egyptian' => 'Egyptian',
    'ghanaian' => 'Ghanaian',
    'ivorian' => 'Ivorian',
    'libyan' => 'Libyan',
    'moroccan' => 'Moroccan',
    'nigerian' => 'Nigerian',
    'tunisian' => 'Tunisian',
    'ethiopian' => 'Ethiopian',
    'kenyan' => 'Kenyan',
    'somalian' => 'Somalian',
    'sudanese' => 'Sudanese',
    'tanzanian' => 'Tanzanian',
    'ugandan' => 'Ugandan',
    'angolan' => 'Angolan',
    'botswanan' => 'Botswanan',
    'congolese' => 'Congolese',
    'malagasy' => 'Malagasy',
    'mozambican' => 'Mozambican',
    'namibian' => 'Namibian',
    'south_african' => 'South African',
    'zambian' => 'Zambian',
    'zimbabwean' => 'Zimbabwean',
    'uzbekistan' => "Uzbekistan"
];
