<?php
return [

    //Footer
    'footer_description' => 'CVPro is a resume creation service that supports the self-expression of job seekers. Simply fill out the form and you\'ll have a beautifully shaped A3 page. You can download it as a PDF. In addition, we have prepared a large number of paper designs, so you can choose the paper that suits the sensibility of job seekers.',
    'quick_links' => 'Quick Links',
    'privacy_policy' => 'Privacy Policy',
    'footer_input_name' => 'Type Your Name',
    'footer_input_email' => 'Email',
    'footer_textarea_message' => 'Your Message',
    'footer_send_btn' => 'Send Message',
    'name' => 'Name',
    'phone_no' => 'Phone No',
    'contact_detail' => 'Contact Detail'
];
