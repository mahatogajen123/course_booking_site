<?php
return [

    //Role
    'role_permissions' => 'Roles and Permission',
    'new_role' => 'New Role',
    'update_role' => 'Update Role',
    'read-only' => 'Read-only',
    'role_name' => 'Role Name',
    'role_permission'=>'Permission',
    'module'=>'Module',
    'view'=>'View',
    'create'=>'Create',
    'update'=>'Update',
    'delete'=>'Delete',
    'role'=>'Role',
    'cancel'=>'Cancel',
];
