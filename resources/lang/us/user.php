<?php
return [
    //Users
    'admin_users' => 'Admin Users',
    'normal_users' => 'Normal Users',
    'employer_users' => 'Employer Users',
    'role'=>'Role',
    'new_user' => 'New User',
    'update_user' => 'Update User',
    'account_verified_at' => 'Account Verified At',
];
